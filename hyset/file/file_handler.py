from copy import deepcopy
from functools import singledispatchmethod
from pathlib import Path
from typing import Any, Optional

# from ..warnings import warn_pending_deprecated
from .file import File
from .file_handler_local import LocalFileHandler
from .file_handler_remote import RemoteFileHandler

DIRS = [
    'scratch_dir_local', 'scratch_dir_remote', 'scratch_dir_container',
    'work_dir_local', 'work_dir_remote', 'work_dir_container',
    'data_dir_local', 'data_dir_container', 'data_dir_remote',
    'submit_dir_local', 'submit_dir_remote'
]

CONNECT_KEYS = [
    'host', 'user', 'port', 'config', 'gateway', 'forward_agend',
    'connect_timeout', 'connect_kwargs', 'inline_ssh_env'
]


class FileHandler(LocalFileHandler, RemoteFileHandler):
    """File handler."""

    def __init__(self, run_settings: Any):
        """Initialize."""
        self.run_settings = run_settings
        self.debug = getattr(self.run_settings, 'debug', False)

        if hasattr(self.run_settings, 'use_relative_paths'):
            self.use_relative_paths = self.run_settings.use_relative_paths
        else:
            self.use_relative_paths = False

        self.dirs = {k: getattr(self.run_settings, k, None) for k in DIRS}
        for k, v in self.dirs.items():
            if v is not None:
                self.dirs[k] = self._path(v)

        if self.use_relative_paths:
            self._gen_relative_paths()
        else:
            self._gen_absolute_paths()

        self.connection = {
            k: getattr(self.run_settings, k, None)
            for k in CONNECT_KEYS
        }
        if all(v is None for v in self.connection.values()):
            self.connection = None

    def __repr__(self):
        """Return repr(self)."""
        attrs = [(name, value) for name, value in self.__dict__.items()]
        attrs_str = ', '.join(f'{k} = {v}' for k, v in attrs)
        return f'{self.__class__.__name__}({attrs_str})'

    def _gen_relative_paths(self):
        """Generate relative paths."""
        ref_local = deepcopy(self.dirs.get('work_dir_local'))
        ref_container = deepcopy(self.dirs.get('work_dir_container'))
        ref_remote = deepcopy(self.dirs.get('work_dir_remote'))
        if isinstance(self.use_relative_paths, (str, Path)):
            raise NotImplementedError('use_relative_paths must be a bool')
        for k, v in self.dirs.items():
            if k.endswith('_local') and ref_local:
                self.dirs[k] = self._gen_relative_path(v, ref_local)
            elif k.endswith('_container') and ref_container:
                self.dirs[k] = self._gen_relative_path(v, ref_container)
            elif k.endswith('_remote') and ref_remote:
                self.dirs[k] = self._gen_relative_path(v, ref_remote)

    def _gen_absolute_paths(self):
        """Generate absoloute paths."""
        for k, v in self.dirs.items():
            if v is not None:
                path_obj = Path(v)

                if path_obj.is_absolute():
                    # If path is already absolute, leave it as is
                    absolute_path = path_obj
                else:
                    # Resolve relative paths
                    absolute_path = path_obj.resolve()

                # Update the path in dirs
                self.dirs[k] = absolute_path

    def file(self,
             name: str,
             content: Optional[str] = None,
             variables: Optional[dict] = None,
             folder: Optional[str] = None,
             **kwargs):
        """Create a file handler.

        Parameters
        ----------
        name : str
            File name.
        content : str, optional
            File content, by default None
        variables : dict, optional
            Variables to resolve in the file content, by default None
        folder : str, optional
            Folder to place the file in, by default None

        Returns
        -------
        FileHandler.File
            File handler.

        """
        file_dict = {
            'handler': self,
            'name': name,
            'content': content,
            'variables': variables,
            'folder': folder,
        }
        file_dict.update(kwargs)
        return File(**file_dict)  # type: ignore

    @singledispatchmethod
    def to_file(self, file: Any, **kwargs):
        """Convert file to File instance."""
        if file is None:
            return None
        input_dict = {}
        attrs = ['handler', 'name', 'content', 'variables', 'folder']
        for attr in attrs:
            if hasattr(file, attr):
                input_dict[attr] = getattr(file, attr)
        input_dict.update(kwargs)
        if input_dict:
            return self.file(**input_dict)
        raise ValueError('file must be a File instance')

    @to_file.register(list)
    def _(self, file: list, **kwargs):
        """Convert file to File instance."""
        for i, f in enumerate(file):
            file[i] = self.to_file(f, **kwargs)
        return file

    @to_file.register(str)
    def _(self, file: str, **kwargs):
        """Convert file to File instance."""
        return self.file(name=file, **kwargs)

    @to_file.register(tuple)
    def _(self, file: tuple, **kwargs):
        """Convert file to File instance."""
        return self.file(name=str(file[0]), content=file[1], **kwargs)

    @to_file.register(Path)
    def _(self, file: Path, **kwargs):
        """Convert file to File instance."""
        return self.file(name=file.name, **kwargs)

    @to_file.register(dict)
    def _(self, file: dict, **kwargs):
        """Convert file to File instance."""
        return self.file(**file, **kwargs)

    @to_file.register(File)
    def _(self, file: File, **kwargs):
        """Convert file to File instance."""
        file_dict = {
            'handler': self,
            'name': file.name,
            'content': file.content,
            'variables': file.variables,
            'folder': file.folder
        }
        return self.file(**file_dict, **kwargs)  # type: ignore


# if __name__ == '__main__':

#     from ..compute_settings import create_compute_settings

#     rs = create_compute_settings('local').run_settings
#     fh = FileHandler(rs)
#     f = fh.file('test.txt', 'testcontent {{var}}', {'var': 444})
#     print(f)
