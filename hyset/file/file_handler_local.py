from pathlib import Path
from string import Template
from typing import List, Optional, Union

from hytools.logger import LoggerDummy

PathLike = Union[str, Path, None]
PathLikeList = Union[PathLike, List[PathLike]]


def _path(path: PathLike) -> Optional[Path]:
    """Return Path object."""
    if path is None:
        return None
    try:
        return Path(path)
    except (OSError, TypeError):
        raise TypeError(f'could not create Path object from {path}')


class LocalFileHandler:
    """File handler for local file system."""

    def _path(self, path: Union[str, Path]) -> Path:
        """Return Path object."""
        return _path(path)

    def str_in_list_regex(self, instr: str, inlist: List[str]) -> bool:
        """Find str in list. list meight be either string or regex.

        Parameter
        ---------
        instr : str
            str to be looked for, uses '*' as bash-like wilcard
        inlist : List[str]
            list of strings that could potentially contain instr

        Returns
        -------
        bool
            if instr is in inlist

        """
        match = False
        import re
        for e in inlist:
            regex = e.replace('*', '.*')
            try:
                pattern = re.compile(regex)
                match = bool(pattern.fullmatch(instr))
            except Exception:  # Todo: specify exception
                match = (instr == e)
            if match:
                return True
        return False

    def remove_file_local(self,
                          filenames: PathLikeList,
                          recursive: Optional[bool] = False,
                          ignore: Optional[bool] = True) -> bool:
        """Delete existing file or directory.

        Parameter
        ---------
        filenames : (list of) :obj:`pathlib.Path`
            files/directories to remove
        recursive: bool, optional
            remove recursively, defaults to True
        ignore: bool, optional
            ignore if file does not exist, defaults to False

        Returns
        -------
        bool
            True if successful.

        """
        if not isinstance(filenames, list):
            filenames = [filenames]
        if len(filenames) == 0:
            return True
        filenames = [self._path(f) for f in filenames]  # type: ignore
        for f in filenames:
            if not f.exists():  # type: ignore
                if ignore:
                    continue
                else:
                    raise FileNotFoundError(f'{f} does not exist')
            if any([f.is_file(), f.is_symlink()]):  # type: ignore
                f.unlink()  # type: ignore
            elif f.is_dir():  # type: ignore
                if recursive:
                    for child in f.iterdir():  # type: ignore
                        child.unlink()
                f.rmdir()  # type: ignore
        return not all([f.exists() for f in filenames])  # type: ignore

    def exists_local(self, filename: PathLike) -> bool:
        """Check if file exists.

        Parameter
        ---------
        filename : :obj:`pathlib.Path`
            file to check

        Returns
        -------
        bool
            True if file exists.

        """
        path = self._path(filename)
        return path.exists()

    def create_dir_local(self, dir_path: Union[list, PathLike]) -> bool:
        """Create directory.

        Parameter
        ---------
        dir_path : :obj:`pathlib.Path`
            directory to create

        """
        if not isinstance(dir_path, list):
            dir_path = [dir_path]
        for d in dir_path:
            path = self._path(d)  # type: ignore
            if not path.exists():
                path.mkdir(parents=True, exist_ok=True)
        return all([d.exists() for d in dir_path])  # type: ignore

    def clean_dir_local(self,
                        dirname: Union[str, Path],
                        exceptions: Optional[Union[str, List[str]]] = None
                        ) -> None:
        """Clean directory.

        Parameter
        ---------
        path : str or :obj:`pathlib.Path`
            folder to remove
        exceptions : List[str] or str, optional
            list of exeptions, e.g. [ 'myfile', '*.inp' ]

        """
        path = self._path(dirname)  # type: ignore
        if not path.exists():
            return
        if isinstance(exceptions, str):
            exceptions = [exceptions]
        if exceptions is None:
            exceptions = []
        files_to_remove = []
        for f in list(path.iterdir()):
            keep = self.str_in_list_regex(  # type: ignore
                str(f.name), exceptions)
            if f.is_dir():
                continue
            if not keep:
                files_to_remove.append(f)
        self.remove_file_local(files_to_remove)  # type: ignore
        return

    def write_file_local(self, filename: PathLike, content: str) -> bool:
        """Write content to file.

        Parameter
        ---------
        filename : :obj:`pathlib.Path`
            file to write to
        content : str
            content to write to file

        Returns
        -------
        bool
            True if successful.

        """
        filename = self._path(filename)
        filename.write_text(content)
        return filename.exists()

    def read_file_local(self, filename: PathLike) -> str:
        """Read content from file.

        Parameter
        ---------
        filename : :obj:`pathlib.Path`
            file to read from

        Returns
        -------
        str
            File content.

        """
        filename = self._path(filename)
        return filename.read_text()

    def copy_file_local(self, src: PathLike, dst: PathLike) -> bool:
        """Copy file.

        Parameter
        ---------
        src : :obj:`pathlib.Path`
            source file
        dst : :obj:`pathlib.Path`
            destination file

        Returns
        -------
        bool
            True if successful.

        """
        src = self._path(src)  # type: ignore
        dst = self._path(dst)  # type: ignore
        dst.write_text(src.read_text())
        return dst.exists()

    def move_file_local(self, src: PathLike, dst: PathLike) -> bool:
        """Move file.

        Parameter
        ---------
        src : :obj:`pathlib.Path`
            source file
        dst : :obj:`pathlib.Path`
            destination file

        Returns
        -------
        bool
            True if successful.

        """
        src = self._path(src)  # type: ignore
        dst = self._path(dst)  # type: ignore
        src.replace(dst)  # type: ignore
        return dst.exists()

    def zip_files_local(self, filenames: PathLikeList,
                        zip_filename: PathLike) -> bool:
        """Zip files.

        Parameter
        ---------
        filenames : (list of) :obj:`pathlib.Path`
            files to zip
        zip_filename : :obj:`pathlib.Path`
            zip file

        Returns
        -------
        bool
            True if successful.

        """
        filenames = [self._path(f) for f in filenames]  # type: ignore
        zip_filename = self._path(zip_filename)  # type: ignore
        import zipfile
        with zipfile.ZipFile(zip_filename, 'w') as zip_file:
            for f in filenames:
                zip_file.write(f)  # type: ignore
        return zip_filename.exists()

    def unzip_files_local(self, zip_filename: PathLike, dst: PathLike) -> bool:
        """Unzip files.

        Parameter
        ---------
        zip_filename : :obj:`pathlib.Path`
            zip file
        dst : :obj:`pathlib.Path`
            destination directory

        Returns
        -------
        bool
            True if successful.

        """
        zip_filename = self._path(zip_filename)  # type: ignore
        dst = self._path(dst)  # type: ignore
        try:
            import zipfile
        except ImportError:
            raise ImportError('zipfile module is not available')
        with zipfile.ZipFile(zip_filename, 'r') as zip_file:
            zip_file.extractall(dst)
        return len(list(dst.iterdir())) > 0  # type: ignore

    # @app.route('/')
    def substitute_var_in_str(self, string: str, variables: dict) -> str:
        """Substitute variables in string.

        Parameter
        ---------
        string : str
            string to substitute variables in
        variables : dict
            variables to substitute

        """
        # try:
        #     from jinja2 import Environment
        # except ImportError:
        #     raise ImportError('jinja2 module is not available')

        # env = Environment(autoescape=True)
        # template = env.from_string(string)
        # return template.render(**variables)
        return Template(string).safe_substitute(**variables)

    def create_dir(self, dir_path: PathLike) -> Path:
        """Create directory.

        Parameter
        ---------
        dir_path : :obj:`pathlib.Path`
            directory to create

        """
        logger = getattr(self, 'logger', LoggerDummy())
        message = 'create_dir is deprecated, use create_dir_local instead'
        logger.warning(message)
        if self.create_dir_local(dir_path):
            return self._path(dir_path)
        raise ValueError(f'could not create {dir_path}')

    def read_file(self, filename: PathLike) -> str:
        """Read content from file.

        Parameter
        ---------
        filename : :obj:`pathlib.Path`
            file to read from

        """
        logger = getattr(self, 'logger', LoggerDummy())
        message = 'read_file is deprecated, use read_file_local instead'
        logger.warning(message)
        return self.read_file_local(filename)

    def write_file(self, filename: PathLike, content: str) -> Path:
        """Write content to file.

        Parameter
        ---------
        filename : :obj:`pathlib.Path`
            file to write to
        content : str
            content to write to file

        """
        logger = getattr(self, 'logger', LoggerDummy())
        message = 'write_file is deprecated, use write_file_local instead'
        logger.warning(message)
        if self.write_file_local(filename, content):
            return self._path(filename)
        raise ValueError(f'could not write to {filename}')

    def remove_file(self,
                    filenames: PathLikeList,
                    recursive: Optional[bool] = False,
                    ignore: Optional[bool] = True) -> None:
        """Delete existing file or directory.

        Parameter
        ---------
        filenames : (list of) :obj:`pathlib.Path`
            files/directories to remove
        recursive: bool, optional
            remove recursively, defaults to True
        ignore: bool, optional
            ignore if file does not exist, defaults to False

        """
        logger = getattr(self, 'logger', LoggerDummy())
        message = 'remove_file is deprecated, use remove_file_local instead'
        logger.warning(message)
        self.remove_file_local(filenames, recursive, ignore)

    def find_var_in_env_local(
        self,
        var: str,
        env: Optional[Union[dict, str]] = None,
        key: Optional[str] = 'PATH',
    ) -> Optional[str]:
        """Find variable in environment.

        Parameter
        ---------
        var : str
            name of variable to find
        env : dict, optional
            environment, by default None
        key : str, optional
            key to look for, by default 'PATH'

        Returns
        -------
        str
            variable value.

        """
        if isinstance(env, (str, list)):
            return var
        if env is None:
            import os
            env = os.environ.copy()

        paths = str(env.get(key, None)).split(':')  # type: ignore
        paths.append(str(Path.cwd()))
        for folder in paths:
            folder_path = self._path(folder)  # type: ignore
            if not folder_path.exists():
                continue
            for f in folder_path.iterdir():
                if f.name == var:
                    return str(f)
        return None

    def _gen_relative_path(self, path1: PathLike, path2: PathLike) -> PathLike:
        """Return relative path1 relative to path2.

        From Python3.12 : use Path.relative_to(Path, walk_up=True)
        """
        if path1 is None:
            return None
        if path2 is None:
            raise ValueError('path2 cannot be None')
        p1 = self._path(path1).resolve()
        p2 = self._path(path2).resolve()
        import os
        return self._path(os.path.relpath(p1, p2))
