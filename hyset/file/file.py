import os
from copy import deepcopy
from dataclasses import dataclass, field
from pathlib import Path
from typing import Callable, Optional, Union

# from ..warnings import warn_pending_deprecated

PathLike = Union[str, Path, None]


@dataclass
class File:
    """File handler for bookeeping."""

    handler: Optional[Callable] = field(repr=False, default=None)
    name: Optional[PathLike] = None
    file_type: Optional[str] = None
    content: Optional[str] = None
    variables: Optional[dict] = None
    folder: Optional[PathLike] = None
    subfolder: Optional[PathLike] = None

    def __repr__(self):
        """Return repr(self)."""
        attrs = [(name, value) for name, value in self.__dict__.items()
                 if name not in ['handler']]
        attrs_str = ', '.join(f'{k} = {v}' for k, v in attrs)
        return f'{self.__class__.__name__}({attrs_str})'

    def __post_init__(self):
        """Post-initialization."""
        try:
            self.path = Path(self.name)
        except (OSError, TypeError):
            raise ValueError(f'Invalid filename: {self.name}')

        # TODO: make standard
        # if '/' in str(self.name):
        #     self.folder = self.path.parent
        #     self.name = self.path.name
        #     self.path = Path(self.name)

        if self.path.is_absolute():
            # warn_pending_deprecated('Absolute paths are deprecated')

            # make realtive paths out of absolute paths
            if self.handler is not None:
                work_dir_local = self.handler.dirs['data_dir_local']

                shared_folder = Path(os.path.commonpath([self.path,
                                                         work_dir_local]))
                # find the shared folder
                # work_dir_local_parts = work_dir_local.parts
                # path_parts = self.path.parts
                # N = min(len(work_dir_local_parts), len(path_parts))
                # for index in range(min(len(work_dir_local_parts),
                # len(path_parts))):
                #     if work_dir_local_parts[index] != path_parts[index]:
                #         N = index
                #         break
                # shared_folder = Path(*work_dir_local_parts[:N])

                self.name = self.path.relative_to(shared_folder)
                self.path = Path(self.name)
            elif self.path.exists():
                cwd = Path.cwd()
                self.name = self.path.relative_to(cwd)
                self.path = Path(self.name)
            else:
                raise ValueError(f'Invalid absoloute filename: {self.name}. '
                                 'Does not exists and cannot be turned into a '
                                 'relative path.')

        if self.variables is None:
            self.variables = {}

        if self.file_type is None:
            self.file_type = self.path.suffix[1:]

        if self.handler is not None:
            for k, v in self.handler.dirs.items():
                if v is None:
                    continue
                kstr = k.replace('dir', 'path')

                if self.folder is not None:
                    parent = self.folder
                else:
                    parent = v

                if self.subfolder is not None:
                    parent = parent / self.subfolder

                setattr(self, kstr, parent / self.path)

    def change_paths(self,
                     folder: Optional[PathLike] = None,
                     subfolder: Optional[PathLike] = None) -> None:
        """Change paths."""
        self.folder = folder or self.folder
        self.subfolder = subfolder or self.subfolder
        self.__post_init__()

    def substitute_var_in_content(self,
                                  variables: Optional[dict] = None) -> str:
        """Substitute variables in the file content.

        Parameters
        ----------
        variables : Optional[dict], optional
            Variables, by default None

        Returns
        -------
        str
            Resolved content.

        """
        var = deepcopy(self.variables)
        var.update(variables or {})
        return self.handler.substitute_var_in_str(  # type: ignore
            string=self.content, variables=var)

    def send_to_remote(self, source: PathLike, dest: PathLike) -> None:
        """Send file to remote server."""
        if not self.handler.send(  # type: ignore
                local=source,
                remote=dest,
                connection=self.handler.connection):  # type: ignore
            raise ValueError('Send failed')

    def get_from_remote(self, source: PathLike, dest: PathLike) -> None:
        """Get file from remote server."""
        if not self.handler.receive(  # type: ignore
                remote=source,
                local=dest,
                connection=self.handler.connection):  # type: ignore
            raise ValueError('Get failed')

    def write(
        self,
        filename: Optional[PathLike] = None,
        remote: Optional[bool] = False,
    ) -> bool:
        """Write file content to file.

        Parameters
        ----------
        filename : Optional[PathLike], optional
            Filename, by default None
        remote: Optional[bool], optional
            Write to remote file, by default False

        Returns
        -------
        bool
            True if successful.

        """
        if filename is None:
            filename = self.handler._path(self.name)  # type: ignore

        content = self.substitute_var_in_content()

        if remote:
            return self.handler.write_file_remote(  # type: ignore
                filename=filename,
                content=content,
                connection=self.handler.connection)  # type: ignore

        return self.handler.write_file_local(  # type: ignore
            filename=filename, content=content)

    def read(self,
             filename: Optional[PathLike] = None,
             remote: Optional[bool] = False) -> None:
        """Read file content.

        Parameters
        ----------
        filename : Optional[PathLike], optional
            Filename, by default None
        remote: Optional[bool], optional
            Read from remote file, by default False

        """
        if filename is None:
            filename = self.handler._path(self.name)  # type: ignore

        if remote:
            self.content = self.handler.read_file_remote(  # type: ignore
                filename=filename,
                connection=self.handler.connection)  # type: ignore
        with open(filename, 'r') as f:  # type: ignore
            self.content = f.read()

        self.variables = None


# if __name__ == '__main__':

#     from ..compute_settings import create_compute_settings
#     from .file_handler import FileHandler
#     cs = create_compute_settings('local')
#     FH = FileHandler(cs.run_settings)  # type: ignore
#     f = FH.file('test.txt', content='{{ test }}', variables={'test': 444})
#     assert f.content == '{{ test }}'
#     f.write(f.work_path_local)
#     f.read(f.work_path_local)
#     assert f.content == '444'
