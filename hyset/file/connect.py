import time
from contextlib import suppress
from functools import singledispatch, wraps
from typing import Optional, Union

from fabric import Connection
from fabric.connection import Connection as Connection2
from paramiko.ssh_exception import SSHException

connection_type = Optional[Union[dict, Connection]]


class ConnectError(Exception):
    """Conenction error."""

    def __init__(self, message):
        """Initialize."""
        super().__init__(message)


def reconnect(function):
    """Reconnect decorator."""

    @wraps(function)
    def wrapper(*args, **kwargs):
        """Wrap function."""
        connection_in = kwargs.pop('connection', None)
        # TODO: Make exception handling more specific
        i = 0
        max_connect_attempts = 1
        while i < max_connect_attempts:
            try:
                connection = connect(connection_in)
            except (ValueError, NotImplementedError) as e:
                raise ValueError(f'{function.__name__}:\n', e)
            else:
                auto_connect = getattr(connection, 'auto_connect', False)
                max_connect_attempts = getattr(
                    connection, 'max_connect_attempts', 1
                )
            if i > 0:
                print('\n...trying to connect...')
            try:
                if not connection.is_connected:
                    if auto_connect:
                        connection = connect(connection_in)
                    else:
                        raise ConnectError(
                            f'{function.__name__}:\n' +
                            'Connection is not open' +
                            'and auto_connect is False',
                        )
            except SSHException:
                i += 1
                sleep_time = min(1**i, 300)
                print(
                    'Could not connect to remote server. '
                    f'Waiting {sleep_time} seconds before trying again.'
                )
                time.sleep(sleep_time)
                continue
            except Exception as e:
                raise ValueError(f'{function.__name__}:\n', e)
            else:
                with suppress(ConnectError, SSHException, Exception):
                    return function(*args, connection=connection, **kwargs)
        raise ConnectionError(
            f'{function.__name__}:\n',
            'Could not connect to remote server ',
            'after {max_connect_attempts} attempts',
        )

    return wrapper


@singledispatch
def connect(connection: connection_type, **kwargs) -> Connection:
    """Connect to remote server.

    Parameters
    ----------
    connection : dict or fabric.Connection, optional
        Connection (options).

    Returns
    -------
    fabric.Connection
        Connection object.

    """
    if connection is None:
        raise ValueError('connection must be a dict or fabric.Connection')
    try:
        c_dict = connection.__dict__
    except AttributeError:
        raise NotImplementedError(
            'connect is not implemented for ' + f'{type(connection)}'
        )
    else:
        return connect(dict(c_dict), **kwargs)


@connect.register(Connection)
@connect.register(Connection2)
def _(connection: Connection, **kwargs) -> Connection:
    """Connect to remote server.

    Parameters
    ----------
    connection : fabric.Connection
        Connection object.

    Returns
    -------
    fabric.Connection
        Connection object.

    """
    if kwargs.get('transfer', None) is not None:
        for k, v in kwargs['transfer'].items():
            setattr(connection, k, v)
    return connection


@connect.register(dict)
def _(connection: dict, **kwargs) -> Connection:
    """Connect to remote server.

    Parameters
    ----------
    connection : mapping
        Connection options.

    Returns
    -------
    fabric.Connection
        Connection object.

    Raises
    ------
    ValueError
        If host is not specified in options.

    """
    copt = connection.copy()
    connection_keys = [
        'host',
        'user',
        'port',
        'config',
        'gateway',
        'forward_agend',
        'connect_timeout',
        'connect_kwargs',
        'inline_ssh_env',
    ]
    transfer_keys = ['auto_connect', 'max_connect_attempts']
    transfer_values = [copt.pop(k, None) for k in transfer_keys]
    transfer = dict(zip(transfer_keys, transfer_values))
    copt = {k: copt.pop(k) for k in connection_keys if k in copt}
    if any(copt[k] == '' for k in ['host', 'user']):
        raise ValueError(
            'host and user must be specified in ' + 'connection_options'
        )
    host = copt.pop('host', None)
    if host is None:
        raise ValueError('host must be specified in ' + 'connection_options')
    return connect(Connection(host, **copt), transfer=transfer, **kwargs)
