from contextlib import contextmanager
from pathlib import Path
from typing import Optional, Union

from fabric import Connection
from hytools.logger import Logger, get_logger
from patchwork.files import exists

from .connect import connect, connection_type, reconnect
from .file_handler_local import PathLike
from .rsync import rsync


class RemoteFileHandler:
    """File handler for remote execution."""

    def __init__(self):
        """Initialize."""
        self.logger: Logger = getattr(self, 'logger', get_logger())

    @contextmanager
    def connect_to_remote(self,
                          connection: Optional[connection_type] = None
                          ) -> Connection:
        """Connect to remote server.

        Parameters
        ----------
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None

        Returns
        -------
        fabric.Connection
            Connection object.

        """
        c = connect(connection)
        c.open()
        try:
            yield c
        finally:
            c.close()

    @reconnect
    def get_cwd(self, connection: Optional[connection_type] = None) -> Path:
        """Get current working directory.

        Parameters
        ----------
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None

        Returns
        -------
        Path
            Current working directory.

        """
        cwd = connection.run('pwd', hide='both').stdout.strip()  # type: ignore
        return Path(cwd)

    @reconnect
    def exists_remote(self,
                      filename: Optional[PathLike] = None,
                      connection: Optional[connection_type] = None) -> bool:
        """Check if file exists on remote server.

        Parameters
        ----------
        filename : Optional[PathLike], optional
            Filename, by default None
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None

        Returns
        -------
        bool
            True if file exists.

        """
        if filename is None:
            raise ValueError('filename must be specified')
        return exists(connection, filename)

    @reconnect
    def read_file_remote(self,
                         filename: Optional[PathLike] = None,
                         connection: Optional[connection_type] = None) -> str:
        """Read file content from remote server.

        Parameters
        ----------
        filename : Optional[PathLike], optional
            Filename, by default None
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None

        Returns
        -------
        str
            File content.

        """
        if filename is None:
            raise ValueError('filename must be specified')

        # workaround for https://github.com/fabric/fabric/issues/1981
        if connection.is_connected:  # type: ignore
            connection.close()  # type: ignore
        connection._sftp = None  # type: ignore
        connection.open()  # type: ignore
        with connection.sftp() as sftp, sftp.open(  # type: ignore
                str(filename), 'rt') as file:  # type: ignore
            return file.read().decode()

    @reconnect
    def write_file_remote(self,
                          filename: Optional[PathLike] = None,
                          content: Optional[str] = None,
                          connection: Optional[connection_type] = None,
                          overwrite: Optional[bool] = False) -> bool:
        """Write file content to remote server.

        Parameters
        ----------
        filename : Optional[PathLike], optional
            Filename, by default None
        content : Optional[str], optional
            File content, by default None
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None
        overwrite : Optional[bool], optional
            Overwrite existing file, by default False

        Returns
        -------
        bool
            True if successful.

        """
        if filename is None:
            raise ValueError('filename must be specified')
        if exists(connection, filename):
            self.logger.debug(f'DEBUG: remote file {filename} already exists')
            # warn('remote file already exists', UserWarning)
            if not overwrite:
                return True
        # workaround for https://github.com/fabric/fabric/issues/1981
        if connection.is_connected:  # type: ignore
            connection.close()  # type: ignore
        connection._sftp = None  # type: ignore
        connection.open()  # type: ignore
        with connection.sftp() as sftp, sftp.open(  # type: ignore
                str(filename), 'wt') as file:  # type: ignore
            file.write(content)
        return exists(connection, filename)

    @reconnect
    def move_file_remote(self,
                         filename_old: Optional[PathLike] = None,
                         filename_new: Optional[PathLike] = None,
                         connection: Optional[connection_type] = None):
        """Move file on remote server.

        Parameters
        ----------
        filename_old : Optional[PathLike], optional
            Old filename, by default None
        filename_new : Optional[PathLike], optional
            New filename, by default None
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None

        Returns
        -------
        bool
            True if successful.

        """
        if filename_old is None or filename_new is None:
            raise ValueError('filename_old and filename_new must be specified')
        if exists(connection, filename_new):
            self.logger.debug(f'DEBUG: remote file {filename_new} ' +
                              'already exists')
            return False
        connection.run(f'mv {filename_old} {filename_new}')  # type: ignore
        return exists(connection, filename_new)

    @reconnect
    def create_dir_remote(
            self,
            dirname: Optional[Union[list, PathLike]] = None,
            connection: Optional[connection_type] = None) -> bool:
        """Create directory on remote server.

        Parameters
        ----------
        dirname : Optional[PathLike], optional
            Directory name, by default None
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None

        Returns
        -------
        bool
            True if successful.

        """
        if dirname is None:
            raise ValueError('dirname must be specified')
        if isinstance(dirname, list):
            dirname = ' '.join([str(d) for d in dirname])
        return connection.run(f'mkdir -p {dirname}').ok  # type: ignore

    @reconnect
    def receive(self,
                remote: Optional[PathLike] = None,
                local: Optional[PathLike] = None,
                connection: Optional[connection_type] = None,
                overwrite: Optional[bool] = False) -> bool:
        """Download file from remote server.

        Parameters
        ----------
        remote : Optional[PathLike], optional
            remote filename, by default None
        local : Optional[PathLike], optional
            local filename, by default None
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None
        overwrite : Optional[bool], optional
            Overwrite existing file, by default False

        Notes
        -----
        If local filename is not specified, the file will be downloaded to
        the current directory.

        """
        if any(x is None for x in [remote, local]):
            raise ValueError('remote and local filename must be specified')

        try:
            if Path(local).name == Path(remote).name:
                local = Path(local).parent
        except (OSError, TypeError):
            pass

        local = str(local)
        remote = str(remote)
        if ' ' in remote:
            remote_files = remote.split(' ')
        else:
            remote_files = [remote]
        local_files = [str(Path(local) / Path(f).name) for f in remote_files]

        for i, (fl, fr) in enumerate(zip(local_files, remote_files)):
            if '*' in fr:
                self.logger.warning('wildcard not supported for remote files')
                remote_files.pop(i)
                continue
            if not exists(connection, str(fr)):
                self.logger.debug(f'remote file {fr} does not exist')
                remote_files.pop(i)
                continue
            if Path(fl).exists():
                msg = f'\nWARNING: local file {fl} already exists'
                if overwrite:
                    msg += ' and will be overwritten'
                else:
                    msg += ' and will be skipped'
                    remote_files.pop(i)
                self.logger.debug(msg)

        if not remote_files:
            self.logger.debug(f'no files to receive from {remote}')
            return True
        remote = ' '.join(remote_files)
        self.logger.debug(f'receiving file(s) {remote} to {local}')
        try:
            r = rsync(connection,
                      remote,
                      local,
                      rsync_opts='-qa',
                      download=True)
        except Exception:
            with connection.sftp() as sftp:  # type: ignore
                remote_str = str(remote)
                if ' ' in remote:
                    for f in remote_str.split(' '):
                        sftp.get(f, local)
                else:
                    sftp.get(remote_str, local)
            return Path(local).exists()
        else:
            return r.return_code == 0  # type: ignore

    @reconnect
    def run_remote(self,
                   cmd: Optional[str] = None,
                   remote_dir: Optional[PathLike] = None,
                   connection: Optional[connection_type] = None) -> str:
        """Run command on remote server.

        Parameters
        ----------
        cmd : Optional[str], optional
            Command, by default None
        remote_dir : Optional[PathLike], optional
            Remote directory, by default None
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None

        Returns
        -------
        str
            Command output.

        Notes
        -----
        If remote directory is not specified, the command will be executed in
        the home directory.

        Raises
        ------
        ValueError
            If cmd is not specified.
        RuntimeError
            If command fails.

        Note
        ----
        run() calls open() if the connection is not open.

        """
        if cmd is None:
            raise ValueError('cmd must be specified')
        if remote_dir is None:
            c = connection.run(cmd, hide='stdout')  # type: ignore
            if c.ok:
                return c.stdout.strip()
            else:
                raise RuntimeError(c.stderr.strip())

        with connection.cd(remote_dir):  # type: ignore
            c = connection.run(cmd, hide='stdout')  # type: ignore
            if c.ok:
                return c.stdout.strip()
            else:
                raise RuntimeError(c.stderr.strip())

    @reconnect
    def send(self,
             local: Optional[PathLike] = None,
             remote: Optional[PathLike] = None,
             connection: Optional[connection_type] = None,
             overwrite: Optional[bool] = False) -> bool:
        """Upload file to remote server.

        Parameters
        ----------
        local : Optional[PathLike], optional
            local filename, by default None
        remote : Optional[PathLike], optional
            remote filename, by default None
        connection : fabric.Connection or dict, optional
            Connection object or options, by default None
        overwrite : Optional[bool], optional
            Overwrite existing file, by default False

        Notes
        -----
        If remote filename is not specified, the file will be downloaded to
        the home directory.

        """
        if any(x is None for x in [local, remote]):
            raise ValueError('remote and local filename must be specified')

        if Path(local).name == Path(remote).name:
            remote = Path(remote).parent

        local = str(local)
        remote = str(remote)
        if ' ' in local:
            local_files = local.split(' ')
        else:
            local_files = [local]
        remote_files = [str(Path(remote) / Path(f).name) for f in local_files]

        for i, (fl, fr) in enumerate(zip(local_files, remote_files)):
            if not Path(fl).exists():
                self.logger.debug(f'local file {fl} does not exist')
                local_files.pop(i)
                continue
            if exists(connection, str(fr)):
                msg = f'\nWARNING: remote file {fr} already exists'
                if overwrite:
                    msg += ' and will be overwritten'
                else:
                    msg += ' and will be skipped'
                    local_files.pop(i)
                self.logger.debug(msg)

        if not local_files:
            self.logger.debug(f'no files to send to {remote}')
            return True
        local = ' '.join(local_files)

        self.logger.debug(f' sending file(s) {local} to {remote}')

        try:
            self.create_dir_remote(Path(remote).parent, connection=connection)
            r = rsync(connection,
                      local,
                      remote,
                      rsync_opts='-qa',
                      download=False)
        except Exception:
            with connection.sftp() as sftp:  # type: ignore
                local_str = str(local)
                if ' ' in local:
                    for f in local_str.split(' '):
                        sftp.put(f, remote)
                else:
                    sftp.put(local_str, remote)
            return exists(connection, remote)
        else:
            return r.return_code == 0  # type: ignore


# if __name__ == '__main__':

#     import os
#     from dataclasses import dataclass
#     from datetime import datetime
#     from pathlib import Path

#     import pytz
#     user = os.environ.get('USER')
#     host = 'saga.sigma2.no'
#     @dataclass
#     class M:
#         """Mockup class for RunSettings."""
#         host: str = host
#         port: int = 22
#         user: str = user
#     # test RemoteFileHandler
#     rh = RemoteFileHandler()
#     c = rh.connect_to_remote(M)
#     assert c.is_connected
#     remote = Path()
#     local = Path()
#     # f1 = remote/ str(datetime.now().timestamp())
#     f1 = remote/ 'file_on_saga'
#     time_usa = pytz.timezone('US/Eastern')
#     # f2 = local / str(datetime.now(time_usa).timestamp())
#     f2 = local / 'local_file'
#     # assert not rh.exists_remote(f1, connection=c)
#     # assert not f2.exists()
#     template = 'Hello World'
#     rh.write_file_remote(f1, template, connection=c, overwrite=False)
#     # assert rh.exists_remote(f1, connection=c)
#     # content_remote = rh.read_file_remote(f1, connection=c)
#     # assert content_remote == template
#     # rh.receive(remote=f1, local=f2, connection=c)
#     # assert f2.exists()
#     # assert f2.read_text() == template
#     # rh.create_dir_remote(remote/'test', connection=c)
#     # assert rh.exists_remote(remote/'test', connection=c)
