import shutil
from dataclasses import dataclass, field
from pathlib import Path
from typing import List, Optional, Union

from hytools.logger import LoggerDummy

PathLike = Union[str, Path, None]


@dataclass
class ComputeSettingsConda:
    """Base class for containerization.

    Parameters
    ----------
    conda_env : Optional[str], optional
        conda environment, by default None

    """

    conda_env: Optional[str] = None
    launcher: Optional[List[str]] = field(default_factory=list)

    def _set_conda_env(self) -> None:
        """Set conda environment."""
        launcher = self.launcher.copy() or []
        logger = getattr(self, 'logger', LoggerDummy())
        path = getattr(self, 'path', [])

        if not self.conda_env:
            logger.debug('Conda environment not set in compute settings.')
            return

        if 'conda' in launcher:
            pos_env = launcher.index(self.conda_env)
            launcher = launcher[pos_env + 1:]

        conda_launcher = ['conda', 'run', '-n', self.conda_env]  # type: ignore
        launcher.extend(conda_launcher)  # type: ignore
        self.launcher = launcher
        logger.debug(f'Conda environment set: {self.conda_env}')
        logger.debug(f'Conda launcher set: {self.launcher}\n')
        try:
            t_dir = str(self.conda_env) + '/bin'
            conda_bin_dir = str(
                Path(shutil.which('python')).parents[2] / t_dir  # type: ignore
            )
        except AttributeError:
            return

        if conda_bin_dir:
            if not isinstance(path, list):
                self.path = [path]
            self.path = [conda_bin_dir] + path  # type: ignore
            logger.debug(f'Conda path set: {self.path}\n')
