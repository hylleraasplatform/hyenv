# from dataclasses import replace
import asyncio
from copy import deepcopy
from dataclasses import replace
from functools import wraps

try:
    import nest_asyncio
except ImportError:
    nest_asyncio = None

# from .abc import RunSettings
# def pre_setup(func):
#     """Decorator for performing pre-setup run_settings tasks."""
#     @wraps(func)
#     def wrapper(self, *args, **kwargs):
#         if not self.run_settings:
#             self.run_settings = self.compute_settings.run_settings
#         if kwargs.get('run_opt'):
#           self.run_settings = replace(self.run_settings, **kwargs['run_opt'])
#         return func(self, *args, **kwargs)
#     return wrapper


def pre_setup(func):
    """Perform pre-setup tasks."""
    @wraps(func)
    def wrapper(self, arg, *args, **kwargs):
        run_settings = deepcopy(self.run_settings)
        if kwargs.get('run_opt'):
            run_settings = replace(run_settings,
                                   **kwargs['run_opt'])   # type: ignore
        if arg is None:
            return run_settings
        return func(self, arg, *args, run_settings=run_settings, **kwargs)
    return wrapper


def run_sequentially(func):
    """Perform a sequence of runs."""
    @wraps(func)
    def wrapper(self, arg, *args, **kwargs):
        if not isinstance(arg, list):
            return func(self, arg, *args, **kwargs)
        results = []
        for elem in arg:
            results.append(func(self, elem, *args, **kwargs))
        return results
    return wrapper


def arun_sequentially(func):
    """Perform a sequence of runs."""
    @wraps(func)
    async def wrapper(self, arg, *args, **kwargs):
        if not isinstance(arg, list):
            return await func(self, arg, *args, **kwargs)
        results = []
        for elem in arg:
            results.append(await func(self, elem, *args, **kwargs))
        return results
    return wrapper


async def gather(*args, **kwargs):
    """Gather results."""
    return await asyncio.gather(*args, **kwargs)


def fix_opt(opt, length):
    """Fix run_opt."""
    if opt is None:
        return [None for _ in range(length)]
    if isinstance(opt, dict):
        return [opt for _ in range(length)]
    if isinstance(opt, list):
        if len(opt) == length:
            return opt
    raise TypeError('run_opt must be a dict or list of dicts of length ' +
                    f'equal to arg {length}')


def run_concurrent(func):
    """Perform a sequence of runs in concurrance."""
    @wraps(func)
    def wrapper(self, arg, *args, **kwargs):
        """Perform a sequence of runs in concurrance."""
        run_opt = fix_opt(kwargs.pop('run_opt', None), len(arg))
        program_opt = fix_opt(kwargs.pop('program_opt', None), len(arg))

        if not isinstance(arg, list):
            arg = [arg]
        if in_event_loop():
            loop = asyncio.get_running_loop()
            if nest_asyncio is None:
                raise ImportError('nest_asyncio must be installed to run ' +
                                  'concurrently in an event loop')
            nest_asyncio.apply()
            results = loop.run_until_complete(
                gather(*[func(self, a, *args,
                              program_opt=program_opt[i], run_opt=run_opt[i],
                              index=i, **kwargs) for i, a in enumerate(arg)]))
        else:
            results = asyncio.run(
                gather(*[func(self, a, *args,
                              program_opt=program_opt[i], run_opt=run_opt[i],
                              index=i, **kwargs) for i, a in enumerate(arg)]))
        return results
    return wrapper


def in_event_loop() -> bool:
    """Check if in event loop."""
    try:
        loop = asyncio.get_running_loop()
    except RuntimeError:
        return False
    else:
        return loop.is_running()
        # return True
