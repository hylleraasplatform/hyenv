def warn_deprecated(msg):
    """Warn about deprecation."""
    import sys
    if not sys.warnoptions:
        import warnings
        warnings.simplefilter('always', DeprecationWarning)
        warnings.warn(msg, DeprecationWarning, stacklevel=2)


def warn_pending_deprecated(msg):
    """Warn about pending deprecation."""
    import sys
    if not sys.warnoptions:
        import warnings
        warnings.simplefilter('always', PendingDeprecationWarning)
        warnings.warn(msg, PendingDeprecationWarning, stacklevel=2)
