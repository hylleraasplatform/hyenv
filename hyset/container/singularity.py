def get_singularity_cmd(*args, **kwargs):
    """Return a command to run a Singularity container.

    Returns
    -------
    list
        Command to run a Singularity container.

    """
    raise NotImplementedError('Singularity is not implemented yet.')
