from dataclasses import dataclass, field
from pathlib import Path
from typing import List, Optional, Union

from hytools.logger import LoggerDummy

from .docker import get_docker_cmd
from .singularity import get_singularity_cmd

PathLike = Union[str, Path, None]

CMD_MAPPING = {'docker': get_docker_cmd,
               'singularity': get_singularity_cmd}


@dataclass
class ComputeSettingsContainer:
    """Base class for containerization.

    Parameters
    ----------
    container_image : Optional[str], optional
        container image, by default None
    container_executable : Optional[str], optional
        container executable, by default None
    container_mounts : Optional[dict], optional
        container mounts, by default None
    container_type : Optional[str], optional
        container type, by default None
    work_dir_container : Optional[PathLike], optional
        work directory container, by default None
    scratch_dir_container : Optional[PathLike], optional
        scratch directory container, by default None
    data_dir_container : Optional[PathLike], optional
        data directory container, by default None
    launcher : Optional[Union[str, List[str]]], optional
        launcher, by default list
    args : Optional[Union[str, List[str]]], optional
        args, by default list

    """

    container_image: Optional[str] = None
    container_executable: Optional[str] = None
    container_mounts: Optional[dict] = None
    container_type: Optional[str] = None
    #
    work_dir_container: Optional[PathLike] = None
    scratch_dir_container: Optional[PathLike] = None
    data_dir_container: Optional[PathLike] = None
    #
    launcher: Optional[List[str]] = field(default_factory=list)
    args: Optional[List[str]] = field(default_factory=list)

    def _set_container(self):
        """Set containerization."""
        logger = getattr(self, 'logger', LoggerDummy())
        self.container_type = self._get_container_type()
        if not self.container_type:
            logger.debug('Container type not set in compute settings.')
            return

        if not self.container_image:
            raise ValueError('Container image not set in compute settings.')

        self.container_executable = (self.container_executable or
                                     self.container_type)

        logger.debug('\nContainer type: %s', self.container_type)
        logger.debug('Container executable: %s', self.container_executable)
        logger.debug('Container image: %s', self.container_image)

        self._set_container_dirs()

        logger.debug('work/scratch/data directores container: ' +
                     f'{self.work_dir_container}, ' +
                     f'{self.scratch_dir_container},' +
                     f'{self.data_dir_container}'
                     )

        self.container_mounts = {
            str(self.work_dir_local): str(self.work_dir_container),
            **({str(self.scratch_dir_local): str(self.scratch_dir_container)}
               if self.scratch_dir_container else {}),
            **({str(self.data_dir_local): str(self.data_dir_container)}
               if self.data_dir_container else {}),
            **(self.container_mounts if self.container_mounts else {})}

        logger.debug('Container mounts: %s', self.container_mounts)
        logger.debug('\n')

    def _set_container_dirs(self):
        """Set directories."""
        if not hasattr(self, 'work_dir_local'):
            raise ValueError('work_dir_local not set in compute settings.')

        for k in ['work_dir_container',
                  'scratch_dir_container',
                  'data_dir_container']:
            setattr(self, k,
                    getattr(self, k) or getattr(self, k.replace('container',
                                                                'local')))

    def _get_container_type(self):
        """Set container type."""
        target = getattr(self, 'target', '')
        return (self.container_type
                if self.container_type
                else (target
                      if target in CMD_MAPPING.keys()
                      else None))

    def update_container_launcher(self):
        """Update container launcher."""
        launcher = self.launcher.copy() or []
        args = self.args.copy() or []
        logger = getattr(self, 'logger', LoggerDummy())
        cpus_per_task = getattr(self, 'cpus_per_task', None)
        env_vars = getattr(self, 'env_vars', None)

        container_type = self.container_type or self._get_container_type()
        if not container_type:
            raise ValueError(f'Invalid container type: {container_type}.')

        cmd = CMD_MAPPING[container_type](
            cpus_per_task=cpus_per_task,
            env_vars=env_vars,
            container_image=self.container_image,
            container_mounts=self.container_mounts,
            container_executable=self.container_executable,
            work_dir_container=self.work_dir_container
        )

        if self.container_executable in launcher:
            if self.container_image not in launcher:
                raise ValueError('container_image must be in launcher')
            pos_image = launcher.index(self.container_image)
            launcher = launcher[pos_image + 1:]

        launcher = cmd + launcher

        args = [str(arg) if isinstance(arg, (Path, str))
                else str(self.work_dir_container / arg.name)
                if hasattr(arg, 'name') else arg for arg in args]

        logger.debug('Container launcher: %s',
                     ' '.join(launcher[:1000]) + ' ...\n')
        logger.debug('Container args: %s', ' '.join(args[:1000]) + ' ...\n')

        self.launcher = launcher
        self.args = args
