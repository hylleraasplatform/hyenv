import json
from dataclasses import replace
from pathlib import Path, PosixPath
from typing import Callable, Optional, Union

import yaml
from hytools.logger import get_logger

from .abc import ComputeSettings, RunSettings
from .local import LocalArch, LocalRunSettings
from .remote import RemoteArch, RemoteRunSettings

# Temporary fix until inheritance is implemented
PURGE_LIST = ['work_dir', 'scratch_dir', 'data_dir', 'submit_dir',
              'path', 'lib', 'mpi_procs', 'smp_threads', 'omp_num_threads',
              'conda_env', 'image', 'container_work_dir', 'memory',
              'run_in_container', 'run_settings', 'Runner', 'host', 'user',
              'port', 'config', 'gateway', 'forward_agent', 'connect_timeout',
              'connect_kwargs', 'inline_ssh_env', 'auto_connect',
              'max_connect_attempts', 'env_vars', 'ssh_hostname',
              'ssh_username', 'dry_run', 'debug',
              'output_type']

ARCH_TYPE_MAPPING = {'local': LocalRunSettings,
                     'remote': RemoteRunSettings}

ARCH_MAPPING = {
    'local': LocalArch,
    'container': LocalArch,
    'docker': LocalArch,
    'conda': LocalArch,
    'saga': RemoteArch,
    'betzy': RemoteArch,
    'lumi': RemoteArch,
    'slurm': RemoteArch,
}


def create_run_settings(*args, **kwargs) -> RunSettings:
    """Create run settings."""
    run_opt = kwargs.pop('run_opt', {})
    if len(args) > 1:
        raise TypeError('Only one positional argument is allowed')
    if len(args) == 1:
        if isinstance(args[0], dict):
            d = args[0].copy()
        else:
            try:
                d = args[0].__dict__.copy()
            except AttributeError:  # pragma: no cover
                raise TypeError('Only dict or dataclass is allowed')
    else:
        d = kwargs.copy()
    arch_type = d.pop('arch_type', None)
    for key in PURGE_LIST:
        d.pop(key, None)
    run_settings: RunSettings = ARCH_TYPE_MAPPING[arch_type](**d)

    if run_opt:
        run_settings = replace(run_settings, **run_opt)  # type: ignore
    return run_settings


def create_compute_settings(*args, **kwargs
                            ) -> ComputeSettings:
    """Create a computational environment based on the provided architecture.

    This function takes an optional positional argument and any number of
    keyword arguments. The positional argument, if provided, should be a string
    representing the target architecture, a file name or a dictionary.
    If no positional argument is provided, the target architecture defaults to
    'local'.

    if the file ./overwrite/compute_settings.yml exists, it will
    overwrite all arguments passed to the function.

    Parameters
    ----------
    *args : tuple
        A tuple containing zero or one positional argument. If provided,
        the argument should be a string, Path or a dictionary.
    **kwargs : dict
        A dictionary containing any number of keyword arguments. These
        arguments are passed to the ComputeSettings constructor.

    Returns
    -------
    ComputeSettings
        A ComputeSettings object representing the computational
        environment.

    Raises
    ------
    TypeError
        If more than one positional argument is provided, or if the
        positional argument is not a string.
    NotImplementedError
        If the target architecture is not in the ARCH_MAPPING dictionary
        and is not a valid path to a file or a dictionary.

    """
    _validate_args(args)
    target_arch = args[0] if args else 'local'

    if isinstance(target_arch, ComputeSettings):
        return target_arch

    settings_dict = _get_settings_dict(target_arch)
    if settings_dict is not None:
        return _create_compute_settings_from_dict(settings_dict, kwargs)

    raise NotImplementedError(f'{target_arch} of type {type(target_arch)} ' +
                              'is not a valid target')


def _get_settings_dict(target_arch) -> Optional[dict]:
    """Get settings dict."""
    if overwrite_file := _get_overwrite_file(target_arch):
        return overwrite_file

    action_type = Callable[[Union[ComputeSettings, dict, str, PosixPath]],
                           Optional[dict]]
    type_action_map: dict[type, action_type] = {  # type: ignore
        # ComputeSettings: lambda x: None,
        dict: lambda x: x,  # type: ignore
        PosixPath: lambda x: _get_settings_dict_from_str(str(x)),
        str: _get_settings_dict_from_str  # type: ignore
        }
    return type_action_map.get(type(target_arch), lambda x: None)(target_arch)


def _get_settings_dict_from_str(target_arch: str) -> Optional[dict]:
    """Get settings dict from string."""
    if ((file_dict := _from_file(target_arch)) or
            (str(target_arch) in ARCH_MAPPING)):
        return file_dict if file_dict else {'target': target_arch}
    return None


def _validate_args(args) -> None:
    """Validate args."""
    if len(args) > 1 or (args and not isinstance(args[0], (str,
                                                           dict,
                                                           Path,
                                                           ComputeSettings))):
        raise TypeError('Only one positional argument is allowed and it must' +
                        ' be a str, pathlib.Path or ComputeSettings object')


def _create_compute_settings_from_dict(d: dict,
                                       d_update: dict) -> ComputeSettings:
    """Create ComputeSettings from dict."""
    target_arch = d.pop('target', d_update.pop('target', 'local')).lower()
    if target_arch not in ARCH_MAPPING:
        raise ValueError(f'{target_arch} is not a valid target')
    d.update(d_update)
    return ARCH_MAPPING[target_arch](target=target_arch, **d)


def _get_overwrite_file(target_arch) -> Optional[dict]:
    """Get overwrite file."""
    files = ['./overwrite/compute_settings.yml',
             f'./overwrite/compute_settings_{target_arch}.yml']
    for f in files:
        if d := _from_file(f):
            return d
    return None


def _from_file(filename) -> Optional[dict]:
    """Check if overwrite file exists."""
    try:
        p = Path(filename)
    except (OSError, TypeError):  # pragma: no cover
        return None
    try:
        e = p.exists()
    except (OSError, TypeError):  # pragma: no cover
        return None
    else:
        if not e:
            return None
    d = _dict_from_str(p.read_text())
    logger = get_logger(logging_level=d.get('print_level', 'WARNING'),
                        logger_name='hyset')
    logger.warning(f'Overwriting compute_settings input with {p}')
    d.update({'logger': logger})

    return d


def _dict_from_str(s: str) -> Optional[dict]:
    """Read dict from file."""
    try:
        d = yaml.safe_load(s)
    except yaml.YAMLError:
        try:
            d = json.loads(s)
        except json.JSONDecodeError:
            return None
    return d if isinstance(d, dict) else None
