from .compute_settings import ComputeSettingsBase  # noqa: F401
from .result import Result, get_result  # noqa: F401

__all__ = ['get_result']
