from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Union

from hytools.logger import Logger, get_logger

PathLike = Union[str, Path, None]


@dataclass
class ComputeSettingsLogger:
    """Base class for Logging in ComputeSettings.

    Parameters
    ----------
    print_level : str, optional
        print level, by default 'ERROR'
    logger: Logger, optional
        logger, by default None

    """

    print_level: Optional[str] = 'ERROR'
    logger: Optional[Logger] = None

    def _set_logger(self) -> None:
        """Set logger."""
        if self.logger:
            if not all(hasattr(self.logger, method) for method in ['debug',
                                                                   'info',
                                                                   'warning',
                                                                   'error',
                                                                   'critical'
                                                                   ]):
                raise ValueError('Logger does not have required methods')
            try:
                self.logger.setLevel(str(self.print_level).upper())
            except ValueError:
                self.logger.error(f'Invalid print level {self.print_level} '
                                  f'for logger {self.logger}')
            return

        self.logger = get_logger(logging_level=self.print_level,
                                 logger_name='hyset')
        self.logger.debug('Logger initialized')
