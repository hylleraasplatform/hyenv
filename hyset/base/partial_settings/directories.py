from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Union

PathLike = Union[str, Path, None]


@dataclass
class ComputeSettingsDirectories:
    """Base class for directories in ComputeSettings.

    Parameters
    ----------
    work_dir_local : PathLike, optional
        local work directory, by default None
    scratch_dir_local : PathLike, optional
        local scratch directory, by default None
    data_dir_local : PathLike, optional
        local data directory, by default None
    sub_dir : Union[bool, PathLike], optional
        sub directory, by default None

    """

    work_dir_local: Optional[PathLike] = None
    scratch_dir_local: Optional[PathLike] = None
    data_dir_local: Optional[PathLike] = None
    sub_dir: Optional[Union[bool, PathLike]] = None

    def _set_dirs(self):
        """Set directories."""
        # if self.work_dir is None or self.work_dir_local is None:
        #     self.work_dir_local = self.work_dir
        # if self.scratch_dir is None or self.scratch_dir_local is None:
        #     self.scratch_dir_local = self.scratch_dir
        # if self.data_dir is not None and self.data_dir_local is None:
        #     self.data_dir_local = self.data_dir
        arch_type = getattr(self, 'arch_type', '')

        self.work_dir_local = self.work_dir_local or Path.cwd()
        self.scratch_dir_local = self.scratch_dir_local or self.work_dir_local
        self.data_dir_local = self.data_dir_local or self.work_dir_local

        if arch_type == 'local':
            return
