import hashlib
from dataclasses import dataclass
from functools import singledispatchmethod
from pathlib import Path
from shlex import quote, split
from typing import List, Optional, Union

from hytools.logger import LoggerDummy
from yaml import dump

PathLike = Union[str, Path, None]


@dataclass
class ComputeSettingsGeneral:
    """Base class for General ComputeSettings.

    Parameters
    ----------
    output_type : str, optional
        output type, by default None
    dry_run : bool, optional
        dry run, by default None
    force_recompute : bool, optional
        force recompute, by default None
    scheduler : str, optional
        scheduler, by default None
    wait : bool, optional
        wether to wait for result, by default None
    post_cmd : Union[str, List[str]], optional
        post command, by default None
    pre_cmd : Union[str, List[str]], optional
        pre command, by default None
    connection_type: str, optional
        connection type to cluster
    connection_opt: dict, optional
        connection options

    """

    output_type: Optional[str] = None
    dry_run: Optional[bool] = None
    force_recompute: Optional[bool] = None
    scheduler: Optional[str] = None
    wait: Optional[bool] = None
    post_cmd: Optional[Union[str, List[str]]] = None
    pre_cmd: Optional[Union[str, List[str]]] = None
    connection_type: Optional[str] = None
    connection_opt: Optional[dict] = None
    # hash: Optional[str] = None

    @singledispatchmethod
    def get_hash(self, inp: Union[str, dict, list, tuple], **kwargs) -> str:
        """Get hash."""
        logger = getattr(self, 'logger', LoggerDummy())
        logger.error('Hash not implemented for type {type}.')
        return ''

    @get_hash.register(str)
    def _(self, inp: str, **kwargs) -> str:
        """Get hash."""
        return hashlib.sha256(inp.encode()).hexdigest()

    @get_hash.register(list)
    @get_hash.register(tuple)
    def _(self, inp: Union[list, tuple], **kwargs) -> str:  # type: ignore
        """Get hash."""
        return hashlib.sha256(dump(inp, sort_keys=True).encode()).hexdigest()

    @get_hash.register(dict)
    def _(self, inp: dict, **kwargs) -> str:  # type: ignore
        """Get hash."""
        if kwargs:
            inp = {k: v for k, v in inp.items() if k in kwargs}
        return hashlib.sha256(dump(inp, sort_keys=True).encode()).hexdigest()

    def _sanitize_cmd(self,
                      cmd: Optional[Union[str, List[str]]] = None
                      ) -> List[str]:
        """Sanitize command."""
        if isinstance(cmd, list):
            cmd = ' '.join(cmd)
        return split(quote(cmd)) if cmd else None

    def __post_init__(self):
        """Post initialization."""
        self.post_cmd = self._sanitize_cmd(self.post_cmd)
        self.pre_cmd = self._sanitize_cmd(self.pre_cmd)
