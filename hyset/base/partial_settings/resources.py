import os
from contextlib import suppress
from dataclasses import dataclass
from datetime import timedelta
from pathlib import Path
from typing import Optional, Union

from hytools.logger import LoggerDummy, get_logger


@dataclass
class ComputeSettingsResource:
    """Base class for computational resources.

    Parameters
    ----------
    ntasks : int, optional
        Number of tasks, by default None
    cpus_per_task : int, optional
        Number of CPUs per task, by default None
    memory_per_cpu : int, optional
        Memory per CPU, by default None
    job_time : Union[str, timedelta], optional
        Job time, by default None
    env : Union[dict, str, lst], optional
        Environment, by default None
    env_vars : Union[dict, str, list], optional
        Environment variables, by default None
    add_to_path: list
        add to PATH environment variable
    add_to_ld_library_path: list
        add to LD_LIBRARY_PATH environment variable


    """

    ntasks: Optional[int] = None
    cpus_per_task: Optional[int] = None
    memory_per_cpu: Optional[int] = None
    job_time: Optional[Union[str, timedelta]] = None
    env: Optional[Union[dict, str, list]] = None
    env_vars: Optional[Union[dict, str, list]] = None
    add_to_path: Optional[list] = None
    add_to_ld_library_path: Optional[list] = None

    def __post_init__(self):
        """Post init."""
        self.logger = getattr(self, 'logger', get_logger())

    def _set_resources(self) -> None:
        """Set resources."""
        arch_type = getattr(self, 'arch_type', '')
        logger = getattr(self, 'logger', LoggerDummy())

        self.ntasks = self.ntasks or 1
        self.cpus_per_task = self.cpus_per_task or 1

        if (not self.memory_per_cpu and arch_type in ['slurm',
                                                      'pbs',
                                                      'remote']):
            logger.error('Memory per CPU not set in compute settings.')

        self.job_time = (None if arch_type == 'local'
                         else self.job_time or timedelta(minutes=10))

    def _convert_env(self):
        """Convert env."""
        logger = getattr(self, 'logger', LoggerDummy())
        arch = getattr(self, 'arch', '')
        if isinstance(self.env, (str, Path)):
            p = False
            with suppress(OSError):
                p = Path(self.env).exists()
            if p:
                suffix = Path(self.env).suffix
                if suffix in ['.yml', '.yaml']:
                    import yaml
                    with open(self.env, 'r') as envfile:
                        self.env = yaml.safe_load(envfile)
                elif suffix in ['.json']:
                    import json
                    with open(self.env, 'r') as envfile:
                        self.env = json.load(envfile)
                else:
                    logger.error(f'File format {suffix} not supported')
        if arch in ['remote', 'slurm', 'pbs']:
            if isinstance(self.env, dict):
                self.env = [f'{k}={v}' for k, v in self.env.items()]
        elif arch == 'local':
            if isinstance(self.env, dict):
                return
            lines = self.env.split('\n')
            self.env = {}
            for line in lines:
                if '=' in line:
                    k, v = line.split('=')
                    self.env[k] = v
                else:
                    logger.error(f'{line} not a valid env. variable')

    def _gen_env_path_str(self, path_obj: Union[list, str]) -> str:
        """Generate str for pathlib env variables."""
        if isinstance(path_obj, str):
            path_obj = path_obj.split(':')
        return ':'.join(path_obj)

    def _update_env_var(self, env: dict, key: str, var: Union[str, list]):
        """Update environment variable with path."""
        if var or key in env:
            val_new = self._gen_env_path_str(var or env.pop(key, ''))
            val_old = env.get(key, '')
            env[key] = val_new if not val_old else f'{val_new}:{val_old}'

    def _set_env_variables(self):
        """Set environment variables."""
        arch_type = getattr(self, 'arch_type', 'unknown')
        if arch_type == 'local':
            default_env = os.environ.copy()
            env = self.env or default_env
            env_vars = self.env_vars or {}
            if isinstance(env_vars, list):
                # ['VAR=VAL', 'VAR2=VAL2'] convert to dict
                env_vars = {v.split('=')[0]: v.split('=')[1] for v in env_vars}

            self._update_env_var(env, 'PATH', self.add_to_path)
            self._update_env_var(env, 'LD_LIBRARY_PATH',
                                 self.add_to_ld_library_path)

            env.update(env_vars)
            self.env = env

            if self.cpus_per_task > 0:
                # env.update({'OMP_NUM_THREADS': str(self.cpus_per_task)})
                env['OMP_NUM_THREADS'] = str(self.cpus_per_task)

        elif arch_type == 'remote':
            self.env = self.env or []
            if isinstance(self.env, dict):
                self.env = [f'export {k}={v}' for k, v in self.env.items()]
            elif isinstance(self.env, str):
                self.env = self.env.split('\n')

            self.env_vars = self.env_vars or []
            if isinstance(self.env_vars, dict):
                self.env_vars = [f'export {k}={v}'
                                 for k, v in self.env_vars.items()]
            elif isinstance(self.env_vars, str):
                self.env_vars = self.env_vars.split('\n')
            self.env.extend(self.env_vars)
            if self.add_to_path:
                self.env.extend([f'export PATH={self.add_to_path}:$PATH'])
            if self.add_to_ld_library_path:
                self.env.extend(['export LD_LIBRARY_PATH=' +
                                 f'{self.add_to_ld_library_path}' +
                                 ':$LD_LIBRARY_PATH'])
            if self.cpus_per_task > 0:
                self.env.extend(
                    ['export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK'])
        else:
            raise ValueError(f'arch_type {arch_type} not supported')
            # put env_vars here

        # if lib is not None:
        #     if isinstance(lib, list):
        #         lib_lstr = [str(p) for p in lib]
        #     else:
        #         lib_lstr = [str(lib)]
        #     lib_str = ':'.join(lib_lstr)
        #     oldlib = env.get('LD_LIBRARY_PATH', '')
        #     env.update({'LD_LIBRARY_PATH': lib_str + ':' + oldlib})

        # if env_vars is not None:
        #     if 'PATH' in env_vars.keys():
        #         v = env_vars.pop('PATH')
        #         path_str =  self._gen_env_path_str(add_to_path)
        #         # sanitize
        #         if not isinstance(v, list):
        #             v = [v]
        #         paths = [p.replace(':', '') for p in v]
        #         if v:
        #             path_str = ':'.join(paths)
        #             env['PATH'] = f'{path_str}:' + env.get('PATH', '')
        #     if 'LD_LIBRARY_PATH' in env_vars.keys():
        #         v = env_vars.pop('LD_LIBRARY_PATH')
        #         env['LD_LIBRARY_PATH'] = f'{v}:' + env.get('LD_LIBRARY_PATH',
        #                                                    '')
        #     env.update(env_vars)

    def _dump_env_to_file(self,
                          env: Optional[dict] = None,
                          filename: Optional[str] = None):
        """Dump env to file."""
        filename = filename or 'env'
        try:
            import yaml as m
            suffix = 'yml'
        except ImportError:
            import json as m  # type: ignore    # noqa
            suffix = 'json'
        env = env or os.environ  # type: ignore
        env_dict = {k: v for k, v in env.items()}
        with open(f'{filename}.{suffix}', 'w') as envfile:
            m.dump(env_dict, envfile)
        if hasattr(self, 'logger'):
            self.logger.info(f'env dumped to {filename}.{suffix}')
        print(f'env dumped to {filename}.{suffix}')
