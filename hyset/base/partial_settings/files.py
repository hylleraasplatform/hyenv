from dataclasses import dataclass, field
from pathlib import Path
from typing import Dict, List, Optional, Union

from ...file import File

FileLike = Union[str, Path, File]
PathLike = Union[str, Path]


FOLDER_FILE_MAP = {
    'work_dir_local': ['output_file', 'stdin_file', 'stdout_file',
                       'stderr_file', 'files_to_write', 'files_for_restarting',
                       'files_to_parse', 'files_to_remove', 'files_to_zip',
                       'files_to_tar', 'files_to_send',
                       'files_not_to_transfer'],
    'data_dir_local': ['data_files'],
}
FOLDER_FILE_MAP['submit_dir_local'] = ['job_script']


@dataclass
class RunSettingsFiles:
    """Base class for files in RunSettings."""

    output_file: Optional[FileLike] = None
    stdout_file: Optional[FileLike] = 'stdout.out'
    stderr_file: Optional[FileLike] = 'stderr.out'
    stdin_file: Optional[FileLike] = None
    files_to_write: List[FileLike] = field(default_factory=list)
    files_for_restarting: List[FileLike] = field(default_factory=list)
    files_to_rename: List[Dict[str, FileLike]] = field(default_factory=list)
    files_to_parse: List[FileLike] = field(default_factory=list)
    files_to_remove: List[FileLike] = field(default_factory=list)
    files_to_zip: List[FileLike] = field(default_factory=list)
    files_to_tar: List[FileLike] = field(default_factory=list)
    files_to_send: list = field(default_factory=list)
    files_not_to_transfer: list = field(default_factory=list)
    data_files: List[Union[str, Path]] = field(default_factory=list)
    #
    output_folder: Optional[PathLike] = None
    tar_all_files: Optional[bool] = False
    zip_all_files: Optional[bool] = False
    use_relative_paths: Optional[Union[bool, PathLike]] = None
    overwrite_files: Optional[bool] = False

    def __post_init__(self):
        """Post initialization."""
        for fields in FOLDER_FILE_MAP.values():
            for field_name in fields:
                value = getattr(self, field_name, None)
                if value is None:
                    continue
                # If single file-like object, convert it
                if isinstance(value, (str, Path, File)):
                    setattr(self, field_name, self.get_file(value))
                # If list of files
                elif isinstance(value, list):
                    setattr(self,
                            field_name,
                            [self.get_file(v) for v in value])
        # self.files_to_rename = [
        #     {'from': self._to_file(file_dict['from']),
        #      'to': self._to_file(file_dict['to'])}
        #     for file_dict in self.files_to_rename
        # ]

    def get_file(self, file: FileLike) -> File:
        """Convert file to File instance."""
        if isinstance(file, File):
            return file
        return File(name=str(file))
