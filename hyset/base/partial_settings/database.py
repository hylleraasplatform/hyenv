from dataclasses import dataclass
from pathlib import Path
from typing import List, Optional, Union

from hydb import Database, get_database

PathLike = Union[str, Path, None]


@dataclass
class ComputeSettingsDatabase:
    """Base class for Database related stuff.

    Parameters
    ----------
    database : Union[str, Path, Databse], optional
        hydb.Database, by default None
    database_opt : dict, optional
        database options, by default None
    database_type : str, optional
        database type, by default 'python'
    database_keys : List[str], optional
        database keys, by default None

    """

    database: Optional[Union[str, Path, Database]] = None
    database_type: Optional[str] = None
    database_opt: Optional[dict] = None
    database_keys: Optional[List[str]] = None

    def _set_database(self) -> None:
        """Set database."""
        if self.database:
            database_opt = self.database_opt or {}
            database_type = self.database_type or database_opt.get('db_type',
                                                                   'python')
            self.database = get_database(self.database,
                                         db_type=database_type,
                                         logger=getattr(self, 'logger', None),
                                         **database_opt)
        # self.update_database = (bool(self.database) *
        #                         bool(self.update_database))
