from dataclasses import dataclass, field
from pathlib import Path
from typing import Optional, Union

from hytools.logger import LoggerDummy

from ..abc import ComputeSettings, RunSettings
from .partial_settings import (ComputeSettingsDatabase,
                               ComputeSettingsDirectories,
                               ComputeSettingsGeneral, ComputeSettingsLogger,
                               ComputeSettingsResource)


@dataclass
class ComputeSettingsBase(ComputeSettings,
                          ComputeSettingsGeneral,
                          ComputeSettingsResource,
                          ComputeSettingsDatabase,
                          ComputeSettingsLogger,
                          ComputeSettingsDirectories):
    """Base class for ComputeSettings."""

    target: Optional[str] = None
    run_settings: RunSettings = field(repr=False, default=None, init=False)

    # def update(self, *args, **kwargs):
    #     """Update."""
    #     d = args[0] if args else {}
    #     purge = {'run_settings', 'arch_type'}
    #     allowed = {f.name for f in fields(self)} - purge
    #     merged = {**d, **kwargs}
    #     d_update = {k: v for k, v in merged.items() if k in allowed}
    #     for k, v in d_update.items():
    #         setattr(self, k, v)

    def _set_base(self):
        """Set base."""
        self._set_logger()
        self._set_database()
        self._set_resources()
        self._set_dirs()
        self._convert_env()
        self._set_env_variables()

    def _set_run_settings(self):
        """Set run settings."""
        logger = getattr(self, 'logger', LoggerDummy())
        try:
            from ..creators import create_run_settings
        except ImportError:  # pragma: no cover
            logger.error('ImportError, create_run_settings')
        else:
            self.run_settings = create_run_settings(self)

    def dump(self, **kwargs) -> Optional[dict]:
        """Dump."""
        path_types = ['PosixPath', 'PurePath', 'Path', 'WindowsPath',
                      'PurePosixPath', 'PureWindowsPath']
        types_to_store = ['str', 'int', 'float', 'bool', 'list', 'dict',
                          'ndarray', 'tuple'] + path_types
        d = self.__dict__

        d = {k: str(v) if type(v).__name__ in path_types else v
             for k, v in d.items() if type(v).__name__ in types_to_store}
        filename = kwargs.get('filename')
        return d if not filename else self._dump_to_file(d, filename)

    def _dump_to_file(self, d: dict, filename: Union[str, Path]):
        """Dump to file."""
        logger = getattr(self, 'logger', LoggerDummy())
        logger.debug(f'Dumping settings to file {filename}')
        p = Path(filename)
        suffix = p.suffix
        with open(p, 'w') as f:
            if suffix in ['.yml', '.yaml']:
                from yaml import dump
                dump(d, f)
            elif suffix == '.json':
                import json
                json.dump(d, f)
            else:
                f.write(str(d))

    def print(self):
        """Print."""
        skip = ['run_settings']
        print('ComputeSettings:')
        for k, v in self.__dict__.items():
            if v is None or k in skip:
                continue
            print(f'{k}: {v}')
