from abc import ABC
from typing import Callable


class Runner(ABC):
    """Base class for runners."""

    run: Callable


class ComputeSettings(ABC):
    """Base class for Architectures."""


class RunSettings(ABC):
    """Base class for RunSettings."""


class ComputeResult(ABC):
    """Base class for ComputeResult."""
