# mypy: disable-error-code="misc"
import hashlib
import inspect
from dataclasses import dataclass, field
from datetime import timedelta
from pathlib import Path
from typing import List, Optional, Union

from ..abc import RunSettings
from ..base.partial_settings.files import RunSettingsFiles
from ..file import File, FileHandler, LocalFileHandler
from ..warnings import warn_pending_deprecated
from .compute_settings import RemoteArch
from .timedelta import slurmtime_to_timedelta

PathLike = Union[Path, str, None]
PathLikeList = Union[PathLike, List[PathLike]]
FileLike = Union[PathLike, File]


@dataclass
class RemoteRunSettings(RunSettings, RunSettingsFiles,
                        RemoteArch, LocalFileHandler):
    """Run settings for Slurm execution."""

    args: List[Union[str, dict, list]] = field(default_factory=list)
    program: Optional[Union[str, List[str]]] = None
    launcher: Optional[str] = 'srun'
    post_cmd: Union[str, List[str]] = None
    pre_cmd: Union[str, List[str]] = None
    job_name: Optional[str] = None
    job_time: Optional[Union[str, timedelta]] = timedelta(minutes=10)
    job_id: Optional[Union[str, int]] = None
    create_symlinks: Optional[bool] = None
    slurm_extra: Optional[List[str]] = None
    progress_bar: Optional[bool] = True
    connection: dict = field(default_factory=dict)
    work_dir_remote: Optional[PathLike] = None
    data_dir_local: Optional[PathLike] = None
    data_dir_remote: Optional[PathLike] = None
    submit_dir_local: Optional[PathLike] = None
    submit_dir_remote: Optional[PathLike] = None
    debug: Optional[bool] = False
    memory_per_cpu: Optional[int] = 2000
    slurm_account: Optional[str] = 'nn4654k'
    modules: List[str] = field(default_factory=list)
    qos_devel: Optional[bool] = False
    job_script_filename: Optional[Union[str, Path]] = None
    monitor: Optional[bool] = True
    # deprecated
    stdout_redirect: Optional[Union[str, Path]] = None

    def __post_init__(self):
        """Post-initialization."""
        self._fix_deprecated()
        if isinstance(self.job_time, str):
            self.job_time = slurmtime_to_timedelta(self.job_time)
        self._convert_args()
        if self.sub_dir:
            self.sub_dir = self.__class__._gen_subdir_name(
                self.sub_dir, self.program, self.args)
        self._gen_full_paths()
        self.file_handler = FileHandler(self)

        self._fix_files()
        self._fix_paths_in_arglist()

    def _fix_deprecated(self):
        """Fix deprecated attributes."""
        if self.stdout_file is None and self.stdout_redirect is not None:
            self.stdout_file = self.stdout_redirect
            warn_pending_deprecated(
                'stdout_redirect is deprecated, use stdout_file instead')

    @classmethod
    def from_dict(cls, env):
        """Create environment from dict."""
        return cls(**{
            k: v
            for k, v in env.items() if k in inspect.signature(cls).parameters
        })

    def _fix_paths_in_arglist(self):
        """Fix paths in arglist."""
        for i, arg in enumerate(self.args):
            if isinstance(arg, (str, Path)):
                self.args[i] = str(arg)
                continue
            elif isinstance(arg, File):
                f = self.file_handler.to_file(arg)
                if f.folder is not None:
                    self.args[i] = str(f.path)
                    continue
                self.args[i] = str(f.work_path_local)

    def _fix_files(self):
        """Fix files."""
        self.output_file = self._to_file(self.output_file)
        self.stdout_file = self._to_file(self.stdout_file)
        self.stderr_file = self._to_file(self.stderr_file)
        self.files_to_write = [self._to_file(f) for f in self.files_to_write]

        self.files_for_restarting = [
            self._to_file(f) for f in self.files_for_restarting
        ]
        self.files_to_parse = [self._to_file(f) for f in self.files_to_parse]
        self.files_to_remove = [self._to_file(f) for f in self.files_to_remove]
        self.files_to_zip = [self._to_file(f) for f in self.files_to_zip]
        self.files_to_tar = [self._to_file(f) for f in self.files_to_tar]
        self.data_files = [self._to_file(f) for f in self.data_files]
        self.files_to_send = [self._to_file(f) for f in self.files_to_send]
        self.files_to_rename = [
            {'from': self._to_file(file_dict['from']),
             'to': self._to_file(file_dict['to'])}
            for file_dict in self.files_to_rename
        ]

    def _to_file(self, file: FileLike, folder: Optional[PathLike] = None):
        """Convert file to File instance."""
        if folder is not None:
            return self.file_handler.to_file(file, folder=folder)
        return self.file_handler.to_file(file)

    def _convert_args(self):
        """Convert args to strlist."""
        if isinstance(self.args, str):
            if ' ' in self.args:
                self.args = self.args.split(' ')
                return
            self.args = [self.args]
            return

    def abs_path(self,
                 filename: Union[str, Path],
                 run_in_container: Optional[bool] = False) -> Path:
        """Return absolute path for file.

        Parameters
        ----------
        filename : Union[str, Path]
            Filename.
        run_in_container : Optional[bool], optional
            If True, return path in container, by default False

        Returns
        -------
        Path
            Absolute path.

        """
        warn_pending_deprecated('abs_path is deprecated, use File() instead')
        if run_in_container:
            return self.work_dir_local / filename  # type: ignore
        return self.work_dir_local / filename  # type: ignore

    def _gen_full_paths(self) -> None:
        """Generate full paths."""
        if isinstance(self.sub_dir, bool):
            raise TypeError('sub_dir must be a string or None, not bool')
        keys = [
            'work_dir_local', 'work_dir_remote', 'data_dir_local',
            'data_dir_remote', 'submit_dir_local', 'submit_dir_remote'
        ]
        for key in keys:
            if isinstance(getattr(self, key), (str, Path)):
                setattr(self, key, self._path(getattr(self, key)))
            elif isinstance(getattr(self, key), list):
                setattr(self, key, [self._path(p) for p in getattr(self, key)])
                if self.sub_dir is not None:
                    raise ValueError('sub_dir must be None when using lists')

        if self.sub_dir is not None:
            self.work_dir_local = self._path(
                self.work_dir_local) / self.sub_dir
            self.work_dir_remote = self._path(
                self.work_dir_remote) / self.sub_dir
            self.submit_dir_local = self._path(
                self.submit_dir_local) / self.sub_dir
            self.submit_dir_remote = self._path(
                self.submit_dir_remote) / self.sub_dir
            self.sub_dir = None

        if self.output_folder is None:
            return

        if str(self.output_folder) in str(self.work_dir_local):
            return

        self.work_dir_local = self._path(
            self.work_dir_local) / self.output_folder

    # def _gen_subdir_name(self) -> Union[bool, str, Path, None]:
    #     """Generate sub_dir name."""
    #     if isinstance(self.sub_dir, bool):
    #         if not self.program:
    #             return self.sub_dir
    #         if self.sub_dir:
    #             try:
    #                 args_hash = hashlib.sha256(' '.join(
    #                     self.args).encode()  # type: ignore
    #                                            ).hexdigest()
    #             except TypeError:
    #                 args_hash = ''
    #             finally:
    #                 return self._path(str(self.program) + '_' + args_hash)
    #     return self.sub_dir

    @classmethod
    def _gen_subdir_name(cls,
                         sub_dir: Optional[Union[bool, PathLike]] = None,
                         program: Optional[str] = None,
                         args: Optional[list] = None) -> str:
        """Generate sub_dir name."""
        if isinstance(sub_dir, (str, Path)):
            return str(sub_dir)
        if program is None:
            program = 'run'
        hashes = [str(a) for a in args if isinstance(a, (str, Path))]
        return str(program) + '_' + hashlib.sha256(
            ' '.join(hashes).encode()).hexdigest()
