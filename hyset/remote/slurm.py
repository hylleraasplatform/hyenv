from dataclasses import dataclass
from typing import List

import fabric


@dataclass
class SLURMJob:
    """SLURM job information.

    Parameters
    ----------
    id : str
        Job id.
    name : str
        Job name.
    submission_time : str
        Job submission time.
    state : str
        Job state.
    quality_of_service : str
        Job quality of service.
    elapsed_time : str
        Job elapsed time.
    time_left : str
        Job time left.

    """

    id: str
    name: str
    submission_time: str
    state: str
    quality_of_service: str
    elapsed_time: str
    time_left: str

    def __init__(self, input_string: str) -> None:
        """Initialize SLURM job information.

        Parameters
        ----------
        input_string : str
            Input string.

        """
        split_string = input_string.split()
        self.id = split_string[0]
        self.name = split_string[1]
        self.submission_time = split_string[2]
        self.state = split_string[3]
        self.quality_of_service = split_string[4]
        self.elapsed_time = split_string[5]
        self.time_left = split_string[6]

    def __str__(self) -> str:
        """Return string representation of SLURM job information.

        Returns
        -------
        str
            String representation of SLURM job information.

        """
        return (f'SLURMJob(\n'
                f'    id                 = {self.id}\n'
                f'    name               = {self.name}\n'
                f'    submission_time    = {self.submission_time}\n'
                f'    state              = {self.state}\n'
                f'    quality_of_service = {self.quality_of_service}\n'
                f'    elapsed_time       = {self.elapsed_time}\n'
                f'    time_left          = {self.time_left}\n'
                f')\n')


def fetch_all_slurm_jobs(
    ssh_host,
    ssh_username,
    slurm_username,
    connect_kwargs=None,
) -> List[SLURMJob]:
    """Fetch all active SLURM jobs from a remote host.

    Parameters
    ----------
    ssh_host : str
        SSH connection host.
    ssh_username : str
        SSH connection username.
    slurm_username : str
        SLURM username.
    connect_kwargs : dict, optional
        Keyword arguments passed to `fabric.Connection`.

    Returns
    -------
    List[SLURMJob]
        List of SLURM job objects representing each active SLURM job.

    """
    connect_kwargs = connect_kwargs or {}
    with fabric.Connection(ssh_host,
                           user=ssh_username,
                           connect_kwargs=connect_kwargs) as connection:
        # """
        # %i
        #     Job or job step id. In the case of job arrays, the job ID format
        #     will be of the form "<base_job_id>_<index>".
        # %j
        #     Job or job step name. (Valid for jobs and job steps)
        # %V
        #     The job's submission time.
        # %T
        #     Job state in extended form.
        # %q
        #     Quality of service associated with the job. (Valid for jobs only)
        # %M
        #     Time used by the job or job step in days-hours:minutes:seconds.
        #     The days and hours are printed only as needed.
        # %L
        #     Time left for the job to execute in days-hours:minutes:seconds.
        #     This value is calculated by subtracting the job's time used from
        #     its time limit.
        # """
        result = connection.run(
            f"squeue -h -u {slurm_username} -o '%i %j %V %T %q %M %L'",
            hide=True,
        )
        if result.ok:
            jobs = result.stdout.splitlines()
            return [SLURMJob(job) for job in jobs]
        else:
            raise RuntimeError(f'Failed to fetch SLURM jobs from {ssh_host}:\n'
                               f'{result.stderr}')


def fetch_job_info(
    job_id,
    ssh_host,
    ssh_username,
    slurm_username,
    connect_kwargs=None,
) -> SLURMJob:
    """Fetch SLURM job information using `sacct`.

    Parameters
    ----------
    job_id : int
        Job id.
    ssh_host : str
        SSH connection host.
    ssh_username : str
        SSH connection username.
    slurm_username : str
        SLURM username.
    connect_kwargs : dict, optional
        Keyword arguments passed to `fabric.Connection`.

    Returns
    -------
    SLURMJob
        SLURM job object.

    """
    connect_kwargs = connect_kwargs or {}
    with fabric.Connection(ssh_host,
                           user=ssh_username,
                           connect_kwargs=connect_kwargs) as connection:
        result = connection.run(
            f"sacct -j {job_id} -n -o 'JobID,JobName,Submit,State,Elapsed,TimeLimit,'",  # noqa: E501
            hide=True,
        )
        if result.ok:
            return SLURMJob(result.stdout)
        else:
            raise RuntimeError(
                f'Failed to fetch SLURM job info from {ssh_host}:\n'
                f'{result.stderr}')
