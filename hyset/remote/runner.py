# mypy: disable-error-code="attr-defined"
import asyncio
import copy
import fnmatch
import time
from copy import deepcopy
from dataclasses import replace
from datetime import datetime, timedelta
from functools import singledispatchmethod
from pathlib import Path
# from typing import TYPE_CHECKING, List, Optional, Union
from typing import List, Optional, Union

from ..abc import Runner, RunSettings
from ..file import File, FileHandler, RemoteFileHandler
from ..local.compute_result import RemoteResult
from ..run_settings import run_sequentially
from .job_info import JobInfo
from .progress_bar import ProgressBar
from .runner_bundle import SlurmBundleRunner
from .slurm import SLURMJob
from .timedelta import timedelta_to_slurmtime

# if TYPE_CHECKING:
#     from .compute_settings import SlurmArch


PathLike = Union[Path, str, None]
PathLikeList = Union[PathLike, List[PathLike]]


class SlurmRunner(Runner, RemoteFileHandler, SlurmBundleRunner):
    """Runner for machines running slurm queueing system."""

    @singledispatchmethod
    def run(self,  # type: ignore
            run_settings: Union[RunSettings, List[RunSettings]]
            ) -> Union[RemoteResult, List[RemoteResult]]:  # type: ignore
        """Run a computation on slurm host.

        Parameters
        ----------
        run_settings : RunSettings
            Run settings used for this calculation.

        Returns
        -------
        RemoteResult
            Local result object containing the result of the computation
            if the job is finished, else information about the ongoing job.

        """
        raise TypeError('run_settings must be of type RunSettings')

    @run.register(list)
    def _(self, run_settings: List[RunSettings],  # type: ignore
          **kwargs) -> List[RemoteResult]:

        # self.debug = any([rs.debug for rs in run_settings])
        progress_bar = any([rs.progress_bar for rs in run_settings])
        self._check_common_settings(run_settings)
        jobs = [JobInfo(run_settings=rs) for rs in run_settings]
        jobs_all = copy.deepcopy(jobs)
        jobs_finished = []
        jobs_exist = self._quick_return(jobs)
        if all(jobs_exist):
            return self._result(jobs)
        if any(jobs_exist):
            remote_dirs = len(list(set([job.run_settings.work_dir_remote
                                        for job in jobs]))) > 1
            if not remote_dirs:
                raise RuntimeError('all jobs have the same work_dir_remote, ' +
                                   'but some jobs are already finished. ' +
                                   'This may be due to a previous run with ' +
                                   'force_recompute=False.')

            jobs_finished = [
                job for job, exist in zip(jobs, jobs_exist) if exist
                ]
            self.logger.debug(f'jobs_finished: {jobs_finished}')
            jobs = [job for job in jobs if job not in jobs_finished]

        job_ids = [rs.job_id for rs in run_settings if rs.job_id is not None]
        job_ids = list(set(job_ids))
        if len(job_ids) > 1:
            raise ValueError('multiple job_ids not allowed')
        if len(job_ids) == 1 and job_ids[0] is not None:
            return self._check_job(jobs_all)  # type: ignore

        if progress_bar:
            job_time = sum(
                [job.run_settings.job_time.total_seconds()  # type: ignore
                 for job in jobs])
            jobs[0] = replace(jobs[0],
                              progress_bar=ProgressBar(
                                  job_time=timedelta(seconds=job_time),
                                  msg='Initializing'))

        self._create_local_dirs(jobs)
        job_name = self._gen_job_name_bundle(jobs)
        jobs = [replace(job, job_name=job_name) for job in jobs]
        job_script_name = 'job_script_' + job_name + '.sh'
        job_script = jobs[0].run_settings.file_handler.file(
            name=job_script_name,
            content=self._gen_job_script_bundle(jobs))
        self.logger.debug(f'job-script:\n {job_script}')

        self.write_file_local(job_script.submit_path_local,
                              content=job_script.content)

        for job in jobs:
            self._write_files(job.run_settings.files_to_write,
                              file_handler=job.run_settings.file_handler,
                              overwrite=job.run_settings.overwrite_files)

        if progress_bar:
            jobs[0].progress_bar._tqdm_update_description('Connecting')
        with self.connect_to_remote(jobs[0].run_settings.connection_opt) as c:

            # run_settings.files_to_send.append(job_script)

            self.logger.info(f'connection {c} established')
            jobs = [replace(job, connection=c) for job in jobs]

            self._create_remote_dirs(jobs)
            if progress_bar:
                jobs[0].progress_bar._tqdm_update_description('Sending')
            self._send_files_to_remote(jobs)
            if self.exists_remote(job_script.submit_path_remote, connection=c):
                if not jobs[0].run_settings.overwrite_files:
                    raise ValueError(
                        'job_script already exists on remote machine' +
                        ' and overwrite_files is False')
            self.send(job_script.submit_path_local,
                      jobs[0].run_settings.submit_dir_remote,
                      connection=c,
                      overwrite=jobs[0].run_settings.overwrite_files)
            if progress_bar:
                jobs[0].progress_bar._tqdm_update_description('Submitting')

            jobs[0] = self.submit(jobs[0], job_script_name=job_script_name)
            jobs = [replace(job, job_id=jobs[0].job_id) for job in jobs]
            if not jobs[0].run_settings.monitor:
                if progress_bar:
                    jobs[0].progress_bar.close()
                return self._result(jobs)
            jobs[0] = self.poke(jobs[0])
            if progress_bar:
                jobs[0].progress_bar._tqdm_update_description('Receiving')

            ref_work_dir = jobs[0].run_settings.work_dir_remote
            if all([job.run_settings.work_dir_remote == ref_work_dir
                    for job in jobs]):
                self._fetch_results(jobs[0])
            else:
                self._fetch_results(jobs)
            if progress_bar:
                jobs[0].progress_bar._tqdm_update_description('Finishing')
            self.teardown(jobs)
            if progress_bar:
                jobs[0].progress_bar.close()
            return self._result(jobs_all)

    @run.register(RunSettings)
    def _(self, run_settings: RunSettings) -> RemoteResult:  # type: ignore
        """Run a computation on slurm host.

        Parameters
        ----------
        run_settings : RunSettings
            Run settings used for this calculation.

        Returns
        -------
        RemoteResult
            Local result object containing the result of the computation
            if the job is finished, else information about the ongoing job.

        """
        # self.debug = run_settings.debug  # unproblematic wrt race conditions
        progress_bar = run_settings.progress_bar

        job = JobInfo(run_settings=run_settings)

        if self._quick_return(job):
            return self._result(job)

        if job.run_settings.job_id:
            return self._check_job(job)  # type: ignore
        if progress_bar:
            job = replace(job,
                          progress_bar=ProgressBar(job.run_settings.job_time,
                                                   msg='Initializing'))

        self._create_local_dirs(job)
        # write job_script
        job_name = self._gen_job_name(job)
        job = replace(job, job_name=job_name)
        job_script_name = 'job_script_' + job_name + '.sh'
        if run_settings.job_script_filename is not None:
            job_script_name = str(run_settings.job_script_filename)
        job_script = run_settings.file_handler.file(
            name=job_script_name,
            content=self._gen_job_script(job))

        self.logger.debug(f'job-script:\n {job_script}')

        self.write_file_local(job_script.submit_path_local,
                              content=job_script.content)

        # run_settings.files_to_send.append(job_script)
        self._write_files(
            run_settings.files_to_write,
            file_handler=run_settings.file_handler,
            overwrite=run_settings.overwrite_files)  # type: ignore

        if progress_bar:
            job.progress_bar._tqdm_update_description('Connecting')
        with self.connect_to_remote(run_settings.connection_opt) as c:
            self.logger.info(f'connection {c} established')
            job = replace(job, connection=c)
            self._create_remote_dirs(job)
            if progress_bar:
                job.progress_bar._tqdm_update_description('Sending')
            self.send(job_script.submit_path_local,
                      job.run_settings.submit_dir_remote,
                      connection=c,
                      overwrite=job.run_settings.overwrite_files)
            self._send_files_to_remote(job)
            if progress_bar:
                job.progress_bar._tqdm_update_description('Submitting')
            job = self.submit(job, job_script_name=job_script_name)
            if not run_settings.monitor:
                if progress_bar:
                    job.progress_bar.close()
                return self._result(job)
            job = self.poke(job)
            if progress_bar:
                job.progress_bar._tqdm_update_description('Receiving')
            self._fetch_results(job)
            if progress_bar:
                job.progress_bar._tqdm_update_description('Finishing')
            self.teardown(job)
            if progress_bar:
                job.progress_bar.close()
            return self._result(job)

    @singledispatchmethod
    async def arun(
            self, run_settings: Union[RunSettings, List[RunSettings]]
            ) -> Union[RemoteResult, List[RemoteResult]]:
        """Run a computation on slurm host.

        Parameters
        ----------
        run_settings : RunSettings
            Run settings used for this calculation.

        Returns
        -------
        RemoteResult
            Local result object containing the result of the computation
            if the job is finished, else information about the ongoing job.

        """
        raise TypeError('run_settings must be of type RunSettings' +
                        ' or List[RunSettings], not ' +
                        str(type(run_settings)))

    @arun.register(list)
    async def _(self,  # type: ignore
                run_settings: List[RunSettings]) -> List[RemoteResult]:

        # self.debug = any([rs.debug for rs in run_settings])
        progress_bar = any([rs.progress_bar for rs in run_settings])
        self._check_common_settings(run_settings)
        jobs: List[JobInfo] = [JobInfo(run_settings=rs) for rs in run_settings]
        jobs_all = copy.deepcopy(jobs)
        jobs_finished = []
        jobs_exist = self._quick_return(jobs)
        if all(jobs_exist):
            return self._result(jobs)
        if any(jobs_exist):
            jobs_finished = [
                job for job, exist in zip(jobs, jobs_exist) if exist
                ]
            jobs = [job for job in jobs if job not in jobs_finished]

        job_ids = [rs.job_id for rs in run_settings if rs.job_id is not None]
        job_ids = list(set(job_ids))
        if len(job_ids) > 1:
            raise ValueError('multiple job_ids not allowed')
        if len(job_ids) == 1 and job_ids[0] is not None:
            return await self.acheck_job(jobs_all)  # type: ignore

        if progress_bar:
            job_time = sum(
                [job.run_settings.job_time.total_seconds()  # type: ignore
                 for job in jobs])
            jobs[0] = replace(jobs[0],
                              progress_bar=ProgressBar(
                                  job_time=timedelta(seconds=job_time),
                                  msg='Initializing'))
        run_settings = jobs[0].run_settings  # type: ignore
        progress_bar = jobs[0].progress_bar

        self._create_local_dirs(jobs)
        job_name = self._gen_job_name_bundle(jobs)
        jobs = [replace(job, job_name=job_name) for job in jobs]
        job_script_name = 'job_script_' + job_name + '.sh'
        if run_settings.job_script_filename is not None:
            job_script_name = str(run_settings.job_script_filename)
        job_script = run_settings.file_handler.file(
            name=job_script_name,
            content=self._gen_job_script_bundle(jobs))
        self.logger.debug(f'job-script:\n {job_script}')

        self.write_file_local(job_script.submit_path_local,
                              content=job_script.content)

        for job in jobs:
            self._write_files(job.run_settings.files_to_write,
                              file_handler=job.run_settings.file_handler,
                              overwrite=job.run_settings.overwrite_files)

        if progress_bar:
            progress_bar._tqdm_update_description('Connecting')
        with self.connect_to_remote(run_settings.connection_opt) as c:

            # run_settings.files_to_send.append(job_script)

            self.logger.info(f'connection {c} established')
            jobs = [replace(job, connection=c) for job in jobs]

            self._create_remote_dirs(jobs)
            if progress_bar:
                progress_bar._tqdm_update_description('Sending')
            self._send_files_to_remote(jobs)
            if self.exists_remote(job_script.submit_path_remote, connection=c):
                if not run_settings.overwrite_files:
                    raise ValueError(
                        'job_script already exists on remote machine' +
                        ' and overwrite_files is False')
            self.send(job_script.submit_path_local,
                      run_settings.submit_dir_remote,
                      connection=c,
                      overwrite=run_settings.overwrite_files)
            if progress_bar:
                progress_bar._tqdm_update_description('Submitting')

            jobs[0] = self.submit(jobs[0], job_script_name=job_script_name)
            jobs = [replace(job, job_id=jobs[0].job_id) for job in jobs]
            if not run_settings.monitor:
                if progress_bar:
                    progress_bar.close()
                return self._result(jobs)
            jobs[0] = await self.apoke(jobs[0])
            if progress_bar:
                progress_bar._tqdm_update_description('Receiving')

            ref_work_dir = run_settings.work_dir_remote
            if all([job.run_settings.work_dir_remote == ref_work_dir
                    for job in jobs]):
                self._fetch_results(jobs[0])
            else:
                self._fetch_results(jobs)
            if progress_bar:
                progress_bar._tqdm_update_description('Finishing')
            self.teardown(jobs)
            if progress_bar:
                progress_bar.close()
            return self._result(jobs_all)

    @arun.register(RunSettings)
    async def _(self, run_settings: RunSettings  # type: ignore
                ) -> RemoteResult:
        """Run a computation on slurm host.

        Parameters
        ----------
        run_settings : RunSettings
            Run settings used for this calculation.

        Returns
        -------
        RemoteResult
            Local result object containing the result of the computation
            if the job is finished, else information about the ongoing job.

        """
        # self.debug = run_settings.debug  # unproblematic wrt race conditions
        progress_bar = run_settings.progress_bar

        job = JobInfo(run_settings=run_settings)

        if self._quick_return(job):
            return self._result(job)

        if job.run_settings.job_id:
            return await self.acheck_job(job)  # type: ignore
        if progress_bar:
            job = replace(job,
                          progress_bar=ProgressBar(job.run_settings.job_time,
                                                   msg='Initializing'))

        self._create_local_dirs(job)
        # write job_script
        job_name = self._gen_job_name(job)
        job = replace(job, job_name=job_name)
        job_script_name = 'job_script_' + job_name + '.sh'
        job_script = run_settings.file_handler.file(
            name=job_script_name,
            content=self._gen_job_script(job))

        self.write_file_local(job_script.submit_path_local,
                              content=job_script.content)
        # run_settings.files_to_send.append(job_script)
        self._write_files(
            run_settings.files_to_write,
            file_handler=run_settings.file_handler,
            overwrite=run_settings.overwrite_files)  # type: ignore

        if progress_bar:
            job.progress_bar._tqdm_update_description('Connecting')

        with self.connect_to_remote(run_settings.connection_opt) as c:

            self.logger.info(f'connection {c} established')
            job = replace(job, connection=c)
            self._create_remote_dirs(job)
            if progress_bar:
                job.progress_bar._tqdm_update_description('Sending')
            self.send(job_script.submit_path_local,
                      job.run_settings.submit_dir_remote,
                      connection=c,
                      overwrite=job.run_settings.overwrite_files)
            self._send_files_to_remote(job)
            if progress_bar:
                job.progress_bar._tqdm_update_description('Submitting')
            job = self.submit(job, job_script_name=job_script_name)
            if not run_settings.monitor:
                if progress_bar:
                    job.progress_bar.close()
                return self._result(job)
            job = await self.apoke(job)
            if progress_bar:
                job.progress_bar._tqdm_update_description('Receiving')
            self._fetch_results(job)
            if progress_bar:
                job.progress_bar._tqdm_update_description('Finishing')
            self.teardown(job)
            if progress_bar:
                job.progress_bar.close()
            return self._result(job)

    def _get_stream(self,
                    stream: Union[str, Path, File],
                    default_filename: str = '',
                    ) -> Optional[Union[str, Path]]:
        """Get stream."""
        try:
            p = stream.work_path_local  # type: ignore
        except AttributeError:
            if isinstance(stream, File):
                return stream.name
            return stream  # typ
        if not p.exists():
            return None
        # use utf-8 encoding because nequip/lighting/pytorch uses non-ascii
        # characters
        return p.read_text(
            encoding='utf-8') if p.name == default_filename else p

    def _rename_files_local(self, files_to_rename: List[dict],
                            connection: RemoteFileHandler) -> None:
        """Rename files on remote machine."""
        for d in files_to_rename:
            f = d['from']
            nf = d['to']
            if f.folder is not None:
                path = Path(f.folder) / f.name
            else:
                path = f.work_path_local  # type: ignore
            self.logger.debug(f'renaming file: {path}')
            if isinstance(nf, str):
                newpath = path.parent / nf
            else:
                if nf.folder is not None:  # type: ignore
                    newpath = Path(nf.folder) / nf.name  # type: ignore
                else:
                    newpath = nf.work_path_local  # type: ignore
            path.rename(newpath)

    def _rename_files_remote(self, files_to_rename: List[dict],
                             connection: RemoteFileHandler) -> None:
        """Rename files on remote machine."""
        for d in files_to_rename:
            f = d['from']
            nf = d['to']
            if f.folder is not None:
                path = Path(f.folder) / f.name
            else:
                path = f.work_path_remote  # type: ignore
            self.logger.debug(f'renaming file: {path}')
            if isinstance(nf, str):
                newpath = path.parent / nf
            else:
                if nf.folder is not None:  # type: ignore
                    newpath = Path(nf.folder) / nf.name  # type: ignore
                else:
                    newpath = nf.work_path_remote  # type: ignore
            moved = self.move_file_remote(path, newpath, connection)
            if not moved:
                raise RuntimeError(f'Could not rename {path} to {newpath}')

    @run_sequentially
    def _result(self, job: JobInfo) -> RemoteResult:
        """Create result object."""
        rs = job.run_settings
        if rs.output_file:
            output_file = str(
                rs.output_file.work_path_local)  # type: ignore
            job_hash = self._path(output_file).name.split('.')[0]
        else:
            output_file = None
            job_hash = None

        stdout = self._get_stream(rs.stdout_file,
                                  default_filename='stdout.out')
        stderr = self._get_stream(rs.stderr_file,
                                  default_filename='stderr.out')

        job_id = str(job.job_id)
        if '@' not in job_id:
            job_id = f'{job_id}@'
            job_id += str(getattr(job.connection, 'host', 'unknwon'))
        files_to_parse = list(rs.files_to_parse)
        for i, f in enumerate(files_to_parse):
            if isinstance(f, File) and hasattr(f, 'work_path_local'):
                files_to_parse[i] = f.work_path_local  # type: ignore
        # files_to_rename = job.run_settings.files_to_rename or []
        # for d in files_to_rename:
        #     f = d['from']
        #     nf = d['to']
        #     if f.folder is not None:
        #         path = Path(f.folder) / f.name
        #     else:
        #         path = f.work_path_local   # type: ignore
        #     if self.debug:
        #         print('DEBUG: renaming file', path)
        #     if isinstance(nf, str):
        #         newpath = path.parent / nf
        #     else:
        #         if nf.folder is not None:  # type: ignore
        #             newpath = Path(nf.folder) / nf.name  # type: ignore
        #         else:
        #             newpath = nf.work_path_local  # type: ignore
        #     path.rename(newpath)
        output_folder = rs.work_dir_local if rs.output_folder else None

        return RemoteResult(job_id=str(job_id),  # type: ignore
                            job_hash=job_hash,
                            output_file=output_file,
                            output_folder=output_folder,
                            files_to_parse=files_to_parse,
                            stdout=stdout,
                            stderr=stderr,)

    def _get_job_id(self,
                    job: JobInfo,
                    check_host: Optional[bool] = False) -> int:
        """Get job id from job/run_settings.

        Parameters
        ----------
        job : JobInfo
            JobInfo object.
        check_host : bool, optional
            Check if job was submitted to the same host as the current
            connection, by default False

        Returns
        -------
        int
            Job id. -1 if not specified.

        """
        if job.job_id is not None:
            return job.job_id
        if job.run_settings.job_id is None:
            return -1
        try:
            job_id, host = str(job.run_settings.job_id).split('@')
        except ValueError:
            job_id = job.run_settings.job_id  # type: ignore
            # host = job.run_settings.connection.host  # type: ignore
            host = job.run_settings.connection_opt.get('host', '')

        if check_host:
            # if host not in job.run_settings.connection.host:  # type: ignore
            if host not in job.run_settings.connection_opt.get('host', ''):
                raise ValueError('Job was submitted to another host')
        self.logger.info(f'job_id {job_id} on host {host}')

        return int(job_id)

    def _check_job(
            self,
            job: Union[JobInfo, List[JobInfo]],
            ) -> Union[list, RemoteResult]:
        """Check if job is finished.

        Parameters
        ----------
        job : JobInfo
            JobInfo object.

        Returns
        -------
        Optional[RemoteResult]
            RemoteResult object if job is finished, else None.

        """
        jobs = job
        if isinstance(job, list):
            jobs = job
            job = job[0]

        job_id = self._get_job_id(job, check_host=True)
        if job_id == -1:
            raise ValueError('Job id not specified')

        if job.connection is None:
            with self.connect_to_remote(job.run_settings.connection_opt) as c:
                self.logger.debug(f'_check_job: connection {c} established')
                job = replace(job, connection=c)
                return self._check_job(job)
        else:
            if not job.connection.is_connected:
                job.connection.open()
                self.logger.debug(f'_check_job: connection {job.connection} ' +
                                  'established')

        job = replace(job, job_finished=False)

        job_status = self._get_slurm_job_info(job)
        if job_status is None:
            self._fetch_results(jobs)
            self.teardown(jobs)
            return self._result(jobs)

        if not job.run_settings.monitor:
            return self._result(jobs)

        if job.run_settings.progress_bar:
            progress_bar = ProgressBar(job.run_settings.job_time,
                                       msg='Retrieving')
            job = replace(job, progress_bar=progress_bar)

        self.poke(jobs)  # type: ignore
        self._fetch_results(jobs)
        self.teardown(jobs)
        return self._result(jobs)

    async def acheck_job(
            self,
            job: Union[JobInfo, List[JobInfo]],
            ) -> Union[list, RemoteResult]:
        """Check if job is finished.

        Parameters
        ----------
        job : JobInfo
            JobInfo object.

        Returns
        -------
        Optional[RemoteResult]
            RemoteResult object if job is finished, else None.

        """
        jobs = job
        if isinstance(job, list):
            jobs = job
            job = job[0]

        job_id = self._get_job_id(job, check_host=True)
        if job_id == -1:
            raise ValueError('Job id not specified')

        if job.connection is None:
            with self.connect_to_remote(job.run_settings.connection_opt) as c:
                self.logger.debug(f'acheck_job: connection {c} established')
                job = replace(job, connection=c)
                return self._check_job(job)  # type: ignore
        else:
            if not job.connection.is_connected:
                job.connection.open()
                self.logger.debug(f'acheck_job: connection {job.connection} ' +
                                  'established')

        job = replace(job, job_finished=False)

        job_status = self._get_slurm_job_info(job)
        if job_status is None:
            self._fetch_results(jobs)
            self.teardown(jobs)
            return self._result(jobs)

        if not job.run_settings.monitor:
            return self._result(jobs)

        if job.run_settings.progress_bar:
            progress_bar = ProgressBar(job.run_settings.job_time,
                                       msg='Retrieving')
            job = replace(job, progress_bar=progress_bar)

        await self.apoke(jobs)  # type: ignore
        self._fetch_results(jobs)
        self.teardown(jobs)
        return self._result(jobs)

    @run_sequentially
    def teardown(self,
                 job: JobInfo,
                 remote_dir: Optional[PathLike] = None) -> None:
        """Teardown remote machine after computation."""
        f = getattr(job.run_settings, 'files_to_remove', [])
        for file in f:
            if isinstance(file, File):
                p = Path(file.name)
                if not p.is_absolute():
                    p = getattr(job.run_settings, 'work_dir_local', '') / p
            else:
                p = file
            p = Path(p)
            if p.exists():
                p.unlink()
            else:
                self.logger.error(f'File {p} does not exist')
        # if remote_dir is None:feils to renmae
        #     remote_dir = job.run_settings.work_dir_remote
        # cmd = f'rm {job.job_id}/* && rmdir {job.job_id}'
        # self.run_remote(cmd=cmd,
        #                 remote_dir=remote_dir,
        #                 connection=job.connection)

    async def ateardown(self, *args, **kwargs):
        """Teardown dummy."""
        return self.teardown(*args, **kwargs)

    # def _get_output_folder(self, job: JobInfo) -> Path:
    #     """Get output folder."""
    #     if of := job.run_settings.output_folder:
    #         if Path(of).is_absolute():
    #             return Path(of)
    #         return job.run_settings.work_dir_local / of
    #     return job.run_settings.work_dir_local

    @run_sequentially
    def _fetch_results(self,
                       job: JobInfo,
                       remote_dir: Optional[PathLike] = None) -> None:
        """Fetch results from remote machine."""
        if remote_dir is None:
            remote_dir = (job.run_settings.work_dir_remote /  # type: ignore
                          job.job_id)  # type: ignore
        if not self.exists_remote(remote_dir, connection=job.connection):
            raise ValueError(f'Directory {remote_dir} does not exist')
        self._rename_files_remote(job.run_settings.files_to_rename,
                                  connection=job.connection)

        files_to_get = self.run_remote(cmd=f'ls -d {remote_dir}/*',
                                       connection=job.connection)
        self.logger.debug(f'ls in the remote: {files_to_get}')
        files_to_get = files_to_get.split('\n')

        i = 0
        while True:

            if i >= len(files_to_get):
                break
            if files_to_get[i] == '':
                break
            if files_to_get[i] == ' ':
                break
            i += 1

        files_to_get = self._remove_files_from_transfer(job, files_to_get)
        if len(files_to_get) == 0:
            raise ValueError(f'No files in directory {remote_dir}')
        files_to_get = ' '.join(files_to_get[:i + 1]).strip()
        self.logger.debug(f'files to fetch: {files_to_get}')
        if not self.receive(remote=files_to_get,
                            local=job.run_settings.work_dir_local,
                            connection=job.connection,
                            overwrite=job.run_settings.overwrite_files):
            raise ValueError('No files were received from remote machine')

    def poke(self, job: JobInfo) -> JobInfo:
        """Poke the remote machine to check if the job is finished."""
        job_status = self._get_slurm_job_info(job)
        if job_status is None:
            if job.progress_bar:
                job.progress_bar._tqdm_update_to_slurm_stat(job_status,
                                                            finished=True)
            return replace(job, job_finished=True)
        time_to_sleep = 1  # seconds
        total_time = 0
        job_time: timedelta = job.run_settings.job_time  # type: ignore
        auto_timeout = job_time.total_seconds() + 3600
        connect_timeout = (
            getattr(job.connection, 'connect_timeout')
            or auto_timeout
            )
        while job_status.state in ['PENDING', 'RUNNING']:
            time.sleep(time_to_sleep)
            total_time += time_to_sleep
            job_status = self._get_slurm_job_info(job)
            self.logger.debug(f'\nDEBUG: updated job status {job_status} ' +
                              f'after {total_time} seconds (max. ' +
                              f'{connect_timeout} seconds)')
            if job_status is None:
                break
            time_to_sleep = min(time_to_sleep * 2, 60)
            if total_time > connect_timeout:
                print('WARNING: Connection timed out in {connect_timeout} '
                      'seconds. Use a larger connect_timeout value to '
                      f'avoid this. Last job status: {job_status}')
                break
            if job.progress_bar:
                job.progress_bar._tqdm_update_to_slurm_stat(job_status)

        if job_status is not None:
            if job_status.state == 'CANCELLED':
                raise ValueError('Job was cancelled')
            if job_status.state == 'FAILED':
                raise ValueError('Job failed')
            if job_status.state == 'COMPLETED':
                self.logger.info(f'Job {job.job_id} finished')
                if job.progress_bar:
                    job.progress_bar._tqdm_update_to_slurm_stat(job_status,
                                                                finished=True)
                    return replace(job, job_finished=True)
            self.logger.warning(f'DEBUG: Job {job.job_id} *not* finished, '
                                f'job status: {job_status}')
            return replace(job, job_finished=False)

        if job.progress_bar:
            job.progress_bar._tqdm_update_to_slurm_stat(job_status,
                                                        finished=True)
        return replace(job, job_finished=True)

    async def apoke(self, job: JobInfo) -> JobInfo:
        """Poke the remote machine to check if the job is finished."""
        job_status = self._get_slurm_job_info(job)
        if job_status is None:
            if job.progress_bar:
                job.progress_bar._tqdm_update_to_slurm_stat(job_status,
                                                            finished=True)
            return replace(job, job_finished=True)
        time_to_sleep = 1  # seconds
        total_time = 0
        job_time: timedelta = job.run_settings.job_time  # type: ignore
        auto_timeout = job_time.total_seconds() + 3600
        connect_timeout = (
            getattr(job.connection, 'connect_timeout')
            or auto_timeout
            )
        while job_status.state in ['PENDING', 'RUNNING']:
            await asyncio.sleep(time_to_sleep)
            total_time += time_to_sleep
            job_status = self._get_slurm_job_info(job)
            self.logger.info(f'\nDEBUG: updated job status {job_status} after '
                             f'{total_time} seconds (max. {connect_timeout} ' +
                             'seconds)')
            if job_status is None:
                break
            time_to_sleep = min(time_to_sleep * 2, 60)
            if total_time > connect_timeout:
                print('WARNING: Connection timed out in {connect_timeout} '
                      'seconds. Use a larger connect_timeout value to '
                      f'avoid this. Last job status: {job_status}')
                break
            if job.progress_bar:
                job.progress_bar._tqdm_update_to_slurm_stat(job_status)

        if job_status is not None:
            if job_status.state == 'CANCELLED':
                raise ValueError('Job was cancelled')
            if job_status.state == 'FAILED':
                raise ValueError('Job failed')
            if job_status.state == 'COMPLETED':
                self.logger.info(f'Job {job.job_id} finished')
                if job.progress_bar:
                    job.progress_bar._tqdm_update_to_slurm_stat(job_status,
                                                                finished=True)
                    return replace(job, job_finished=True)
            self.logger.warning(f'Job {job.job_id} *not* finished, '
                                f'job status: {job_status}')
            return replace(job, job_finished=False)

        if job.progress_bar:
            job.progress_bar._tqdm_update_to_slurm_stat(job_status,
                                                        finished=True)
        return replace(job, job_finished=True)

    def submit(self, job: JobInfo, job_script_name: str) -> JobInfo:
        """Submit job to remote machine."""
        # cmd = f'chmod +x {job_script_name} && '
        # cmd += f'sbatch {job_script_name}'
        cmd = f'sbatch ./{job_script_name}'
        submit_result = self.run_remote(
            cmd=cmd,
            remote_dir=job.run_settings.submit_dir_remote,
            connection=job.connection)

        job_id = submit_result.split()[-1]
        self.logger.info(f'Job submitted with job_id {job_id}')
        return replace(job, job_id=job_id)

    def _get_slurm_job_info(self, job: JobInfo) -> Optional[SLURMJob]:
        """Get SLURM job info from remote machine."""
        cmd = f'squeue -h -u {job.connection.user} '
        cmd += '-o "%i %j %V %T %q %M %L"'
        jobs = self.run_remote(cmd=cmd, connection=job.connection)
        for j in jobs.splitlines():
            if j.split()[0] == job.job_id:
                return SLURMJob(j)
        return None

    def _gen_files_to_send(self, files: list, default: str) -> str:
        """Generate a string of files to send to remote machine."""
        if files is None:
            return ''
        if len(files) == 0:
            return ''
        local_files = []
        for f in files:
            if f.folder is not None:  # type: ignore
                path = Path(f.folder) / f.name
            else:
                path = getattr(f, default)  # type: ignore
            local_files.append(str(path))
        return ' '.join(local_files)

    def _remove_files_from_transfer(self,
                                    job,
                                    files_to_transfer: list) -> list:
        """Remove files from transfer."""
        files_not_to_transfer = getattr(job.run_settings,
                                        'files_not_to_transfer', [])
        for s in files_not_to_transfer:
            ss = getattr(s, 'name', str(s))
            if '*' in s or '?' in s:
                files_to_transfer = [f for f in files_to_transfer
                                     if not fnmatch.fnmatch(
                                        getattr(f, 'name', str(f)), ss
                                        )]
            else:
                files_to_transfer = [f for f in files_to_transfer
                                     if ss not in getattr(f, 'name', str(f))]
        return files_to_transfer

    @run_sequentially
    def _send_files_to_remote(self, job: JobInfo) -> None:
        """Send files to remote machine."""
        to_write = self._gen_files_to_send(job.run_settings.files_to_send,
                                           default='work_path_local')
        to_write += ' '
        to_write += self._gen_files_to_send(job.run_settings.files_to_write,
                                            default='work_path_local')

        to_write = ' '.join(self._remove_files_from_transfer(job,
                                                             to_write.split()))

        if to_write.strip():
            self.send(to_write,
                      job.run_settings.work_dir_remote,
                      connection=job.connection,
                      overwrite=job.run_settings.overwrite_files)

        for data_file in job.run_settings.data_files:
            local_file = data_file.data_path_local  # type: ignore
            remote_file = data_file.data_path_remote  # type: ignore

            remote_folder = str(remote_file.parent)
            # Ensure is a folder for rysync to work
            if not remote_folder.endswith('/'):
                remote_folder += '/'
            self.send(local_file, remote_folder, connection=job.connection,
                      overwrite=job.run_settings.overwrite_files)

    def _gen_job_name(self, job: JobInfo) -> str:
        """Generate SLURM job name.

        Parameters
        ----------
        job : JobInfo
            Information about the job.

        Returns
        -------
        str
            Job name generated from the current time and date.

        """
        if job.run_settings.job_name is not None:
            return job.run_settings.job_name

        program = getattr(job.run_settings, 'program', 'slurm_job')
        program_sanitized = ''.join(c for c in program
                                    if c not in ('{', '}', '$'))
        fmt = '%Y-%m-%d_%H:%M:%S'
        job_name: str = (
            f'{program_sanitized}_{datetime.now().strftime(fmt)}')
        self.logger.debug(f'Generating job name: {job_name}')
        return job_name

    def _gen_job_script(self, job: JobInfo) -> str:
        """Generate SLURM job script for running the `program`."""
        run_settings = job.run_settings
        if not job.job_name:
            job = replace(job, job_name=self._gen_job_name(job))

        s = run_settings.submit_dir_remote
        job_script = '#!/bin/bash\n'
        job_script += f'#SBATCH --job-name={job.job_name}\n'
        t = timedelta_to_slurmtime(run_settings.job_time)  # type: ignore
        job_script += f'#SBATCH --time={t}\n'  # noqa: #501
        if run_settings.memory_per_cpu is not None:
            job_script += '#SBATCH '
            job_script += f'--mem-per-cpu={run_settings.memory_per_cpu}\n'
        job_script += f'#SBATCH --cpus-per-task={run_settings.cpus_per_task}\n'
        job_script += f'#SBATCH --ntasks={run_settings.ntasks}\n'
        job_script += f'#SBATCH --account={run_settings.slurm_account}\n'
        job_script += f'#SBATCH --output={s}/{job.job_name}.out\n'
        job_script += f'#SBATCH --error={s}/{job.job_name}.err\n'

        if run_settings.qos_devel:
            job_script += '#SBATCH --qos=devel\n'
        for slurm_option in run_settings.slurm_extra:
            job_script += f'#SBATCH {slurm_option}\n'
        job_script += '\nset -o errexit\n\n'
        if run_settings.modules:
            job_script += '# Load modules\n'
            for module in run_settings.modules:
                job_script += f'module load {module}\n'
        job_script += 'set -x\n\n'
        if run_settings.env:
            job_script += '# Setup environment\n'
            for env_line in run_settings.env:
                job_script += f'{env_line}\n'

        job_script += '# Create work directory\n'
        job_script += f'export SCRATCH="{run_settings.work_dir_remote}/'
        job_script += f'${{SLURM_JOB_ID}}"\n'  # noqa: F541
        job_script += f'mkdir -p ${{SCRATCH}}\n\n'  # noqa: F541

        for f in run_settings.files_to_send + run_settings.files_to_write:
            job_script += f'cp {f.work_path_remote} ${{SCRATCH}}\n'
        job_script += '\n'

        job_script += f'cd ${{SCRATCH}}\n\n'  # noqa: F541
        symlinks_created = []
        if run_settings.create_symlinks and len(run_settings.data_files) > 0:
            job_script += '# Create symlinks to run_settings files in work '
            job_script += 'directory\n'
            for f in run_settings.data_files:
                job_script += f'ln -s {f.data_path_remote} ${{SCRATCH}}\n'
                name = str(f.name)
                if '/' in name:
                    name = name.split('/')[-1]
                symlinks_created.append(name)
            job_script += '\n'
        for i, arg in enumerate(run_settings.args):
            path = self._path(arg)  # type: ignore
            if not path:
                continue
            if path.is_absolute() or (path.is_relative_to('~')  # type: ignore
                                      and '/' in str(path)):  # type: ignore
                run_settings.args[i] = f'${{SCRATCH}}/{path.name}'
        if run_settings.pre_cmd:  # type: ignore
            job_script += '\n# Pre processing\n'
            pp = run_settings.pre_cmd  # type: ignore
            pp = [pp] if isinstance(pp, str) else pp
            if '|' in pp[-1]:  # type: ignore
                job_script += f'{" ".join(pp)} | '  # noqa: F541
                # job_script += f'{" | ".join(pp)}\n'
            else:
                for cmd in pp:
                    job_script += f'{cmd}\n'

        job_script += '# Run program\n'
        job_script += f'{run_settings.launcher} {run_settings.program} '
        job_script += f'{" ".join(run_settings.args)} '  # type: ignore
        if run_settings.stdin_file:
            job_script += f'< {run_settings.stdin_file.path} '  # type: ignore
        job_script += f'> {run_settings.stdout_file.path} '  # type: ignore
        job_script += f'2> {run_settings.stderr_file.path}'  # type: ignore

        if run_settings.post_cmd:  # type: ignore
            pp = run_settings.post_cmd  # type: ignore
            pp = [pp] if isinstance(pp, str) else pp

            if '|' in pp[0]:  # type: ignore
                job_script += f' | {" ".join(pp)}\n'
            else:
                job_script += '\n\n# Post processing\n'
                job_script += '\n'.join(pp) + '\n'
        else:
            job_script += '\n'

        for symlink in symlinks_created:
            job_script += (f'rm {symlink}\n')
        return job_script

    @run_sequentially
    def _create_local_dirs(self, job: JobInfo):
        """Create directories."""
        dirs_to_create = [
            job.run_settings.work_dir_local, job.run_settings.submit_dir_local,
            job.run_settings.data_dir_local
            ]

        if job.run_settings.use_relative_paths:
            ref = Path.cwd()
            if isinstance(job.run_settings.use_relative_paths, (str, Path)):
                ref = self._path(job.run_settings.use_relative_paths)
            dirs_to_create = [
                self._gen_relative_path(d, ref) for d in dirs_to_create
                ]

        self.logger.debug(f'creating directories {dirs_to_create}')
        if not self.create_dir_local(dirs_to_create):  # type: ignore
            raise ValueError(f'Could not create {dirs_to_create}')

        # created_dirs: list = [None]
        # for d in dirs_to_create:
        #     if d in created_dirs:  # type: ignore
        #         continue
        #     if self.debug:
        #         print('\nDEBUG: creating directory', d)
        #     created = self.create_dir_local(d)
        #     if not created:
        #         raise ValueError(f'Could not create {d}')
        #     created_dirs.append(d)  # type: ignore

    def _create_remote_dirs(self, job: Union[list, JobInfo]):
        """Create remote directories."""
        if isinstance(job, list):
            dirs_to_create = []
            for j in job:
                dirs_to_create += [
                    j.run_settings.work_dir_remote,
                    j.run_settings.submit_dir_remote,
                    j.run_settings.data_dir_remote
                    ]
            dirs_to_create = list(set(dirs_to_create))
            job = job[0]
        else:
            dirs_to_create = [
                job.run_settings.work_dir_remote,
                job.run_settings.submit_dir_remote,
                job.run_settings.data_dir_remote
                ]

        c = job.connection  # type: ignore
        if job.run_settings.use_relative_paths:  # type: ignore
            ref = self.get_cwd(connection=c)
            if isinstance(job.run_settings.use_relative_paths,  # type: ignore
                          (str, Path)):
                ref = self._path(
                    job.run_settings.use_relative_paths)  # type: ignore
            dirs_to_create = [
                self._gen_relative_path(d, ref) for d in dirs_to_create
                ]

        # created_dirs: list = [None]
        # for d in dirs_to_create:
        #     if d in created_dirs:  # type: ignore
        #         continue
        #     if self.debug:
        #         print('DEBUG: creating directory', d)
        #     if not self.create_dir_remote(d, connection=c):  # type: ignore
        #         raise ValueError(f'Could not create {d}')
        #     created_dirs.append(d)  # type: ignore
        self.logger.debug(f'creating directories {dirs_to_create}')
        if not self.create_dir_remote(dirs_to_create,  # type: ignore
                                      connection=c):  # type: ignore
            raise ValueError(f'Could not create {dirs_to_create}')

    @run_sequentially
    def _quick_return(self, job: JobInfo) -> bool:
        """Check if output file exists and return if it does."""
        files_to_check = [getattr(f, 'work_path_local')
                          for f in [job.run_settings.output_file,
                                    job.run_settings.stdout_file,
                                    job.run_settings.stderr_file]
                          if getattr(f, 'work_path_local', None) is not None]
        files_to_check = [f for f in files_to_check
                          if f.name not in ['stdout.out', 'stderr.out']]
        if not any([f.exists() for f in files_to_check]):
            return False

        self.logger.warning(f'(one of) output file(s) {files_to_check} exists')

        if job.run_settings.force_recompute:
            self.logger.warning('but will be overwritten')
            return False
        else:
            self.logger.warning('and will*not* be overwritten')
            return True

    def _write_files(self,
                     files: List[File],
                     file_handler: Optional[FileHandler] = None,
                     overwrite: Optional[bool] = False):
        """Write files."""
        if files is None:
            return
        for f in files:
            if f.content is None:  # type: ignore
                continue
            if f.folder is not None:  # type: ignore
                self.create_dir_local(f.folder)  # type: ignore
                path = Path(f.folder) / f.name  # type: ignore
            else:
                path = f.work_path_local  # type: ignore
            content = (self._replace_paths_in_file_content(f,  # type: ignore
                            file_handler=file_handler))  # noqa E128
            self.logger.debug(f'writing file {path} with content\n' +
                              f'{content}')  # type: ignore
            if path.exists():  # type: ignore
                if not overwrite:
                    self.logger.warning(f'file {path} exists, ' +
                                        '*not* overwriting')
                    return
            self.write_file_local(path, content)  # type: ignore

    def _replace_paths_in_file_content(self,
                                       file: File,
                                       file_handler=None,
                                       default: Optional[str] = None) -> str:
        """Replace paths in file content."""
        default = default or 'data_path_remote'
        content = file.content
        if content is None:
            return ''

        if not file.variables:
            return content
        variables = deepcopy(file.variables or {})
        for k, v in variables.items():
            if isinstance(v, File):
                if file_handler is not None:
                    v = file_handler.to_file(v)
                v = getattr(v, default)
                variables[k] = v
        return file.handler.substitute_var_in_str(  # type: ignore
            string=content, variables=variables)

    # def change_local_paths_to_remote_paths_in_json_file(
    #     self,
    #     local_file: str,
    #     remote_dir: Union[pathlib.Path, str],
    # ) -> str:
    #     """Change local paths to remote paths if `local_file` is a JSON file.

    #     Parameters
    #     ----------
    #     local_file : str
    #          Contents of a local file. If the file is a .json file, any local
    #         paths will be changed to remote paths. If the file is not a valid
    #          .json file, the contents will be returned unchanged.
    #     remote_dir : Union[pathlib.Path, str]
    #         Remote directory to which the file will be sent.

    #     Returns
    #     -------
    #     str
    #         Contents of the new file, change if `local_file` is a JSON file,
    #         otherwise unchanged.

    #     """
    #     # cluster_work_dir = pathlib.Path('/cluster/work/users/')
    #     # cluster_work_dir = (
    #     #     cluster_work_dir
    #     #     / pathlib.Path(self.compute_settings.ssh_username)
    #     #     / 'data_files'
    #     # )
    #     # cluster_work_dir = input.data_dir

    #     # If the file has the .json extension, we assume it is a valid .json
    #     # file
    #     if not Path(local_file).suffix.lower() == '.json':

    #         # If not, we try to load the file as a JSON file and check if it
    #         # is valid
    #         try:
    #             json_data = json.loads(local_file)
    #         except json.decoder.JSONDecodeError:
    #             return local_file

    #     # If the file is a valid .json file, we change any local paths to
    #     # remote paths
    #     json_data = json.loads(local_file)

    #     # Recursively iterate over the JSON keys and values, find any values
    #     # containing paths and change them to remote paths
    #     def change_to_remote_paths(path: str) -> str:
    #        if path.startswith('/') or (path.startswith('~') and '/' in path):
    #             # return f'{cluster_work_dir}/{pathlib.Path(path).name}'
    #             return f'{remote_dir}/{Path(path).name}'
    #         return path

    #     def iterate_json_keys(data: Dict[str, Any]) -> Dict[str, Any]:
    #         for key, value in data.items():
    #             if isinstance(value, dict):
    #                 data[key] = iterate_json_keys(value)
    #             elif isinstance(value, str):
    #                 value_changed: str = change_to_remote_paths(value)
    #                 data[key] = value_changed
    #             elif isinstance(value, list):
    #                 for i, item in enumerate(value):
    #                     if isinstance(item, str):
    #                         value_changed = change_to_remote_paths(item)
    #                         data[key][i] = value_changed
    #         return data

    #     json_data = iterate_json_keys(json_data)

    #     # Dump the changed JSON to a new string
    #     json_string = json.dumps(json_data)
    #     return json_string


# if __name__ == '__main__':

#     from hyif import Orca, Xtb
#     from hyobj import Molecule

#     from .compute_settings import SlurmArch

#     cs = SlurmArch(
#         target='saga',
#         user='tilmann',
#         modules=['xtb/6.4.1-intel-2021a'],
#         # modules=['ORCA/5.0.4-gompi-2022a'],
#         connect_kwargs={'key_filename': '/Users/tilmann/.ssh/id_rsa'},
#         debug=True,
#         force_recompute=True,
#         overwrite_files=True,
#     )
#     xtb = Xtb({'check_version': False, 'parser': 'full'},
#               compute_settings=cs)
#     orca = Orca({'check_version': False},
#                 compute_settings=cs)
#     mol = Molecule('H 0 0 0\n H 0 0 1.4')

#     setup = xtb.setup(mol)
#     # r = cs.run(setup)
#     # print(r)
#     # result = xtb.parse(r)
#     # print(result)

#     # sub_dirs = ['a', 'b', 'c']
#     # setups = [orca.setup(mol, run_opt={'sub_dir': sub_dirs[i]})
#     #           for i in range(3)]
#     setups = [setup for i in range(3)]
#     r = cs.run(setups)
#     print(xtb.parse(r))

#     # import asyncio

#     # async def run_coro(mol):
#     #     setup = xtb.setup(mol)
#     #     await cs.arun(setup)

#     # async def main(mols):
#     #     await asyncio.gather(*[run_coro(mol) for mol in mols])

#     # # start the asyncio program
#     # asyncio.run(main([mol for i in range(3)]))
