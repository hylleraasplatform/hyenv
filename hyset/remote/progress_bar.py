from datetime import timedelta
from typing import List, Optional, Union

import tqdm

from .slurm import SLURMJob


def _parse_slurm_time_string(slurm_time_str: str) -> timedelta:
    """Parse a SLURM time string.

    Parse a SLURM time string in the form output from

        squeue -o '%V'

    or

        squeue -o '%L'

    and turn it into a datetime.timedelta object.


    Parameters
    ----------
    slurm_time_str : str
        SLURM time string.

    Returns
    -------
    datetime.timedelta
        Time delta object def the time in the SLURM time string.

    """
    days: str = '0'
    hours: str = '0'
    minutes: str = '0'
    seconds: str = '0'
    if '-' in slurm_time_str:
        days, slurm_time_str = slurm_time_str.split(',')
    if ':' in slurm_time_str:
        split_str: List[str] = slurm_time_str.split(':')
        if len(split_str) == 3:
            hours = split_str[0]
            minutes = split_str[1]
            seconds = split_str[2]
        elif len(split_str) == 2:
            minutes = split_str[0]
            seconds = split_str[1]
        elif len(split_str) == 1:
            seconds = split_str[0]
    return timedelta(days=int(days),
                     hours=int(hours),
                     minutes=int(minutes),
                     seconds=int(seconds))


class ProgressBar:
    """Progress bar for SLURM jobs.

    Parameters
    ----------
    job_time : str
        Job time.
    msg : Optional[str], optional
        Message to display, by default 'Initialization'

    """

    def __init__(self,
                 job_time: Union[str, timedelta],
                 msg: Optional[str] = 'Initialization') -> None:
        """Initialize progress bar."""
        if isinstance(job_time, timedelta):
            job_time = str(job_time)
        self.total_job_time: int = int(
            _parse_slurm_time_string(job_time).total_seconds())
        self.progress_bar = tqdm.tqdm(
            total=self.total_job_time,
            ncols=120,
            colour='#2b38ff',
            bar_format=(f'{{l_bar:30}}{{bar}}| {{elapsed:9}} < {job_time:9}'))
        self.progress_bar.n = 0  # type: ignore
        self.progress_bar.n_last_print = 0  # type: ignore
        self.progress_bar.desc = f'{"":<10}{msg:>20}'  # type: ignore # noqa: E501
        self.progress_bar.refresh()  # type: ignore

    def close(self) -> None:
        """Close the progress bar."""
        self.progress_bar.close()

    def _tqdm_update_description(self, msg: str) -> None:
        """Update the description of a tqdm progress bar.

        Parameters
        ----------
        msg : str
            Message to display.

        """
        self.progress_bar.desc = f'{"":<10}{msg:>20}'
        self.progress_bar.refresh()

    def _tqdm_update_to_slurm_stat(self,
                                   job: Optional[SLURMJob] = None,
                                   finished=False) -> None:
        """Update a tqdm progress bar to a specific progress value.

        Parameters
        ----------
        job : SLURMJob, optional
            SLURM job info object.
        finished : bool, optional
            Whether the job has finished, by default `False`.

        """
        if finished:
            self.progress_bar.n = self.progress_bar.total
            self.progress_bar.last_print_n = self.progress_bar.total
            self.progress_bar.refresh()
            return

        time_left_delta: timedelta = _parse_slurm_time_string(job.time_left)
        elapsed_time_delta: timedelta = _parse_slurm_time_string(
            job.elapsed_time)
        elapsed: float = float(elapsed_time_delta.total_seconds())
        total: float = float(elapsed + time_left_delta.total_seconds())
        progress: int = round(self.progress_bar.total * elapsed / total)

        self.progress_bar.n = progress
        self.progress_bar.last_print_n = progress
        if self.progress_bar.desc != f'{job.id:<10}{job.state:>20}':
            self.progress_bar.desc = f'{job.id:<10}{job.state:>20}'
        self.progress_bar.refresh()
