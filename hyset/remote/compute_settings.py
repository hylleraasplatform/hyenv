# mypy: disable-error-code="misc"
import pathlib
from dataclasses import dataclass, field
from pathlib import Path
from typing import List, Optional, Union

import paramiko
import yaml

from ..base import ComputeSettingsBase
from ..warnings import warn_pending_deprecated
from .runner import SlurmRunner
from .timedelta import slurmtime_to_timedelta

PathLike = Union[str, pathlib.Path]


def get_ssh_username_from_default_config(ssh_host: str) -> str:
    """Get SSH username from default configuration locations using paramiko.

    Parameters
    ----------
    ssh_host : str
        SSH host.

    Returns
    -------
    str
        SSH username.

    """
    ssh_config = paramiko.SSHConfig()
    user_config_file = pathlib.Path.home() / '.ssh' / 'config'
    if user_config_file.is_file():
        with user_config_file.open() as f:
            ssh_config.parse(f)
    system_config_file = pathlib.Path('/etc/ssh/ssh_config')
    if system_config_file.is_file():
        with system_config_file.open() as f:
            ssh_config.parse(f)
    return ssh_config.lookup(ssh_host)['user']


@dataclass
class RemoteArch(ComputeSettingsBase, SlurmRunner):
    """Slurm compute settings.

    Parameters
    ----------
    target : str
        Target machine.
    host : str, optional
        Hostname, by default None
    user : str, optional
        Username, by default None
    port : int, optional
        Port, by default None
    config : str, optional
        Config file, by default None
    gateway : str, optional
        Gateway, by default None
    forward_agent : bool, optional
        Forward agent, by default None
    connect_timeout : int, optional
        Connection timeout, by default None
    connect_kwargs : dict, optional
        Connection kwargs, by default None
    inline_ssh_env : bool, optional
        Inline SSH environment, by default None
    work_dir_local : PathLike, optional
        Local work directory, by default None
    work_dir_remote : PathLike, optional
        Remote work directory, by default None
    submit_dir_local : PathLike, optional
        Local submit directory, by default None
    submit_dir_remote : PathLike, optional
        Remote submit directory, by default None
    data_dir_local : PathLike, optional
        Local data directory, by default None
    data_dir_remote : PathLike, optional
        Remote data directory, by default None
    sub_dir : Union[bool, PathLike], optional
        Subdirectory, by default None
    slurm_account : str, optional
        SLURM account, by default 'nn4654k'
    modules : List[str], optional
        Modules, by default []
    job_name : str, optional
        Job name, by default None
    job_time : Union[str, timedelta], optional
        Job time, by default timedelta(minutes=10)
    memory_per_cpu : int, optional
        Memory per CPU, by default 2000
    cpus_per_task : int, optional
        CPUs per task, by default 1
    ntasks : int, optional
        Number of tasks, by default 1
    qos_devel : bool, optional
        Development QoS, by default False
    debug : bool, optional
        Debug mode, by default False
    launcher : str, optional
        Launcher, by default None
    env : List[Union[str, dict]], optional
        Environment, by default []
    env_vars : dict, optional
        Environment variables, by default None
    create_symliks : bool, optional
        Create symlinks, by default True
    slurm_extra : List[str], optional
        Extra SLURM options, by default []
    force_recompute : bool, optional
        Force recompute, by default False
    progress_bar : bool, optional
        Progress bar, by default True

    """

    arch_type: str = field(default='remote')
    # remove this,
    cluster_settings: Optional[Union[dict, PathLike]] = None
    #
    scheduler: Optional[str] = 'slurm'
    host: Optional[str] = None
    user: Optional[str] = None
    port: Optional[int] = None
    config: Optional[str] = None
    gateway: Optional[str] = None
    forward_agent: Optional[bool] = None
    connect_timeout: Optional[int] = None
    connect_kwargs: Optional[dict] = field(default_factory=dict)
    inline_ssh_env: Optional[bool] = None
    auto_connect: Optional[bool] = True
    max_connect_attempts: Optional[int] = 5
    #
    work_dir_local: Optional[PathLike] = None
    work_dir_remote: Optional[PathLike] = None
    submit_dir_local: Optional[PathLike] = None
    submit_dir_remote: Optional[PathLike] = None
    data_dir_local: Optional[PathLike] = None
    data_dir_remote: Optional[PathLike] = None
    sub_dir: Optional[Union[bool, PathLike]] = None
    #
    launcher: Optional[str] = 'srun'
    slurm_account: Optional[str] = 'nn4654k'
    modules: List[str] = field(default_factory=list)
    job_name: Optional[str] = None
    job_id: Optional[Union[int, str]] = None
    qos_devel: Optional[bool] = False
    create_symlinks: Optional[bool] = True
    slurm_extra: List[str] = field(default_factory=list)
    #
    debug: Optional[bool] = False
    progress_bar: Optional[bool] = True
    use_relative_paths: Optional[bool] = False
    post_cmd: Optional[Union[str, List[str]]] = None
    pre_cmd: Optional[Union[str, List[str]]] = None
    # soon deprecated
    ssh_hostname: Optional[str] = None
    work_dir: Optional[PathLike] = None
    scratch_dir: Optional[PathLike] = None
    ssh_username: Optional[str] = None
    submit_dir: Optional[PathLike] = None
    data_dir: Optional[PathLike] = None
    overwrite_files: Optional[bool] = False
    monitor: Optional[bool] = True

    def __post_init__(self):
        """Post initialization."""
        super().__post_init__()
        self._set_pre_config()

        self._set_base()
        self._set_scheduler()

        self._set_deprecated()
        self.host = self.host or self.cluster_settings[self.target].get('host')
        if self.host is None:
            raise ValueError(f'{self.__class__.__name__}: ' +
                             'host must be specified')

        self.connection_type = self.connection_type or 'ssh_fabric'
        # compatibility: set connection_opt from ComputeSettingsBase
        self.connection_opt = {
            'host': self.host,
            'user': self.user,
            'port': self.port,
            'config': self.config,
            'gateway': self.gateway,
            'forward_agent': self.forward_agent,
            'connect_timeout': self.connect_timeout,
            'connect_kwargs': self.connect_kwargs,
            'inline_ssh_env': self.inline_ssh_env,
            'auto_connect': self.auto_connect,
            'max_connect_attempts': self.max_connect_attempts,
        }

        self._set_dirs()

        if self.user is None:
            try:
                self.user = get_ssh_username_from_default_config(self.target)
            except Exception as e:  # Todo: fix concrete Exception
                raise ValueError(
                    f'Could not get SSH username from default config: {e}')

        if isinstance(self.job_time, str):
            self.job_time = slurmtime_to_timedelta(self.job_time)

        # compatibility
        self.Runner = SlurmRunner()
        self._set_run_settings()
        # self.run_settings = RemoteRunSettings.from_dict(self.__dict__)

    @property
    def name(self) -> str:
        """Print environment type."""
        return str(self.target)

    def _set_pre_config(self):
        """Set pre-config."""
        if self.target is None:
            raise ValueError(f'{self.__class__.__name__}: ' +
                             'target must be specified')

        self.cluster_settings = self._get_cluster_settings()
        if self.target not in self.cluster_settings:
            if not any([
                    self.host, self.submit_dir_remote, self.data_dir_remote,
                    self.work_dir_remote
            ]):
                raise ValueError(f'{self.__class__.__name__}: ' +
                                 'the following attributes must be ' +
                                 'specified  if target is not supported: ' +
                                 'host, submit_dir_remote, ' +
                                 'data_dir_remote, work_dir_remote')

    def _set_scheduler(self):
        """Set scheduler."""

    def _get_cluster_settings(self):
        """Set cluster settings."""
        if isinstance(self.cluster_settings, dict):
            return self.cluster_settings
        filename = (self.cluster_settings or
                    Path(__file__).parent / 'config' / 'sigma2.yml')
        with open(filename) as f:
            try:
                cluster_settings = yaml.safe_load(f)
            except yaml.YAMLError as e:
                raise ValueError(f'Error loading cluster settings: {e}')
            else:
                return cluster_settings

    def _set_deprecated(self):
        """Set deprecated attributes."""
        if self.ssh_hostname is not None:
            warn_pending_deprecated(
                'ssh_hostname is deprecated. Use host instead')
            self.host = self.ssh_hostname

        if self.ssh_username is not None:
            warn_pending_deprecated(
                'ssh_username is deprecated. Use user instead')
            self.user = self.ssh_username

        if self.work_dir is not None:
            warn_pending_deprecated(
                'work_dir is deprecated. Use work_dir_local instead')
            self.work_dir_local = self.work_dir

        if self.scratch_dir is not None:
            warn_pending_deprecated('scratch_dir is deprecated')

        if self.submit_dir is not None:
            warn_pending_deprecated(
                'submit_dir is deprecated. Use submit_dir_remote instead')
            self.submit_dir_remote = self.submit_dir

        if self.data_dir is not None:
            warn_pending_deprecated(
                'data_dir is deprecated. Use data_dir_remote instead')
            self.data_dir_remote = self.data_dir

    def _set_dirs(self):
        """Set directories."""
        default_dirs = {
            'work_dir_local': Path.cwd(),
            'submit_dir_local': self.work_dir_local,
            'data_dir_local': self.work_dir_local
        }

        for dir_attr, default in default_dirs.items():
            if getattr(self, dir_attr) is None:
                setattr(self, dir_attr, default)

        remote_dir_names = [
            'work_dir_remote', 'submit_dir_remote', 'data_dir_remote'
        ]
        for d in remote_dir_names:
            val = getattr(self, d) or self.cluster_settings[self.target].get(d)
            if val is None:
                continue
            val = self.substitute_var_in_str(val, self.__dict__)
            setattr(self, d, val)
            self.logger.debug(f'setting {d}: {val}')

    # def dump(self, filename: PathLike) -> None:
    #     """Save settings to file.

    #     Parameters
    #     ----------
    #     filename : PathLike
    #         filename

    #     """
    #     path = Path(filename)
    #     extension = path.suffix
    #     dumpers: Dict[str, Callable] = {'.yml': yaml.dump,
    #                                     '.yaml': yaml.dump,
    #                                     '.json': json.dump}
    #     dont_save = ['run_settings', 'Runner', 'arch_type', 'connection']
    #     d = {k: v for k, v in self.__dict__.items()
    #          if k not in dont_save and v}

    #     for k, v in d.items():
    #         if isinstance(v, Path):
    #             d[k] = str(v)
    #         if isinstance(v, timedelta):
    #             d[k] = str(v)

    #     if extension in dumpers:
    #         with open(path, 'w') as f:
    #             dumpers[extension](d, f)
    #     else:
    #         raise ValueError(
    #             f'Extension {extension} not supported, use .yml, .yaml or ' +
    #             '.json'
    #         )

    #     if extension in dumpers:
    #         with open(path, 'w') as f:
    #             dumpers[extension](d, f)
    #     else:
    #         raise ValueError(
    #             f'Extension {extension} not supported, use .yml, .yaml or ' +
    #             '.json'
    #         )

    # def _create_dirs_local(self):
    #     """Create local directories."""
    #     dir_names = ['work_dir_local', 'submit_dir_local', 'data_dir_local']
    #     dirs = [getattr(self, d) for d in dir_names]
    #     created = [None]
    #     for d in set(dirs):
    #         if d in created:
    #             continue
    #         if self.debug:
    #             print(f'Creating local directory: {d}')
    #         if not self.file_handler.create_dir_local(d):
    #             raise ValueError(f'could not create {d}')
    #         created.append(d)

    # def _create_dirs_remote(self):
    #     """Create remote directories."""
    #     dir_names = ['work_dir_remote', 'submit_dir_remote',
    # 'data_dir_remote']
    #     default_dirs = [self.substitute_var_in_str(
    #         DEFAULTS[self.target].__dict__[d], {'user': self.user})
    #         for d in dir_names]
    #     default_dirs.append(None)
    #     dirs = [getattr(self, d) for d in dir_names]

    #     for d in set(dirs):
    #         if d in default_dirs:
    #             continue
    #         if self.debug:
    #             print(f'Creating remote directory: {d}')
    #         if not self.file_handler.create_dir_remote(
    #             d, connection=self.connection):
    #             raise ValueError(f'could not create {d}')


# if __name__ == '__main__':

#     cs = SlurmArch(target='saga', user='tilmann', debug=True)
#     print(cs)
#     # cs = SlurmArch(target='lumi', user='tilmann')
