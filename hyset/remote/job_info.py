
from dataclasses import dataclass
from typing import Optional

import tqdm
from fabric.connection import Connection

from ..abc import RunSettings


@dataclass
class JobInfo:
    """Dataclass containing information about a job."""

    job_id: int = -1
    job_name: Optional[str] = None
    job_finished: bool = False
    connection: Connection = None
    progress_bar: Optional[tqdm.tqdm] = None
    run_settings: RunSettings = None
