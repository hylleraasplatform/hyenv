from datetime import datetime, timedelta
from typing import List

from ..abc import RunSettings
from ..file import LocalFileHandler
from .timedelta import timedelta_to_slurmtime


class SlurmBundleRunner(LocalFileHandler):
    """Bundle runner for SLURM."""

    def _check_key_equal(self, rs: List[RunSettings], key: str):
        """Check that a key is the same for all run_settings."""
        val = [getattr(run_settings, key, None) for run_settings in rs]
        try:
            val = set(val)  # type: ignore
        except TypeError:
            val = set([tuple(v) for v in val])  # type: ignore
        if len(val) > 1:
            raise ValueError(f'{key} must be the same for all run_settings',
                             val if len(val) < 10 else val[:10])

    def _check_key_not_equal(self, rs: List[RunSettings], key: str):
        """Check that a key is not the same for all run_settings."""
        val = [getattr(run_settings, key, None) for run_settings in rs]
        try:
            val = set(val)  # type: ignore
        except TypeError:
            val = set([tuple(v) for v in val])  # type: ignore
        if len(val) == 1:
            raise ValueError(f'{key} must be different for all run_settings')

    def _check_common_settings(self, rs: List[RunSettings]):
        """Check that the run_settings have the same common settings."""
        for key in [
                'submit_dir_remote', 'submit_dir_local', 'debug',
                'use_relative_paths', 'force_recompute', 'overwrite_files',
                'monitor', 'job_script_file', 'job_id', 'create_symlinks',
                'progress_bar'
        ]:
            self._check_key_equal(rs, key)

        # for key in ['work_dir_remote']:
        #     self._check_key_not_equal(rs, key)

    def _gen_job_script_bundle(self, jobs: list):
        """Generate SLURM job script for running the `program`."""
        njobs = range(len(jobs))
        run_settings = jobs[0].run_settings
        job_time = max([job.run_settings.job_time.total_seconds()
                        for job in jobs])
        slurm_job_time = timedelta_to_slurmtime(timedelta(seconds=job_time))
        job_name = self._gen_job_name_bundle(jobs)
        s = run_settings.submit_dir_remote

        job_script = '#!/bin/bash\n'
        job_script += f'#SBATCH --job-name={job_name}\n'
        job_script += f'#SBATCH --time={slurm_job_time}\n'
        if run_settings.memory_per_cpu is not None:
            job_script += '#SBATCH '
            job_script += f'--mem-per-cpu={run_settings.memory_per_cpu}\n'
        job_script += f'#SBATCH --cpus-per-task={run_settings.cpus_per_task}\n'
        job_script += f'#SBATCH --ntasks={run_settings.ntasks}\n'
        job_script += f'#SBATCH --account={run_settings.slurm_account}\n'
        job_script += f'#SBATCH --output={s}/{job_name}.out\n'
        job_script += f'#SBATCH --error={s}/{job_name}.err\n'

        if run_settings.qos_devel:
            job_script += '#SBATCH --qos=devel\n'
        for slurm_option in run_settings.slurm_extra:
            job_script += f'#SBATCH {slurm_option}\n'
        job_script += '\nset -o errexit\n\n'
        job_script += 'set -x\n\n'

        modules = [job.run_settings.modules for job in jobs]
        if self._check_nested_list_equal(modules, modules, 2):
            modules = [[] for _ in range(len(jobs))]
            modules[0] = jobs[0].run_settings.modules

        envs = [job.run_settings.env for job in jobs]
        if self._check_nested_list_equal(envs, envs, 2):
            envs = [[] for _ in range(len(jobs))]
            envs[0] = jobs[0].run_settings.env
        multiple_remote_dirs = (
            len(list({job.run_settings.work_dir_remote for job in jobs})) > 1
        )

        for i in njobs:

            if modules[i]:
                job_script += '# Load modules\n'
                job_script += 'module purge\n'
                for module in modules[i]:
                    job_script += f'module load {module}\n'
                job_script += '\n'

            if envs[i]:
                job_script += '# Setup environment\n'
                for env_line in envs[i]:
                    job_script += f'{env_line}\n'
                job_script += '\n'

            run_settings = jobs[i].run_settings
            job_script += f'# Run job no. {i}\n'
            job_script += f'export SCRATCH="{run_settings.work_dir_remote}/'
            job_script += f'${{SLURM_JOB_ID}}"\n'  # noqa: F541
            job_script += f'mkdir -p ${{SCRATCH}}\n'  # noqa: F541
            for f in run_settings.files_to_send + run_settings.files_to_write:
                # if f.work_path_remote.parent == run_settings.work_dir_remote:
                #     continue
                job_script += f'cp -f {f.work_path_remote} ${{SCRATCH}}\n'

            job_script += f'cd ${{SCRATCH}}\n'  # noqa: F541

            if run_settings.pre_cmd:
                job_script += '\n# Pre processing\n'
                pp = run_settings.pre_cmd  # type: ignore
                pp = [pp] if isinstance(pp, str) else pp
                for cmd in pp:
                    job_script += f'{cmd}\n'

            symlinks_created = []
            if run_settings.create_symlinks and len(
                    run_settings.data_files) > 0:
                for f in run_settings.data_files:
                    job_script += f'ln -s {f.data_path_remote} ${{SCRATCH}}\n'
                    name = str(f.name)
                    if '/' in name:
                        name = name.split('/')[-1]
                    symlinks_created.append(name)

            for i, arg in enumerate(run_settings.args):
                path = self._path(arg)
                if not path:
                    continue
                if path.is_absolute() or (path.is_relative_to('~')
                   and '/' in str(path)):
                    run_settings.args[i] = f'${{SCRATCH}}/{path.name}'

            job_script += f'{run_settings.launcher} {run_settings.program} '
            job_script += f'{" ".join(run_settings.args)} '
            if run_settings.stdin_file:
                job_script += f'< {run_settings.stdin_file.path} '
            job_script += f'>> {run_settings.stdout_file.path} '
            job_script += f'2>> {run_settings.stderr_file.path}\n'

            for symlink in symlinks_created:
                job_script += (f'rm {symlink}\n')
            if multiple_remote_dirs:
                job_script += f'cp -rf ${{SCRATCH}}/* '  # noqa: F541
                job_script += f'{run_settings.work_dir_remote}\n'

            if run_settings.post_cmd:
                job_script += '\n# Post processing\n'
                pp = run_settings.post_cmd  # type: ignore
                pp = [pp] if isinstance(pp, str) else pp
                for cmd in pp:
                    job_script += f'{cmd}\n'

            job_script += '\n'

        return job_script

    def _check_nested_list_equal(self, list1, list2, max_depth=1):
        """Check that two nested lists are equal."""
        if max_depth < 1:
            return list1 == list2
        if len(list1) != len(list2):
            return False
        for i, ll in enumerate(list1):
            if isinstance(ll, list):
                if not self._check_nested_list_equal(ll, list2[i],
                                                     max_depth - 1):
                    return False
            elif ll != list2[i]:
                return False
        return True

    def _gen_job_name_bundle(self, jobs: list) -> str:
        """Generate SLURM job name.

        Parameters
        ----------
        jobs : List[JobInfo]
            Information about the jobs

        Returns
        -------
        str
            Job name generated from the current time and date.

        """
        job_names = [job.run_settings.job_name for job in jobs]
        if len(set(job_names)) == 1:
            if job_names[0] is not None:
                return job_names[0]

        program = 'slurm_bundle'
        job_name: str = (
            f"{program}_{datetime.now().strftime('%Y-%m-%d_%H:%M:%S')}")
        self.logger.debug(f'Generating job name: {job_name}')
        return job_name
