# from __future__ import annotations

from dataclasses import dataclass, field

from ..base import ComputeSettingsBase
from ..conda import ComputeSettingsConda
from ..container import ComputeSettingsContainer
from .runner import LocalRunner

# import tarfile


@dataclass
class LocalArch(ComputeSettingsBase,
                ComputeSettingsContainer,
                ComputeSettingsConda,
                LocalRunner):
    """Local computational environment."""

    arch_type: str = field(default='local')
    # use_relative_paths: Optional[Union[bool, PathLike]] = None

    def __post_init__(self):
        """Post initialization."""
        super().__post_init__()
        self._set_base()
        # self._set_container()
        # self._set_conda_env()
        # compatibility
        self._set_run_settings()
        self.Runner = LocalRunner()  # type: ignore
        self.scheduler = self.target or 'local'
        self.connection_type = 'local'
        self.logger.debug(f'LocalArch: {self.__dict__}\n')

    @property
    def name(self) -> str:
        """Print environment type."""
        return 'local'

    # def clean_scratch_dir_local(self,
    #                             exceptions: Optional[List[str]] = None
    #                            ) -> None:
    #     """Clean scratch directory.

    #     Parameter
    #     ---------
    #     exceptions: List[str], optional
    #         list of exceptions, e.g. [ 'myfile', '*.inp' ]

    #     """
    #     return self.clean_dir(
    #         self.scratch_dir_local,  # type: ignore
    #         exceptions)  # type: ignore

    # def clean_work_dir_local(self,
    #                          exceptions: Optional[List[str]] = None) -> None:
    #     """Clean working directory.

    #     Parameter
    #     ---------
    #     exceptions: List[str], optional
    #         list of exceptions, e.g. [ 'myfile', '*.inp' ]

    #     """
    #     return self.clean_dir(self.work_dir_local, exceptions)

    # def dump(self, filename: PathLike) -> None:
    #     """Save settings to file.

    #     Parameters
    #     ----------
    #     filename : PathLike
    #         filename

    #     """
    #     path = Path(filename)
    #     extension = path.suffix
    #     dumpers: Dict[str, Callable] = {'.yml': yaml.dump,
    #                                     '.yaml': yaml.dump,
    #                                     '.json': json.dump}
    #     dont_save = ['run_settings', 'Runner', 'arch_type']
    #     d = {k: v for k, v in self.__dict__.items()
    #          if k not in dont_save and v}

    #     for k, v in d.items():
    #         if isinstance(v, Path):
    #             d[k] = str(v)

    #     if extension in dumpers:
    #         with open(path, 'w') as f:
    #             dumpers[extension](d, f)
    #     else:
    #         raise ValueError(
    #             f'Extension {extension} not supported, use .yml, .yaml or ' +
    #             '.json'
    #         )
