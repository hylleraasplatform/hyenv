import hashlib
import inspect
from dataclasses import dataclass, field
from pathlib import Path
from typing import List, Optional, Union

from hytools.logger import LoggerDummy

from ..abc import RunSettings
from ..base.partial_settings import RunSettingsFiles
from ..file import FileHandler
from .compute_settings import LocalArch

try:
    from hytools.file import File
except ImportError:
    from ...file import File

PathLike = Union[Path, str]
PathLikeList = Union[PathLike, List[PathLike]]
FileLike = Union[PathLike, File]


# python >= 3.10: inherit from ComputeSettings with @dataclass(kw_only=True)
@dataclass
class LocalRunSettings(RunSettings, RunSettingsFiles, LocalArch):
    """Run settings for local execution."""

    program: Optional[str] = None
    args: Optional[List[str]] = field(default_factory=list)
    restart: Optional[bool] = False
    # deprecated
    stdout_redirect: Optional[FileLike] = None

    def __post_init__(self):
        """Post-initialization."""
        logger = getattr(self, 'logger') or LoggerDummy()
        self._fix_deprecated()
        self._convert_args()
        if isinstance(self.launcher, str) and ' ' in self.launcher:
            self.launcher = self.launcher.split(' ')

        if self.sub_dir:
            self.sub_dir = self.__class__._gen_subdir_name(
                self.sub_dir, self.program, self.args)

        self._gen_full_paths()
        self.run_in_container = (
            getattr(self, 'container_image', None) is not None)

        self.file_handler = FileHandler(self)

        self._fix_files()
        self._fix_paths_in_arglist()  # type: ignore

        logger.debug(f'LocalRunSettings: {self.__dict__}\n')

    def _fix_deprecated(self):
        """Fix deprecated."""
        logger = getattr(self, 'logger', LoggerDummy())
        if self.stdout_redirect:
            logger.warning(
                'stdout_redirect is deprecated, use stdout_file instead')
            self.stdout_file = self.stdout_redirect or self.stdout_file

    def _fix_files(self):
        """Fix files."""
        self.output_file = self._to_file(self.output_file)
        self.stdout_file = self._to_file(self.stdout_file)
        self.stderr_file = self._to_file(self.stderr_file)
        self.files_to_write = [self._to_file(f) for f in self.files_to_write]
        self.files_for_restarting = [
            self._to_file(f) for f in self.files_for_restarting
        ]
        self.files_to_parse = [self._to_file(f) for f in self.files_to_parse]
        self.files_to_remove = [self._to_file(f) for f in self.files_to_remove]
        self.files_to_zip = [self._to_file(f) for f in self.files_to_zip]
        self.files_to_tar = [self._to_file(f) for f in self.files_to_tar]
        self.data_files = [self._to_file(f) for f in self.data_files]
        self.files_to_rename = [
            {'from': self._to_file(file_dict['from']),
             'to': self._to_file(file_dict['to'])}
            for file_dict in self.files_to_rename
        ]

    def _fix_paths_in_arglist(self):
        """Fix paths in arglist."""
        for i, arg in enumerate(self.args):
            if isinstance(arg, (str, Path)):
                self.args[i] = str(arg)
                continue
            elif isinstance(arg, File):
                f = self.file_handler.to_file(arg)
                if f.folder is not None:
                    self.args[i] = str(f.path)
                    continue
                # used by Runner
                if self.run_in_container:
                    self.args[i] = str(f.work_path_container)
                    continue
                self.args[i] = str(f.work_path_local)

    @property
    def name(self) -> str:
        """Print environment type."""
        return 'local'

    @classmethod
    def from_dict(cls, env):
        """Create environment from dict."""
        return cls(**{
            k: v
            for k, v in env.items() if k in inspect.signature(cls).parameters
        })

    def _convert_args(self):
        """Convert args to strlist."""
        if not isinstance(self.args, str):
            return
        self.args = self.args.split(' ') if ' ' in self.args else [self.args]

    @classmethod
    def _gen_subdir_name(cls,
                         sub_dir: Optional[Union[bool, PathLike]] = None,
                         program: Optional[str] = None,
                         args: Optional[list] = None) -> str:
        """Generate sub_dir name."""
        if isinstance(sub_dir, (str, Path)):
            return str(sub_dir)  # type: ignore
        program_str = str(program) or 'run'
        hashes = [str(a) for a in args if isinstance(a, (str, Path))]
        _hash = hashlib.sha256(' '.join(hashes).encode()).hexdigest()
        return f'{program_str}_{_hash})'

    # def _gen_subdir_name(self) -> Union[bool, str, Path, None]:
    #     """Generate sub_dir name."""
    #     if isinstance(self.sub_dir, bool):
    #         if not self.program:
    #             return self.sub_dir
    #         if self.sub_dir:
    #             try:
    #                 args_hash = hashlib.sha256(' '.join(
    #                     self.args).encode()  # type: ignore
    #                                            ).hexdigest()
    #             except TypeError:
    #                 args_hash = ''
    #             finally:
    #                 return self._path(str(self.program) + '_' + args_hash)
    #     return self.sub_dir

    def _gen_full_paths(self) -> None:
        """Generate full paths."""
        # if isinstance(self.sub_dir, bool):
        #     # self.sub_dir = self._gen_subdir_name()
        #     raise TypeError('sub_dir must be a string or None, not bool')
        dir_attributes = ['work_dir_container',
                          'scratch_dir_container',
                          'data_dir_container']

        self.work_dir_local = self._path(self.work_dir_local)
        self.scratch_dir_local = self._path(self.scratch_dir_local)
        self.data_dir_local = self._path(self.data_dir_local)

        for attr in dir_attributes:
            if hasattr(self, attr):
                setattr(self, attr, self._path(getattr(self, attr)))

        if self.sub_dir:
            self._add_sub_dir()

        if self.output_folder:
            self._add_output_folder(dir_attributes)

    def _add_sub_dir(self) -> None:
        """Add sub directory to directories."""
        self.sub_dir = self._path(self.sub_dir)  # type: ignore
        self.work_dir_local = self.work_dir_local / self.sub_dir
        self.scratch_dir_local = self.scratch_dir_local / self.sub_dir
        if hasattr(self, 'work_dir_container'):
            self.work_dir_container: Path = (
                self.work_dir_container / self.sub_dir
                if self.work_dir_container else None)
        self.sub_dir = None

    def _add_output_folder(self, dir_attributes: List[str]) -> None:
        """Add output folder to directories."""
        if str(self.output_folder) in str(self.work_dir_local):
            return

        for attr in dir_attributes:
            if (hasattr(self, attr)
                and str(self.work_dir_local) == str(getattr(self,
                                                            attr))):
                setattr(self, attr,
                        (self._path(
                            self.work_dir_local) / self.output_folder))
        self.work_dir_local = self._path(
            self.work_dir_local) / self.output_folder

    def _to_file(self, file: FileLike, folder: Optional[PathLike] = None):
        """Convert file to File instance."""
        return (self.file_handler.to_file(file, folder=folder)
                if folder else self.file_handler.to_file(file))

    def abs_path(self,
                 filename: Union[str, Path],
                 run_in_container: Optional[bool] = False) -> Path:
        """Return absolute path for file.

        Parameters
        ----------
        filename : Union[str, Path]
            Filename.
        run_in_container : Optional[bool], optional
            If True, return path in container, by default False

        Returns
        -------
        Path
            Absolute path.

        """
        logger = getattr(self, 'logger', LoggerDummy())
        logger.warning('abs_path is deprecated, use File() instead')
        if run_in_container:
            return self.work_dir_container / filename  # type: ignore
        else:
            return self.work_dir_local / filename  # type: ignore
