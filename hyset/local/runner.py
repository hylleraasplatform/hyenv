# mypy: disable-error-code="attr-defined"
import re
# from shlex import quote, split
import shlex
import subprocess
from contextlib import contextmanager
from copy import deepcopy
from pathlib import Path
from sys import executable as python_ex
from typing import Any, Dict, Generator, List, Optional, Union

from hytools.logger import LoggerDummy

from ..abc import Runner, RunSettings
from ..file import File, LocalFileHandler
from ..run_settings import run_sequentially
from ..warnings import warn_deprecated
from .compute_result import LocalResult

stderr_exceptions = [
    'normal termination of xtb',
    'Cannot open input file: gibberish',
    'src/start/cp2k.F (unit = 6, file = ',
    'DEEPMD INFO',
]

PathLike = Union[Path, str, None]
PathLikeList = List[PathLike]
FileLike = Union[Path, str, None, File]


class LocalRunner(Runner, LocalFileHandler):
    """Run computation on local architecture."""

    def __init__(self, logger: Optional[LoggerDummy] = None):
        """Initialize LocalRunner object."""
        self.logger = getattr(self, 'logger', LoggerDummy())

    @contextmanager
    def run_ctx(self, inp: RunSettings) -> Generator[str, None, None]:
        """Run a computaion inside a context manager.

        Parameter
        ---------
        inp : RunSettings
            input

        Returns
        -------
        Generator
            generates context

        """
        output = str(self.run(inp))
        try:
            yield output
        finally:
            pass

    @run_sequentially
    def teardown(self, inp: RunSettings) -> None:
        """Teardown a computation.

        Parameter
        ---------
        inp : :class:`hyset.RunSettings`
            input
        """
        self._remove_files(inp.files_to_remove,  # type: ignore
                           parent=inp.work_dir_local)
        # r = self.remove_file_local(inp.files_to_remove)  # type: ignore
        # if not r:
        #     raise FileNotFoundError(
        #         'could not remove files: ', inp.files_to_remove
        #     )

    async def ateardown(self, settings: RunSettings):
        """Asynchronous version of teardown."""
        return self.teardown(settings)

    def run_new(self, inp: RunSettings) -> LocalResult:
        """Run a computation locally."""
        warn_deprecated('run_new is deprecated, use run instead')
        return self.run(inp)

    def sanitize_cmd(
        self, cmd: Union[str, list], delimiters: Optional[List[str]] = None
    ) -> List[str]:
        """Sanitize command."""
        if delimiters is None:
            delimiters = [';', '|', '&&']
        if not cmd:
            return []
        if isinstance(cmd, list):
            cmd = ' '.join(cmd)
        elif not isinstance(cmd, str):
            raise TypeError('command must be a string')
        pattern = '|'.join(map(re.escape, delimiters))  # type: ignore
        parts = re.split(f'({pattern})', cmd)
        result = []
        for part in parts:
            if part in delimiters:
                result.append(part)
            else:
                subparts = re.split(r'([ \t+])', part)
                for subpart in subparts:
                    if '\n' in subpart:
                        if '"' in subpart:
                            result.extend(shlex.split(subpart))
                        else:
                            # split
                            subpart = subpart.split('\n')
                            subpart.append(';')
                            result.extend(subpart)
                    else:
                        result.append(subpart)
    # Remove empty strings from the result
        cmd = [subpart for subpart in result if subpart.strip()]
    # return result
        # parts = [part.strip() for part in parts if part.strip()]
        # cmd = [
        #     subpart
        #     for part in parts
        #     for subpart in (part.split() if part not in delimiters else
        # [part])
        # ]
        if any([c in cmd[0] for c in delimiters]):
            cmd = cmd[1:]
        if not any([c in cmd[-1] for c in delimiters]):
            cmd.append(';')

        return cmd

    def prepare_cmd(
        self, cmd: List[str], shell: bool
    ) -> Union[str, List[str]]:
        """Prepare the pre_cmd based on run_opts."""
        # tobe replaced with shlex.join/quote
        return ' '.join(cmd[:-1]) if shell else cmd[:-1]

    def prepare_subprocess_input(
        self,
        pre_cmd: Optional[List[str]] = None,
        result_pre: Optional[subprocess.CompletedProcess] = None,
        stdin_file: Optional[Union[str, File, Path]] = None,
    ) -> Optional[Union[bytes, str]]:
        """Get the input for the subprocess based on pre_cmd and result_pre."""
        if stdin_file is not None:
            self.logger.debug(f'STDIN FILE: {stdin_file}')
        if isinstance(stdin_file, File):
            stdin_path = self.get_path_from_file(stdin_file)
            return stdin_path.read_text()
        elif isinstance(stdin_file, str):
            return stdin_file
        elif isinstance(stdin_file, Path):
            return stdin_file.read_text()
        if not pre_cmd:
            return None
        return result_pre.stdout if pre_cmd and pre_cmd[-1] == '|' else None

    def split_by_delimiters(
            self, commands: List[str], delimiters: List[str]
            ) -> List[List[str]]:
        """Split a list of commands by delimiters into different lists."""
        result = []
        current_list = []

        for item in commands:
            current_list.append(item)
            if item in delimiters:
                result.append(current_list)
                current_list = []

        if current_list:
            result.append(current_list)

        return result

    def get_idx_final_result(
        self,
        cmds: List[List[str]],
        post_cmd: List[str],
        running_list: List[str],
    ) -> int:
        """Get the final result based on post_cmd and running_list."""
        if not post_cmd:
            self.logger.debug(f'Result taken from cmds: {cmds[-1]}')
            return -1

        first_part_of_post_cmd = self.split_by_delimiters(
            post_cmd, ['|', '&&']
        )[0]

        for i, c in enumerate(cmds):
            if c == first_part_of_post_cmd:
                break

        if running_list[-1] in [';', '&&']:
            self.logger.debug(f'Result taken from cmds: {cmds[i - 1]}')
            return i - 1
        else:
            # main result is piped to post_cmd
            for j in range(i + 1, len(cmds)):
                if cmds[j][-1] in [';', '&&']:
                    self.logger.debug(f'Result taken from cmds: {cmds[j]}')
                    return j

        return -1

    def get_path_from_file(self,
                           file: Optional[Union[File, str, Path]] = None,
                           parent_name: Optional[str] = None,
                           parent: Optional[Union[str, Path]] = None,
                           run_settings: Optional[RunSettings] = None) -> Path:
        """Get path from file."""
        parent_name = parent_name or 'work_path_local'

        if isinstance(file, (str, Path)):
            return Path(file)
        if parent is not None:
            parent = Path(parent)
            name = getattr(file, 'name', Path(str(file)).name)
            if Path(name).is_absolute():
                return Path(name)
            return parent / name

        if getattr(file, 'folder', None):
            parent = Path(file.folder)  # type: ignore
            name = getattr(file, 'name', Path(str(file)).name)
            return parent / name if parent else Path(name)

        if getattr(file, parent_name, None):
            return getattr(file, parent_name)

        if getattr(run_settings, parent_name.replace('path', 'dir'), None):
            parent = Path(str(getattr(run_settings,
                                      parent_name.replace('path', 'dir'))))
            name = getattr(file, 'name', Path(str(file)).name)
            return parent / name if parent else Path(name)

        raise ValueError('Invalid file or run settings provided.')

    @run_sequentially
    def run(self, rs: RunSettings) -> LocalResult:
        """Run a computation locally.

        Parameter
        ---------
        rs : :class:`hyset.RunSettings`
            input

        Returns
        -------
        :class:`hyset.LocalResult`
            output

        """
        inp = deepcopy(rs)
        logger = inp.logger or LoggerDummy()
        if self._check_quick_return(inp):
            old_result = LocalResult(**self._gen_output(inp, None))
            logger.warning('Output file exists, returning old result\n')
            logger.debug('\nRESULT: %s', old_result)
            return old_result

        inp._set_conda_env()
        inp._set_container()

        if inp.run_in_container:
            inp.update_container_launcher()  # type: ignore
            inp._fix_paths_in_arglist()  # type: ignore

        self._create_local_dirs(inp)
        self._remove_files(inp.files_to_remove,  # type: ignore
                           parent=inp.work_dir_local)  # type: ignore
        if not inp.restart:
            self._remove_files(inp.files_for_restarting,  # type: ignore
                               parent=inp.work_dir_local)  # type: ignore
        self._write_files(inp.files_to_write)  # type: ignore

        cwd = inp.work_dir_local

        run_opts = {
            'text': True,
            'shell': False,
            'cwd': cwd,
            'env': inp.env,
            'capture_output': True,
        }

        if inp.use_relative_paths:
            ref = Path.cwd()
            if isinstance(inp.use_relative_paths, (str, Path)):
                ref = self._path(inp.use_relative_paths)
            cwd = self._gen_relative_path(cwd, ref)
        running_list: List[str] = self._gen_running_list(
            inp, cwd  # type: ignore
        )  # type: ignore

        pre_cmd = self.sanitize_cmd(inp.pre_cmd)
        post_cmd = self.sanitize_cmd(inp.post_cmd)
        running_list = self.sanitize_cmd(running_list)
        running_list[-1] = (
            '&&' if running_list[-1] == ';' else running_list[-1]
        )

        cmd = pre_cmd + running_list + post_cmd

        for c in cmd:
            if 'python' in c:
                c = c.replace('python', python_ex)

        cmds: List[List[str]] = self.split_by_delimiters(cmd, [';', '|', '&&'])

        idx_final = self.get_idx_final_result(cmds, post_cmd, running_list)
        if idx_final < 0:
            idx_final += len(cmds)
        stdin_file = (inp.work_dir_local / getattr(inp.stdin_file,
                                                   'name', inp.stdin_file)
                      if inp.stdin_file else None)

        logger.debug('RUNNING IN FOLDER: \n %s', str(cwd) + '\n')

        results: List[subprocess.CompletedProcess] = []
        for i, cmd in enumerate(cmds):
            prev_cmd = cmds[i - 1] if i > 0 else None

            logger.debug(f'RUNNING #{i}: {cmd}\n')
            input_subprocess = self.prepare_subprocess_input(
                pre_cmd=prev_cmd, result_pre=results[-1] if i > 0 else None,
                stdin_file=stdin_file if i == idx_final else None
            )
            if input_subprocess:
                logger.debug('STDIN INPUT: \n %s', input_subprocess)
            result = subprocess.run(
                self.prepare_cmd(cmd, run_opts['shell']),
                input=input_subprocess,
                **run_opts,
            )
            logger.debug('RESULT: \n %s', result)
            if result.returncode != 0:
                logger.error(f'RUNNING CMD {cmd} FAILED WITH STDERR\n ' +
                             f'{result.stderr}')
                if cmd[-1] in ['|', '&&']:
                    raise ValueError('RUNNING CMD FAILED')
            results.append(result)
            if input_subprocess:
                logger.debug('RESULT PIPED TO NEXT CMD\n')

        result = results[idx_final]

        # result_pre = None
        # if pre_cmd:
        #     result_pre = subprocess.run(
        #         self.prepare_cmd(pre_cmd, run_opts['shell']), **run_opts)
        #     logger.debug('PRE CMD RESULT: \n %s', result_pre)
        #     if result_pre.returncode != 0:
        #         logger.error(f'PRE CMD {pre_cmd} FAILED\n')
        #         if pre_cmd[-1] in ['|', '&&']:
        #             raise ValueError('PRE CMD FAILED')

        # result = subprocess.run(
        #     self.prepare_cmd(running_list, run_opts['shell']),
        #     input=self.prepare_subprocess_input(pre_cmd=pre_cmd,
        #                                         result_pre=result_pre),
        #     **run_opts,
        #     )
        # logger.debug('RESULT: \n %s', result)
        # if result.returncode != 0:
        #     logger.error(f'RUNNING CMD {running_list} FAILED\n')
        #     if running_list[-1] in ['|', '&&']:
        #         raise ValueError('RUNNING CMD FAILED')

        # if post_cmd:
        #     result_post = subprocess.run(
        #         self.prepare_cmd(post_cmd, run_opts['shell']),
        #         input=self.prepare_subprocess_input(pre_cmd=running_list,
        #                                             result_pre=result),
        #         **run_opts
        #         )
        #     logger.debug('POST CMD RESULT: \n %s', result_post)
        #     if result_post.returncode != 0:
        #         logger.error(f'POST CMD {post_cmd} FAILED, RETURNING\n')
        #     else:
        #         result = result_post

        # result = subprocess.run(
        #         running_list[:-3].split(),
        #         input=pre.stdout,
        #         capture_output=True,
        #         shell=False,
        #         **run_opts,
        #     )

        # post_cmd = (
        #     split(quote(inp.post_cmd))
        #     if not isinstance(getattr(inp, 'post_cmd', []), list)
        #     else getattr(inp, 'post_cmd', [])
        # )
        # pipe_pre_cmd = '|' in pre_cmd[-1]
        # if pipe_pre_cmd:
        #     pre_cmd[-1] = pre_cmd[-1].replace('|', '')
        # pipe_post_cmd = '|' in post_cmd[0]
        # if pipe_post_cmd:
        #     post_cmd[0] = post_cmd[0].replace('|', '')

        # if pre_cmd and not pipe_pre_cmd:
        #     run_opts['shell'] = True
        #     running_list = pre_cmd + [';'] + running_list

        # if post_cmd and not pipe_post_cmd:
        #     run_opts['shell'] = True
        #     running_list = running_list + ['&&'] + post_cmd

        # running_list = ' '.join(
        #     [
        #         c.replace('python', python_ex) if 'python' in c else c
        #         for c in running_list
        #     ]
        # )  # type: ignore

        # # if not run_opts['shell']:
        # #     running_list = split(running_list)  # type: ignore

        # # logger.debug(f'RUNNING: \n {running_list} \n')
        # # logger.debug('IN FOLDER: \n %s', str(cwd) + '\n')

        # enable_shell = run_opts.pop('shell')

        # if not pipe_pre_cmd and not pipe_post_cmd:
        #     result = subprocess.run(
        #         running_list,  # type: ignore
        #         capture_output=True,  # type: ignore
        #         shell=enable_shell,  # type: ignore
        #         **run_opts,
        #     )

        # if pipe_pre_cmd and not pipe_post_cmd:
        #     logger.debug('PIPE PRE CMD' + str(pre_cmd))

        #     pre = subprocess.run(
        #         pre_cmd, capture_output=True, shell=True, **run_opts
        #     )
        #     logger.debug('PRE CMD RESULT: \n %s', pre)

        #     result = subprocess.run(
        #         running_list[:-3].split(),
        #         input=pre.stdout,
        #         capture_output=True,
        #         shell=False,
        #         **run_opts,
        #     )

        #     # with subprocess.Popen(pre_cmd,
        #     #                       stdout=subprocess.PIPE,
        #     #                       shell=True,
        #     #                       **run_opts) as pre:
        #     #     result = subprocess.run(running_list,
        #     #                             stdin=pre.stdout,
        #     #                             shell=enable_shell,
        #     #                             capture_output=True,
        #     #                             **run_opts)

        # if not pipe_pre_cmd and pipe_post_cmd:
        #     logger.debug('PIPE POST CMD' + str(post_cmd))
        #     run_opts.pop('capture_output')
        #     with subprocess.Popen(
        #         running_list, stdout=subprocess.PIPE, shell=True, **run_opts
        #     ) as p:
        #         result = subprocess.run(
        #             post_cmd,
        #             stdin=p.stdout,
        #             shell=enable_shell,
        #             capture_output=True,
        #             **run_opts,
        #         )

        # if pipe_pre_cmd and pipe_post_cmd:
        #     logger.debug('PIPE PRE CMD' + str(pre_cmd))
        #     logger.debug('PIPE POST CMD' + str(post_cmd))
        #     run_opts.pop('capture_output')
        #     with subprocess.Popen(
        #         pre_cmd, stdout=subprocess.PIPE, shell=True, **run_opts
        #     ) as pre:
        #         with subprocess.Popen(
        #             running_list,
        #             stdin=pre.stdout,
        #             shell=True,
        #             stdout=subprocess.PIPE,
        #             **run_opts,
        #         ) as p:
        #             result = subprocess.run(
        #                 post_cmd,
        #                 stdin=p.stdout,
        #                 shell=enable_shell,
        #                 capture_output=True,
        #                 **run_opts,
        #             )

        self._rename_files(inp.files_to_rename,  # type: ignore
                           parent=inp.work_dir_local)  # type: ignore

        total_result = LocalResult(**self._gen_output(inp, result))
        logger.debug('RESULT: \n %s', total_result)

        return total_result

    async def arun_new(self, settings: RunSettings):
        """Asynchronous version of run_new."""
        return self.run_new(settings)

    async def arun(self, settings: RunSettings):
        """Asynchronous version of run."""
        return self.run(settings)

    def _create_local_dirs(self, inp: RunSettings):
        """Create directories."""
        logger = inp.logger or LoggerDummy()
        dirs_to_create = [
            inp.work_dir_local,
            inp.scratch_dir_local,
            inp.data_dir_local,
        ]

        if inp.use_relative_paths:
            ref = Path.cwd()
            if isinstance(inp.use_relative_paths, (str, Path)):
                ref = self._path(inp.use_relative_paths)
            dirs_to_create = [
                self._gen_relative_path(d, ref) for d in dirs_to_create
            ]

        created_dirs: PathLikeList = [None]
        for d in dirs_to_create:
            if d in created_dirs:
                continue
            logger.debug('creating directory %s', d)
            if inp.dry_run:
                logger.debug('dry run, not creating directory %s', d)
                continue
            created = self.create_dir_local(d)
            if not created:
                raise ValueError(f'Could not create {d}')
            created_dirs.append(d)  # type: ignore

    def _check_quick_return(self, inp: RunSettings) -> bool:
        """Check if output file exists and return if it does."""
        logger = inp.logger or LoggerDummy()
        files_to_check = [
            getattr(f, 'work_path_local')
            for f in [inp.output_file, inp.stdout_file, inp.stderr_file]
            if getattr(f, 'work_path_local', None) is not None
        ]
        files_to_check = [
            f
            for f in files_to_check
            if f.name not in ['stdout.out', 'stderr.out']
        ]
        if not any(f.exists() for f in files_to_check if f is not None):
            return False

        logger.debug(f'(one of) output file(s) {files_to_check} exists')

        logger.info(
            'force_recompute is %s, will %srecompute\n',
            'set' if inp.force_recompute else 'not set',
            '' if inp.force_recompute else 'not ',
        )
        return not inp.force_recompute

    # @singledispatchmethod
    # def remove_file(self, file: Union[str, Path, File]):
    #     """Remove file."""
    #     raise TypeError('file must be a string, Path or File')

    # @remove_file.register(str)
    # @remove_file.register(Path)
    # def _(self, file: str):
    #     """Remove file."""
    #     p = Path(file)
    #     if p.exists():
    #         p.unlink()
    #     else:
    #         self.logger.error(f'Could not remove file {file}. File does ' +
    #                           'not exist')

    # @remove_file.register(File)
    # def _(self, file: File):
    #     """Remove file."""
    #     if Path(file.name).exists():
    #         Path(file.name).unlink()
    #     elif Path(file.work_path_local).exists():
    #             file.work_path_local.unlink()
    #     else:
    #         self.logger.error(f'Could not remove file {file}.')

    def _remove_files(self,
                      files: List[File],
                      parent: Union[Path, str] = None):
        """Remove files."""
        logger = self.logger or LoggerDummy()
        if files is None:
            return
        for f in files:
            path = self.get_path_from_file(file=f, parent=parent)
            # if f.folder is not None:
            #     path = Path(f.folder) / f.name
            # else:
            #     path = f.work_path_local  # type: ignore
            logger.debug('removing file %s', path)
            self.remove_file_local(path)

    def _rename_files(self,
                      files: List[Dict[str, File]],
                      parent: Union[Path, str] = None):
        """Rename files."""
        logger = self.logger or LoggerDummy()
        if not files:
            return
        for d in files:
            f = d['from']
            nf = d['to']
            path = self.get_path_from_file(file=f, parent=parent)
            # if f.folder is not None:  # type: ignore
            #     path = Path(f.folder) / f.name  # type: ignore
            # else:
            #     path = f.work_path_local  # type: ignore
            if isinstance(nf, str):
                newpath = path.parent / nf
            else:
                if nf.folder is not None:
                    newpath = Path(nf.folder) / nf.name
                else:
                    newpath = nf.work_path_local  # type: ignore
            logger.debug('renaming file %s to %s', path, newpath)
            path.rename(newpath)

    def _replace_paths_in_file_content(
        self, file: File, default: Optional[str] = None
    ) -> str:
        """Replace paths in file content."""
        default = default or 'data_path_local'
        content = file.content
        if content is None:
            return ''

        if not file.variables:
            return content
        variables = deepcopy(file.variables or {})
        for k, v in variables.items():
            if isinstance(v, File):
                v = getattr(v, default)
                variables[k] = v
        return file.handler.substitute_var_in_str(  # type: ignore
            string=content, variables=variables
        )

    def _write_files(self, files: List[File]):
        """Write files."""
        logger = self.logger or LoggerDummy()
        if files is None:
            return
        for f in files:
            if f.content is None:  # type: ignore
                continue
            content = self._replace_paths_in_file_content(f)  # type: ignore
            if f.folder is not None:  # type: ignore
                self.create_dir_local(f.folder)  # type: ignore
                path = Path(f.folder) / f.name  # type: ignore
            else:
                path = f.work_path_local  # type: ignore
            logger.debug('writing file %s with content\n%s', path, content)
            if path.exists():  # type: ignore
                logger.warning('file %s exists, overwriting', path)
            self.write_file_local(path, content)  # type: ignore

    def _gen_output(self, inp: RunSettings, result: Any) -> Dict[str, Any]:
        """Generate output."""
        logger = inp.logger or LoggerDummy
        output_dict: Dict[str, Any]
        files_to_parse = inp.files_to_parse
        for i, f in enumerate(files_to_parse):
            if isinstance(f, File):
                if hasattr(f, 'work_path_local'):
                    files_to_parse[i] = f.work_path_local  # type: ignore
        output_dict = {
            'files_to_parse': files_to_parse,
            'output_folder': (
                inp.work_dir_local if inp.output_folder else None
            ),
        }
        if inp.output_file:
            if hasattr(inp.output_file, 'work_path_local'):
                f = inp.output_file.work_path_local  # type: ignore
            else:
                f = inp.output_file
            output_dict.update({'output_file': f})  # type: ignore
        if result is None:
            return output_dict
        output_dict.update({'returncode': result.returncode})

        if result.returncode < 0:
            logger.warning(
                f'{inp.program} run failed, '
                + 'returncode: {result.returncode}'
            )
        if result.stderr:
            stderr_file = inp.stderr_file.work_path_local  # type: ignore
            self.write_file_local(stderr_file, result.stderr)
            logger.debug('STDERR: %s', result.stderr)
            output_dict['stderr'] = result.stderr
        if result.stdout:
            stdout_file = inp.stdout_file.work_path_local  # type: ignore
            self.write_file_local(stdout_file, result.stdout)
            output_dict['stdout'] = stdout_file
        return output_dict

    def _gen_running_list(self, inp: RunSettings, cwd: Path) -> List[str]:
        """Generate running list."""
        running_list: List[str] = [
            *inp.launcher,
            inp.program,
            *inp.args,  # type: ignore
        ]  # type: ignore #14891
        running_list = [str(x).strip() for x in running_list]

        if not all([isinstance(x, str) for x in running_list]):
            raise TypeError(
                'subprocess call command must be a list of strings ',
                running_list,
            )
        return running_list


# if __name__ == '__main__':

#     from hyif import Orca, Xtb
#     from hyobj import Molecule

#     from .compute_settings import LocalArch
#     cs = LocalArch(debug=True, force_recompute=True, print_level=2,
#                    scratch_dir = '/Users/tilmann/Downloads/scratch',
#                    use_relative_paths = True)

#     xtb = Xtb({'parser': 'full', 'check_version': True}, compute_settings=cs)
#     mol = Molecule('H 0 0 0\n H 0 0 1.4')

#     # print(xtb.setup(mol))
#     en1 = xtb.get_energy(mol)
#     print('EN1', en1)
#     cs = LocalArch(debug=True, force_recompute=True, print_level=2,
#                    scratch_dir = '/Users/tilmann/Downloads/scratch')
#     xtb = Xtb({'parser': 'full', 'check_version': True}, compute_settings=cs)
#     en1b = xtb.get_energy(mol)
#     print('ENb1', en1b)

#     cs_conda = LocalArch(debug=True,
#                          force_recompute=True,
#                          conda_env='python3.9',
#                          print_level=2)
#     xtb = Xtb({
#         'parser': 'full',
#         'check_version': True
#     },
#               compute_settings=cs_conda)
#     # pen2rint(xtb.setup(mol))
#     en2 = xtb.get_energy(mol)
#     print('EN2', en2)

#     # cs_docker = LocalArch(debug=True,
#     #                       force_recompute=True,
#     #                       docker_image='xtb',
#     #                       print_level=2)
#     # xtb = Xtb({
#     #     'parser': 'full',
#     #     'check_version': False
#     # },
#     #           compute_settings=cs_docker)
#     # en3 = xtb.get_energy(mol)
#     # print('EN3', en3)

#     myenv4 = LocalArch(
#         'local',
#         path='/Users/tilmann/Downloads/orca_5_0_3_macosx_intel_openmpi411/',
#         force_recompute=True,
#         mpi_procs=4,
#         memory=1000,
#         debug=True)
#     orca = Orca({'check_version': False}, compute_settings=myenv4)
#     print(orca.get_energy(mol))
