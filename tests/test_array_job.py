# import asyncio
# import time
# from copy import deepcopy
# from typing import Any, Callable, Optional, Sequence, Tuple, Union

# from hyif import Xtb
# from hyobj import DataSet, Molecule

# from hyset import ComputeSettings, LocalArch, create_compute_settings


# class ArrayJob:
#     """Array job class."""

#     def __init__(self,
#                  functions: Sequence[Callable],
#                  arguments: Sequence[Sequence],
#                  settings: Sequence[ComputeSettings],
#                  shape: Optional[tuple] = None):
#         """Create an array job.

#         Parameters
#         ----------
#         functions: Sequence of Callables
#             Sequence of functions.
#         arguments: Sequence of Sequences
#             Sequence of arguments for methods.
#         settings: Sequence of ComputeSettings
#             Sequence of settings.
#         shape: tuple, optional
#             Shape of the array job. Default is (len(methods), ).

#         """
#     # note all methods come with an own ComputeSettings
#     # the settings object should more like 'wrap ' them into a task and
#     # execute according to it
#     # dryrun: check CS concanetation
#         if len(functions) != len(arguments):
#             raise ValueError('methods and arguments must have  same length')

#         if shape is None:
#             self.shape = tuple([len(functions)//len(settings) +
#                                 (len(functions) % len(settings) > 0),
#                                 len(settings) ])
#         else:
#             self.shape = shape
#         if self.shape[0]*self.shape[1] < len(functions):
#             raise ValueError('methods must be divisible by settings')

#         self.functions = self.flatlist_reshape(functions, self.shape)
#         self.arguments = self.flatlist_reshape(arguments, self.shape)
#         self.settings = settings

#     # def get_cmd(self, method, arg):
#     #     func = deepcopy(method)
#     #     cs = func.__self__.compute_settings
#     #     print(cs), print(cs.dry_run)
#     #     if hasattr(func.__self__, 'runner'):
#     #         func.__self__.runner.dry_run = True
#     #         print(dir(func.__self__.runner))
#     #         print(func.__self__.runner.dry_run)

#     def flatlist_reshape(self, list, target_shape):
#         """Reshape a flat list into a nested list."""
#         newlist = []
#         for i in range(target_shape[1]):
#             newlist.append(list[i::target_shape[1]])
#         return newlist

#     async def run_columns(self, functions, arguments, settings, shape):
#         """Run batches of methods (self.shape[1]) concurrent."""
#         columns = [self.run_rows(i,
#                                  functions[i],
#                                  arguments[i],
#                                  settings[i]
#                                  ) for i in range(shape[1])]
#         # tasks = [ asyncio.create_task(c) for c in columns]
#         result = await asyncio.gather(*columns)
#         # return DataSet.combine_cls([r.data for r in result])
#         return result

#     async def run_rows(self, i, row_functions, row_args, compute_settings):
#         """Run a batch of methods (self.shape[0]) sequential."""
#         # ds = DataSet()
#         ds = []
#         for i, f in enumerate(row_functions):
#             result = f(row_args[i])
#             # ds.append(DataSet(result))
#             ds.append(result)
#         return ds

#     def run(self) -> Union[DataSet, Sequence]:
#         """Run all methods in the array job.

#         Returns
#         -------
#         DataSet or Sequence
#             Collected results
#         """
#         return asyncio.run(self.run_columns(self.functions, self.arguments,
#                                      self.settings, self.shape))

# if __name__=='__main__':

#     # import timeit
#     # from time import sleep
#     def f(val):
#         sleep(1)
#         return val

#     functions = [ f for i in range(5)]
#     arguments = list(range(5))
#     settings = list(range(1))
#     for i in range(1, 6):
#         AJ=ArrayJob(functions, arguments, list(range(i)))
#         print('------')
#         print('shape:', AJ.shape)
#         # times = timeit.timeit(AJ.run, number=1)
#         # print('time (seconds): {times}'.format(times=times))
#         print('output:', AJ.run())
