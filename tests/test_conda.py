import unittest

from hyset import create_compute_settings


class TestComputeSettingsConda(unittest.TestCase):
    """Test class for ComputeSettingsConda."""

    def setUp(self):
        """Set up for tests."""
        self.conda = create_compute_settings('conda', conda_env='test_env')
        self.conda2 = create_compute_settings('conda')

    def test_set_conda_env(self):
        """Test set conda environment."""
        self.conda._set_conda_env()
        self.assertEqual(self.conda.conda_env, 'test_env')
        self.assertIn('conda', self.conda.launcher)
        self.assertIn('run', self.conda.launcher)
        self.assertIn('-n', self.conda.launcher)
        self.assertIn('test_env', self.conda.launcher)

    def test_set_conda_env_no_env(self):
        """Test set conda environment."""
        self.conda2._set_conda_env()
        self.assertIsNone(self.conda2.conda_env)
        self.assertNotIn('conda', self.conda2.launcher)
        self.assertNotIn('run', self.conda2.launcher)
        self.assertNotIn('-n', self.conda2.launcher)
        self.assertNotIn('test_env', self.conda2.launcher)


if __name__ == '__main__':
    unittest.main()
