import unittest
from unittest.mock import AsyncMock, MagicMock

import aiounittest

from hyset import ComputeResult, RunSettings, acompute, compute
from hyset.base import Result


class TestCompute(unittest.TestCase):
    """Test the compute function."""

    def setUp(self):
        """Set up the test."""
        self.method = MagicMock()
        self.object = MagicMock()
        self.run_settings = RunSettings()
        self.result = ComputeResult()
        self.parsed_result = Result()
        self.parsed_result_dict = {}
        self.method.setup.return_value = self.run_settings
        self.method.compute_settings.run.return_value = self.result
        self.method.parse.return_value = self.parsed_result

    # def test_compute_result(self):
    #     """Test that compute works."""
    #     with compute(self.method, self.object) as result:
    #         self.method.setup.assert_called_once_with(self.object)
    #         self.method.compute_settings.run.assert_called_once_with(
    #             self.run_settings)
    #         self.assertEqual(result, self.parsed_result)

    #     self.method.compute_settings.teardown.assert_called_once_with(
    #         self.run_settings)

    def test_compute_dictionary(self):
        """Test that compute works."""
        with compute(self.method, self.object) as result:
            self.method.setup.assert_called_once_with(self.object)
            self.method.compute_settings.run.assert_called_once_with(
                self.run_settings)
            self.assertEqual(result, self.parsed_result_dict)

        self.method.compute_settings.teardown.assert_called_once_with(
            self.run_settings)

    def test_compute_raises_attribute_error(self):
        """Test that compute errors."""
        del self.method.setup

        with self.assertRaises(AttributeError):
            with compute(self.method, self.object):
                pass


class TestACompute(aiounittest.AsyncTestCase):
    """Test the acompute function."""

    def setUp(self):
        """Set up the test."""
        self.method = MagicMock()
        self.object = MagicMock()
        self.run_settings = RunSettings()
        self.result = ComputeResult()
        self.parsed_result = {}
        self.method.setup.return_value = self.run_settings
        self.method.compute_settings.arun = AsyncMock(return_value=self.result)
        self.method.compute_settings.ateardown = AsyncMock()
        self.method.parse.return_value = self.parsed_result

    async def test_acompute(self):
        """Test that acompute works."""
        async with acompute(self.method, self.object) as result:
            self.method.setup.assert_called_once_with(self.object)
            self.method.compute_settings.arun.assert_called_once_with(
                self.run_settings)
            self.assertEqual(result, self.parsed_result)

        self.method.compute_settings.ateardown.assert_called_once_with(
            self.run_settings)

    async def test_acompute_raises_attribute_error(self):
        """Test that acompute errors."""
        del self.method.setup

        with self.assertRaises(AttributeError):
            async with acompute(self.method, self.object):
                pass


if __name__ == '__main__':
    unittest.main()
