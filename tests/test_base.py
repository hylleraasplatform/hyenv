import os
import unittest
from datetime import timedelta
from io import StringIO
from pathlib import Path
from unittest.mock import PropertyMock, patch

# from hydb import Database
from hytools.logger import Logger, get_logger

from hyset.base.compute_settings import ComputeSettingsBase
from hyset.base.partial_settings import (ComputeSettingsDirectories,
                                         ComputeSettingsGeneral,
                                         ComputeSettingsLogger,
                                         ComputeSettingsResource)


class TestComputeSettingsBase(unittest.TestCase):
    """Test class for ComputeSettingsBase."""

    def setUp(self):
        """Set up for tests."""
        self.settings = ComputeSettingsBase()

    def test_instance(self):
        """Test instance."""
        self.assertIsInstance(self.settings, ComputeSettingsBase)

    def test_default_values(self):
        """Test default values."""
        self.assertEqual(self.settings.print_level, 'ERROR')
        self.assertIsNone(self.settings.output_type)
        self.assertIsNone(self.settings.logger)
        self.assertIsNone(self.settings.database)
        self.assertIsNone(self.settings.dry_run)

    def test_dump(self):
        """Test dump."""
        self.settings.some_attribute = 'test'
        result = self.settings.dump()
        self.assertIn('some_attribute', result)
        self.assertEqual(result['some_attribute'], 'test')

    @patch('hyset.base.compute_settings.ComputeSettingsBase.logger',
           new_callable=PropertyMock)
    def test_dump_to_file(self, mock_logger):
        """Test dump to file."""
        self.settings.some_attribute = 'test'
        filename = 'test.json'
        self.settings.dump(filename=filename)
        with open(filename, 'r') as f:
            content = f.read()
        # self.assertEqual(content, '{"some_attribute": "test"}')
        self.assertIn('some_attribute', content)
        if Path(filename).exists():
            Path(filename).unlink()
        # mock_logger.debug.assert_called_once()

    @patch('sys.stdout', new_callable=StringIO)
    def test_print(self, mock_stdout):
        """Test print."""
        cs = ComputeSettingsBase()
        cs.key1 = 'value1'
        cs.key2 = 'value2'
        cs.run_settings = 'run_settings_value'
        cs.none_key = None
        cs.print()
        expected_output = 'ComputeSettings:\nprint_level: ERROR\n'
        expected_output += 'key1: value1\nkey2: value2\n'
        self.assertEqual(mock_stdout.getvalue(), expected_output)


class TestComputeSettingsGeneral(unittest.TestCase):
    """Test class for ComputeSettingsGeneral."""

    def setUp(self):
        """Set up for tests."""
        self.general = ComputeSettingsGeneral(output_type='test',
                                              dry_run=True,
                                              force_recompute=False)

    def test_general_settings(self):
        """Test general settings."""
        self.assertEqual(self.general.output_type, 'test')
        self.assertEqual(self.general.dry_run, True)
        self.assertEqual(self.general.force_recompute, False)

    def test_general_settings_default(self):
        """Test default values."""
        default_general = ComputeSettingsGeneral()
        self.assertIsNone(default_general.output_type)
        self.assertIsNone(default_general.dry_run)
        self.assertIsNone(default_general.force_recompute)

    def test_get_hash_str(self):
        """Test get hash for string."""
        test_str = 'test'
        # expected_hash = hashlib.sha256(test_str.encode()).hexdigest()
        expected_hash = \
            '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08'
        self.assertEqual(self.general.get_hash(test_str), expected_hash)

    def test_get_hash_list(self):
        """Test get hash for list."""
        test_list = ['test', 'list']
        # expected_hash = hashlib.sha256(
        #     yaml.dump(test_list, sort_keys=True).encode()).hexdigest()
        expected_hash = \
            '7b0a4a54ecfc8df7d717b381c56d72e55c6b84527e327e09ea5e8764a33443b8'
        self.assertEqual(self.general.get_hash(test_list), expected_hash)

    def test_get_hash_dict(self):
        """Test get hash for dictionary."""
        test_dict = {'key': 'value', 'key2': 'unused'}
        # expected_hash = hashlib.sha256(
        #     yaml.dump(test_dict, sort_keys=True).encode()).hexdigest()
        expected_hash = \
            '0ddd3d77338ca222ab064e214bbec3a4547e9d33801912eaacc7b4b4e27e1a91'

        self.assertEqual(self.general.get_hash(test_dict, key=True),
                         expected_hash)

    def test_get_hash_unsupported_type(self):
        """Test get hash for unsupported type."""
        test_input = 123  # an unsupported type
        expected_hash = ''
        self.assertEqual(self.general.get_hash(test_input), expected_hash)


# class TestComputeSettingsDatabase(unittest.TestCase):
#     """Test class for ComputeSettingsDatabase."""

#     def setUp(self):
#         """Set up for tests."""
#         self.database = ComputeSettingsDatabase(database='test_database')

#     def test_set_database(self):
#         """Test set database."""
#         self.database._set_database()
#         self.assertIsInstance(self.database.database, Database)
#         self.assertEqual(self.database.database.database_name,
# 'test_database')

#     def test_set_database_no_database(self):
#         """Test set database with no database."""
#         self.database.database = None
#         self.database._set_database()
#         self.assertIsNone(self.database.database)


class TestComputeSettingsResource(unittest.TestCase):
    """Test class for ComputeSettingsGeneral."""

    def setUp(self):
        """Set up for tests."""
        self.resource = ComputeSettingsResource()

    def test_set_resources(self):
        """Test set resources."""
        self.resource._set_resources()

        # Assert that the default values are set correctly
        self.assertEqual(self.resource.ntasks, 1)
        self.assertEqual(self.resource.cpus_per_task, 1)
        self.assertIsNone(self.resource.memory_per_cpu)
        self.assertEqual(self.resource.job_time, timedelta(minutes=10))

    def test_set_resources_with_values(self):
        """Test set resources with values."""
        self.resource = ComputeSettingsResource(ntasks=2, cpus_per_task=2,
                                                memory_per_cpu=1024,
                                                job_time=timedelta(
                                                    minutes=20))
        self.resource._set_resources()

        # Assert that the provided values are set correctly
        self.assertEqual(self.resource.ntasks, 2)
        self.assertEqual(self.resource.cpus_per_task, 2)
        self.assertEqual(self.resource.memory_per_cpu, 1024)
        self.assertEqual(self.resource.job_time, timedelta(minutes=20))

    def test_convert_env_remote(self):
        """Test convert env for remote."""
        self.resource.arch = 'remote'
        self.resource.env = {'key1': 'value1', 'key2': 'value2'}
        self.resource._convert_env()
        self.assertEqual(self.resource.env, ['key1=value1', 'key2=value2'])

    def test_convert_env_local(self):
        """Test convert env for local."""
        self.resource.arch = 'local'
        self.resource.env = 'key1=value1\nkey2=value2'
        self.resource._convert_env()
        self.assertEqual(self.resource.env, {'key1': 'value1',
                                             'key2': 'value2'})

    def test_convert_env_local_invalid(self):
        """Test convert env for local with invalid input."""
        self.resource.arch = 'local'
        self.resource.env = 'key1=value1\ninvalid'
        self.resource._convert_env()
        self.assertEqual(self.resource.env, {'key1': 'value1'})

    # def test_set_env_variables_remote(self):
    #     """Test set env variables for remote."""
    #     self.resource.arch = 'remote'
    #     self.resource._set_env_variables()
    #     self.assertIsNone(self.resource.env)

    def test_set_env_variables_local(self):
        """Test set env variables for local."""
        self.resource.arch_type = 'local'
        self.resource.env_vars = {'PATH': '/test',
                                  'LD_LIBRARY_PATH': '/test',
                                  'TEST_VAR': 'test'}
        self.resource.cpus_per_task = 4
        self.resource._set_env_variables()
        self.assertEqual(self.resource.env['PATH'], '/test')
        # self.assertEqual(self.resource.env['LD_LIBRARY_PATH'],
        #                  '/test:' + os.environ.get('LD_LIBRARY_PATH', ''))
        self.assertEqual(self.resource.env['TEST_VAR'], 'test')
        self.assertEqual(self.resource.env['OMP_NUM_THREADS'], '4')
        self.resource.env = {}
        self.resource.env_vars = None
        self.resource._set_env_variables()
        print('pijiojijij', self.resource.env['PATH'])
        self.assertEqual(self.resource.env['PATH'], os.environ['PATH'])


class TestComputeSettingsLogger(unittest.TestCase):
    """Test class for ComputeSettingsLogger."""

    def setUp(self):
        """Set up for tests."""
        self.logger = ComputeSettingsLogger(print_level='DEBUG')

    def test_set_logger(self):
        """Test set logger."""
        self.logger._set_logger()
        self.assertIsInstance(self.logger.logger, Logger)
        self.assertEqual(self.logger.logger.level, 10)

    def test_set_logger_existing_logger(self):
        """Test set logger with existing logger."""
        existing_logger = get_logger(logging_level='INFO', logger_name='test')
        self.logger.logger = existing_logger
        self.logger._set_logger()
        self.assertEqual(self.logger.logger, existing_logger)

    def test_set_logger_invalid_logger(self):
        """Test set logger with invalid logger."""
        class InvalidLogger:
            pass

        self.logger.logger = InvalidLogger()
        with self.assertRaises(ValueError):
            self.logger._set_logger()


class TestComputeSettingsDirectories(unittest.TestCase):
    """Test class for ComputeSettingsDirectories."""

    def setUp(self):
        """Set up for tests."""
        self.directories = ComputeSettingsDirectories()

    def test_set_dirs(self):
        """Test set directories."""
        self.directories._set_dirs()
        self.assertEqual(self.directories.work_dir_local, Path.cwd())
        self.assertEqual(self.directories.scratch_dir_local, Path.cwd())
        self.assertEqual(self.directories.data_dir_local, Path.cwd())

    def test_set_dirs_with_values(self):
        """Test set directories with values."""
        self.directories.work_dir_local = '/work'
        self.directories.scratch_dir_local = '/scratch'
        self.directories.data_dir_local = '/data'
        self.directories._set_dirs()
        self.assertEqual(self.directories.work_dir_local, '/work')
        self.assertEqual(self.directories.scratch_dir_local, '/scratch')
        self.assertEqual(self.directories.data_dir_local, '/data')


if __name__ == '__main__':
    unittest.main()
