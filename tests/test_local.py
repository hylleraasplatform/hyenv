
import unittest
from pathlib import Path
from typing import Optional
from unittest.mock import MagicMock

from hyset import create_compute_settings
from hyset.local import LocalRunner

# def test_create_cs():
#     """Test creation of ComputeSettings object."""
#     myenv = hyset.create_compute_settings()
#     assert myenv.name == 'local'


# def test_local_cs():
#     """Test methods in LocalArch."""
#     myenv = create_compute_settings(path='/tmp', lib='/tmp')
#     LocalArch.create_path('mytestdir', create=True)
#     out = myenv.Runner.run({'program': 'ls', 'args': ['-l']})
#     assert 'mytestdir' in out
#     LocalArch.delete_path('mytestdir')
#     out = myenv.Runner.run({'program': 'ls', 'args': ['-l']})
#     assert 'mytestdir' not in out


class File:
    """File class."""

    def __init__(self,
                 folder: Optional[str] = None,
                 name: Optional[str] = None):
        """Initialize File object."""
        self.folder = folder
        self.name = name


class RunSettings:
    """RunSettings class."""

    def __init__(self, work_dir_local: Optional[str] = None):
        """Initialize RunSettings object."""
        self.work_dir_local = work_dir_local


def test_initiate_compute_settings():
    """Test creation of ComputeSettings object."""
    myenv = create_compute_settings()
    assert myenv.name == 'local'


class TestCmdSplitting(unittest.TestCase):
    """Test the split_by_delimiters and get_final_result methods."""

    def setUp(self):
        """Set up test."""
        self.runner = LocalRunner()
        self.runner.logger = MagicMock()

    def test_split_by_delimiters(self):
        """Test splitting by delimiters."""
        commands = [
            'echo',
            'Hello',
            'World',
            ';',
            'ls',
            '-l',
            '|',
            'xargs',
            'xtb',
            ';']
        delimiters = [';', '&&', '|']
        expected_output = [['echo', 'Hello', 'World', ';'],
                           ['ls', '-l', '|'], ['xargs', 'xtb', ';']]
        self.assertEqual(
            self.runner.split_by_delimiters(
                commands,
                delimiters),
            expected_output)

    def test_get_final_result_no_post_cmd(self):
        """Test get_final_result method with no post command."""
        # results = [MagicMock(), MagicMock(), MagicMock()]
        cmds = [['echo', 'Hello'], ['ls', '-l'], ['xargs', 'xtb']]
        post_cmd = []
        running_list = ['echo', 'Hello', 'World', ';']
        final_result = self.runner.get_idx_final_result(
            cmds, post_cmd, running_list)
        self.assertEqual(final_result, -1)

    def test_get_final_result_with_post_cmd(self):
        """Test get_final_result method with post command."""
        # results = [MagicMock(), MagicMock(), MagicMock()]
        cmds = [['echo', 'Hello'], ['ls', '-l'], ['xargs', 'xtb']]
        post_cmd = ['xargs', 'xtb', ';']
        running_list = ['echo', 'Hello', 'World', ';']
        final_result = self.runner.get_idx_final_result(
            cmds, post_cmd, running_list)
        self.assertEqual(final_result, 1)

    # def test_get_final_result_with_pipe(self):
    #     """Test get_final_result method with pipe."""
    #     # results = [MagicMock(), MagicMock(), MagicMock()]
    #     cmds = [['echo', 'Hello'], ['ls', '-l'], ['xargs', 'xtb']]
    #     post_cmd = ['xargs', 'xtb', '|']
    #     running_list = ['echo', 'Hello', 'World', '|']
    #     final_result = self.runner.get_idx_final_result(
    #         cmds, post_cmd, running_list)
    #     self.assertEqual(final_result, 2)


class TestFileManager(unittest.TestCase):
    """Test the file manager methods."""

    def setUp(self):
        """Set up test."""
        self.runner = LocalRunner()

    def test_get_path_from_file_with_parent(self):
        """Test get_path_from_file method with perant."""
        file = File(name='file.txt')
        expected_path = Path('/path/to/folder/file.txt')
        self.assertEqual(self.runner.get_path_from_file(
            parent='/path/to/folder/', file=file),
                         expected_path)

    def test_get_path_from_file_with_folder(self):
        """Test get_path_from_file method with folder."""
        file = File(folder='/path/to/folder', name='file.txt')
        expected_path = Path('/path/to/folder/file.txt')
        self.assertEqual(self.runner.get_path_from_file(file=file),
                         expected_path)

    def test_get_path_from_file_with_path(self):
        """Test get_path_from_file method with Path."""
        file = MagicMock()
        file.folder = None
        file.work_path_local = Path('/path/to/file.txt')
        self.assertEqual(self.runner.get_path_from_file(file=file),
                         Path('/path/to/file.txt'))

    def test_get_path_from_file_with_run_settings(self):
        """Test get_path_from_file method with RunSettings."""
        file = File(name='file.txt')
        rs = RunSettings(work_dir_local='/path/to/dir')
        expected_path = Path('/path/to/dir/file.txt')
        self.assertEqual(self.runner.get_path_from_file(file=file,
                                                        run_settings=rs),
                         expected_path)

    def test_get_path_from_file_invalid(self):
        """Test get_path_from_file method with invalid input."""
        file = File()
        rs = RunSettings()
        with self.assertRaises(ValueError):
            self.runner.get_path_from_file(file=file, run_settings=rs)


class TestSanitizeCmd(unittest.TestCase):
    """Test the sanitize_cmd method."""

    def setUp(self):
        """Set up test."""
        self.runner = LocalRunner()

    def test_empty_cmd(self):
        """Test empty command."""
        self.assertEqual(self.runner.sanitize_cmd(''), [])

    def test_combination(self):
        """Test combination."""
        self.assertEqual(self.runner.sanitize_cmd('echo hello && echo world'),
                         ['echo', 'hello', '&&', 'echo', 'world', ';'])

    def test_combination_with_semicolon(self):
        """Test combination with semicolon."""
        self.assertEqual(self.runner.sanitize_cmd('echo hello; echo world'), [
                         'echo', 'hello', ';', 'echo', 'world', ';'])

    def test_combination_with_semicolon_and_ampersand(self):
        """Test combination with semicolon and ampersand."""
        self.assertEqual(
            self.runner.sanitize_cmd(';echo hello| echo world && echo'),
            ['echo', 'hello', '|', 'echo', 'world', '&&', 'echo', ';'])

    def test_list_cmd(self):
        """Test list command."""
        self.assertEqual(self.runner.sanitize_cmd(
            ['echo', 'hello']), ['echo', 'hello', ';'])

    def test_str_cmd(self):
        """Test string command."""
        self.assertEqual(
            self.runner.sanitize_cmd('echo hello'), [
                'echo', 'hello', ';'])

    def test_cmd_with_separator(self):
        """Test command with separator."""
        self.assertEqual(
            self.runner.sanitize_cmd('echo hello&&'), [
                'echo', 'hello', '&&'])

    def test_cmd_with_initial_separator(self):
        """Test command with initial separator."""
        self.assertEqual(
            self.runner.sanitize_cmd('; echo hello'), [
                'echo', 'hello', ';'])

    def test_invalid_cmd_type(self):
        """Test invalid command type."""
        with self.assertRaises(TypeError):
            self.runner.sanitize_cmd(123)

    def test_cmd_with_newline_quoted(self):
        """Test command with newline in str."""
        self.assertEqual(
            self.runner.sanitize_cmd('echo "0\n0\n" |'),
            ['echo', '0\n0\n', '|'])

    def test_cmd_with_newline_quoted2(self):
        """Test command with newline in str."""
        commandline_options = '"0\n0\n"'
        self.assertEqual(
            self.runner.sanitize_cmd(f'echo {commandline_options} |'),
            ['echo', '0\n0\n', '|'])

    def test_cmd_with_newline(self):
        """Test command with newline."""
        self.assertEqual(
            self.runner.sanitize_cmd('echo 0 \n echo 1 |'),
            ['echo', '0', ';', 'echo', '1', '|'])

# def test_parallel_settings():
#     """Test creation of ComputeSettings object."""
#     myenv = create_compute_settings(mpi_procs=2, smp_threads=3, memory=12345,
#                                     launcher='mpirun -np')
#     assert myenv.mpi_procs == 2
#     assert myenv.smp_threads == 3
#     assert myenv.memory == 12345
#     assert myenv.launcher == ['mpirun', '-np']


# def test_subdir():
#     """Test creation of ComputeSettings object."""
#     myenv = create_compute_settings(sub_dir='tests/mytestdir', debug=True)
#     assert myenv.sub_dir == 'mytestdir'
#     myenv.Runner.run({'program': 'touch', 'args': ['testfile']})
#     assert os.path.exists(os.path.join(os.getcwd(), 'mytestdir', 'testfile'))
#     myenv.Runner.run({'program': 'touch', 'args': ['testfile'],
#                       'sub_dir': 'tests/mytestdir2'})
#     assert os.path.exists(os.path.join(os.getcwd(),
#     'mytestdir2', 'testfile'))


if __name__ == '__main__':
    unittest.main()
