import json
import os
import unittest
from pathlib import Path

import yaml

from hyset.base import Result, get_result


class TestResult(unittest.TestCase):
    """Test the Result class."""

    def test_generate_from_dict(self):
        """Test generating Result from dict."""
        data = {'key1': 'value1', 'key2': 'value2'}
        result = Result(data)
        self.assertEqual(result.key1, 'value1')
        self.assertEqual(result.key2, 'value2')

    def test_generate_from_dataclass(self):
        """Test generating Result from dataclass."""
        class DataClass:
            def __init__(self, key1, key2):
                self.key1 = key1
                self.key2 = key2

        data = DataClass('value1', 'value2')
        result = Result(data)
        self.assertEqual(result.key1, 'value1')
        self.assertEqual(result.key2, 'value2')

    def test_generate_from_invalid_input(self):
        """Test generating Result from invalid input."""
        with self.assertRaises(TypeError):
            Result(123)  # Invalid input type

    def test_result_base_magic_methods(self):
        """Test magic methods of Result."""
        data = {'key1': 'value1', 'key2': 'value2', 'key3': 'value3',
                'key4': 'value4', 'key5': 'value5', 'key6': 'value6'}
        ref = '''{'key1': 'value1',
 'key2': 'value2',
 'key3': 'value3',
 'key4': 'value4',
 'key5': 'value5',
 'key6': 'value6'}'''
        result1 = Result(data)
        result2 = Result(data)
        self.assertEqual(result1, result2)  # __eq__ method
        self.assertEqual(result1['key1'], 'value1')  # __getitem__ method
        self.assertEqual(repr(result1), ref)  # __repr__
        self.assertEqual(str(result1), ref)  # __str__

    def test_magic_methods2(self):
        """Test magic methods of Result #2."""
        result = Result()
        result['key1'] = 'value1'
        result2 = Result()
        result2['key1'] = 'value1'
        result2['key2'] = 'value2'

        # Test __setitem__ and __getitem__
        self.assertEqual(result['key1'], 'value1')
        # Test __len__
        self.assertEqual(len(result), 1)
        # Test __contains__
        self.assertTrue('key1' in result)
        # Test __delitem__
        del result['key1']
        self.assertFalse('key1' in result)
        # Test __eq__ and __ne__
        self.assertNotEqual(result, result2)

    def test_result_base_items(self):
        """Test items, keys, and values methods of Result."""
        data = {'key1': 'value1', 'key2': 'value2'}
        result = Result(data)
        expected_items = [('key1', 'value1'), ('key2', 'value2')]
        self.assertEqual(list(result.items()), expected_items)
        expected_keys = ['key1', 'key2']
        self.assertEqual(list(result.keys()), expected_keys)
        expected_values = ['value1', 'value2']
        self.assertEqual(list(result.values()), expected_values)

    def test_result_base_iter(self):
        """Test __iter__ method of Result."""
        data = {'key1': 'value1', 'key2': 'value2'}
        result = Result(data)
        expected_items = [('key1', 'value1'), ('key2', 'value2')]
        self.assertEqual(list(iter(result)), expected_items)

    def test_result_base_getitem(self):
        """Test __getitem__ method of Result."""
        data = {'key1': 'value1', 'key2': 'value2'}
        result = Result(data)
        self.assertEqual(result['key1'], 'value1')
        self.assertEqual(result['key2'], 'value2')
        self.assertEqual(result.get('key1'), 'value1')


class TestResultFunction(unittest.TestCase):
    """Test the get_result function."""

    def setUp(self):
        """Set up."""
        self.result_dict = {'key': 'value'}

    def test_dict_output(self):
        """Test dict output."""
        self.assertEqual(get_result(self.result_dict,
                                    output_type='dict'),
                         self.result_dict)

    def test_json_output(self):
        """Test JSON output."""
        self.assertEqual(get_result(self.result_dict,
                                    output_type='json'),
                         json.dumps(self.result_dict))

    def test_yaml_output(self):
        """Test YAML output."""
        self.assertEqual(get_result(self.result_dict,
                                    output_type='yaml'),
                         yaml.dump(self.result_dict))

    def test_file_output(self):
        """Test file output."""
        output_path = 'test_output.txt'
        self.assertEqual(get_result(self.result_dict,
                                    output_type=output_path),
                         str(Path(output_path).resolve()))
        os.remove(output_path)  # Löschen Sie die Testdatei nach dem Test

    def test_invalid_input(self):
        """Test invalid input."""
        with self.assertRaises(TypeError):
            get_result('invalid_input', 'dict')

    def test_dataclass_input(self):
        """Test dataclass input."""
        class DataClass:
            def __init__(self, key1, key2):
                self.key1 = key1
                self.key2 = key2

        data = DataClass('value1', 'value2')
        self.assertEqual(get_result(data, 'dict'),
                         {'key1': 'value1', 'key2': 'value2'})

    # def test_add_input(self):
    #     """Test add_input_object_to_dict function."""
    #     from hyobj import Molecule
    #     result = get_result({}, input_object=Molecule('H 0 0 0\nH 0 0 1'))
    #     self.assertEqual(result['atoms'], ['H', 'H'])

    # def test_add_input_custom_mapping(self):
    #     """Test add_input_object_to_dict function with custom mapping."""
    #     from hyobj import Molecule
    #     result = get_result({}, input_object=Molecule('H 0 0 0\nH 0 0 1'),
    #                         input_mapping={'_atoms': 'types'})
    #     self.assertEqual(result['types'], ['H', 'H'])

    def test_list_result(self):
        """Test list result."""
        input_list = [{'key1': 'value1'}, {'key2': 'value2'}]
        result = get_result(input_list, 'dict')
        self.assertEqual(result, input_list)


if __name__ == '__main__':
    unittest.main()
