import os
import unittest
from unittest.mock import patch

import yaml

from hyset import (ComputeSettings, LocalArch, LocalRunSettings,
                   create_compute_settings, create_run_settings)
from hyset.creators import _dict_from_str


class TestCreatorsBase(unittest.TestCase):
    """Test class for creators."""

    def test_create_run_settings(self):
        """Test creating run settings."""
        run_settings = create_run_settings(arch_type='local', ntasks=4)
        self.assertIsInstance(run_settings, LocalRunSettings)
        self.assertEqual(run_settings.ntasks, 4)

    def test_create_compute_settings(self):
        """Test creating compute settings."""
        compute_settings = create_compute_settings('local', ntasks=4)
        self.assertIsInstance(compute_settings, LocalArch)
        self.assertEqual(compute_settings.ntasks, 4)


class TestCreatorsComputeSettings(unittest.TestCase):
    """Test class for creators."""

    def test_create_compute_settings(self):
        """Test creating compute settings."""
        # Test with a known architecture
        arch = 'local'
        cs = create_compute_settings(arch)
        self.assertIsInstance(cs, ComputeSettings)
        self.assertEqual(cs.target, arch)

        filename = 'cs.yml'
        cs.dump(filename=filename)
        cs2 = create_compute_settings(filename)
        self.assertIsInstance(cs2, ComputeSettings)
        self.assertEqual(cs2.target, arch)
        os.remove(filename)


def mock_from_file_side_effect(arg):
    """Mock the _from_file function."""
    if arg == 'file.yml':
        return {'target': 'local'}
    else:
        return None


class TestCreators(unittest.TestCase):
    """Test class for creators."""

    @patch('hyset.creators._from_file', side_effect=mock_from_file_side_effect)
    def test_create_compute_settings(self, mock_from_file):
        """Test create_compute_settings function."""
        # Mock the _from_file function

        # Test with a known architecture
        arch = 'local'
        cs = create_compute_settings(arch)
        self.assertEqual(cs.target, arch)

        # Test with ComputeSettings

        cs = create_compute_settings(cs)
        self.assertEqual(cs.target, arch)

        # Test with a dictionary
        cs = create_compute_settings({'target': arch})
        self.assertEqual(cs.target, arch)

        # Test with a file
        mock_from_file.return_value = {'target': arch}
        cs = create_compute_settings('file.yml')
        self.assertEqual(cs.target, arch)

        # Test with an unknown architecture
        with self.assertRaises(NotImplementedError):
            create_compute_settings('unknown')

    def test_dict_from_str(self):
        """Test _dict_from_str function."""
        # Test with a valid JSON string
        s = '{"key": "value"}'
        expected = {'key': 'value'}
        self.assertEqual(_dict_from_str(s), expected)

        # Test with a valid YAML string
        s = 'key: value'
        expected = {'key': 'value'}
        self.assertEqual(_dict_from_str(s), expected)

        # Test with an invalid string
        s = 'invalid'
        self.assertIsNone(_dict_from_str(s))

        # Test with a string that causes a YAMLError
        with patch('yaml.safe_load', side_effect=yaml.YAMLError):
            s = '{:}'
            self.assertIsNone(_dict_from_str(s))


if __name__ == '__main__':
    unittest.main()
