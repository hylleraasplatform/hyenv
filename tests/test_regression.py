# requires pytest-regressions installed (pip install pytest-regressions)
import os
from pathlib import Path

import pytest
import yaml
from hyif import Xtb
from hyobj import Molecule

from hyset import create_compute_settings as ccs


@pytest.fixture(scope='session')
def original_datadir(request) -> Path:
    """Return the original data directory."""
    config = request.config
    return config.rootpath / config.getini('original_datadir')


@pytest.fixture(scope='session')
def datadir(request) -> Path:
    """Return the data directory."""
    config = request.config
    return config.rootpath / config.getini('datadir')


default_tolerance = dict(atol=1e-6, rtol=1e-6)
keys_to_extract = ['energy', 'gradient', 'molecular_dipole']

default_cs = {'print_level': 'debug',
              'force_recompute': True}
compute_settings = {
    'local': ccs('local', **default_cs),
    # 'docker': ccs('docker', container_image='xtb', **default_cs),
    'conda': ccs('conda', conda_env='base', **default_cs),
    'saga': ccs('saga', modules=['xtb/6.4.1-intel-2021a'],
                user=os.getlogin(),
                memory_per_cpu=2000, progress_bar=False, **default_cs)
}
molecules = {'water': Molecule('O'), 'methane': Molecule('C')}
molecules.pop('methane')


def calculate(compute_settings, mol, keys_to_extract=keys_to_extract):
    """Generate data."""
    x = Xtb(compute_settings=compute_settings,
            check_version=False, properties=['gradient'])
    result = x.run(mol)
    return {key: result[key] for key in keys_to_extract}


@pytest.mark.parametrize('mol',
                         list(molecules.values()),
                         ids=list(molecules.keys()))
@pytest.mark.parametrize('cs',
                         list(compute_settings.values()),
                         ids=list(compute_settings.keys()))
def test_all(cs, mol, num_regression, request):
    """Test all."""
    data = calculate(cs, mol)
    # check if value is identical to the reference calculated with same method
    num_regression.check(data,
                         basename=f'{request.node.name}_{mol.hash}',
                         default_tolerance=default_tolerance)
    # check that all values are identical to local reference
    num_regression.check(data,
                         basename=f'{mol.hash}',
                         default_tolerance=default_tolerance)


@pytest.mark.parametrize('mol',
                         list(molecules.values()),
                         ids=list(molecules.keys()))
def test_read_from_file(mol, num_regression):
    """Test test_read_from_file."""
    filename = './cs.yml'
    cs0 = ccs('local', **default_cs).dump()
    with open('./cs.yml', 'w') as f:
        yaml.dump(cs0, f)

    cs1 = ccs(filename)
    data = calculate(cs1, mol)
    num_regression.check(data,
                         basename='from_file',
                         default_tolerance=default_tolerance)
    os.remove(filename)


filenames = ['./overwrite/compute_settings.yml',
             './overwrite/compute_settings_saga.yml'
             ]


@pytest.mark.parametrize('filename',
                         filenames,
                         ids=filenames)
@pytest.mark.parametrize('mol',
                         list(molecules.values()),
                         ids=list(molecules.keys()))
def test_read_from_file_overwrite(mol, filename, num_regression, request):
    """Test test_read_from_file_overwrite."""
    # test overwrite
    cs0 = ccs('saga', user=os.getlogin())
    cs1 = ccs('local', print_level='warning').dump()
    os.makedirs('./overwrite', exist_ok=True)
    with open(filename, 'w') as f:
        yaml.dump(cs1, f)

    data = calculate(cs0, mol)
    num_regression.check(data,
                         basename=f'{request.node.name}-{filename}',
                         default_tolerance=default_tolerance)
    os.remove(filename)
    os.rmdir('./overwrite')
