import unittest

from hyset.container import (ComputeSettingsContainer, get_docker_cmd,
                             get_singularity_cmd)


class TestDocker(unittest.TestCase):
    """Test class for Docker."""

    def test_get_docker_cmd(self):
        """Test get_docker_cmd."""
        expected_cmd = [
            'docker',
            'run', '-a', 'stdout', '-a', 'stderr', '--rm',
            '--cpus', '2',
            '--env TEST_ENV=test_value',
            '--env OMP_NUM_THREADS=2',
            '-v/local:/container',
            '-w/work',
            'test_image'
        ]
        actual_cmd = get_docker_cmd(
            cpus_per_task=2,
            env_vars={'TEST_ENV': 'test_value'},
            container_image='test_image',
            container_mounts={'/local': '/container'},
            work_dir_container='/work'
        )
        self.assertEqual(actual_cmd, expected_cmd)

    def test_get_docker_cmd_no_image(self):
        """Test get_docker_cmd with no image."""
        with self.assertRaises(ValueError):
            get_docker_cmd()


class TestSingularity(unittest.TestCase):
    """Test class for Singularity."""

    def test_get_singularity_cmd(self):
        """Test get_singularity_cmd."""
        with self.assertRaises(NotImplementedError):
            get_singularity_cmd()


class TestComputeSettingsContainer(unittest.TestCase):
    """Test class for ComputeSettingsContainer."""

    def setUp(self):
        """Set up for tests."""
        self.container = ComputeSettingsContainer(
            container_image='test_image',
            container_executable='docker',
            container_mounts={'/local': '/container'},
            container_type='docker',
            work_dir_container='/work_dir',
            scratch_dir_container='/scratch_dir',
            data_dir_container='/data_dir'
        )
        self.container.work_dir_local = '/work_dir'
        self.container.scratch_dir_local = '/scratch_dir'
        self.container.data_dir_local = '/data_dir'
        self.container.cpus_per_task = 2
        self.container.env_vars = {'TEST_ENV': 'test_value'}

    def test_set_container(self):
        """Test set container."""
        self.container._set_container()
        self.assertEqual(self.container.container_type, 'docker')
        self.assertEqual(self.container.container_executable, 'docker')
        self.assertEqual(self.container.container_mounts, {
            '/local': '/container',
            '/work_dir': '/work_dir',
            '/scratch_dir': '/scratch_dir',
            '/data_dir': '/data_dir'
        })

    def test_set_container_dirs(self):
        """Test set directories."""
        self.container._set_container_dirs()
        self.assertEqual(str(self.container.work_dir_container), '/work_dir')
        self.assertEqual(str(self.container.scratch_dir_container),
                         '/scratch_dir')
        self.assertEqual(str(self.container.data_dir_container), '/data_dir')

    def test_get_container_type(self):
        """Test get container type."""
        self.assertEqual(self.container._get_container_type(), 'docker')

    def test_update_container_launcher(self):
        """Test update_container_launcher."""
        self.container.update_container_launcher()
        # note: the actual command contains the aboslute pahts
        # -v work_dir_local: work_dir_container
        # which is dependend on the machine and thus not tested here
        expected_cmd = [
            'docker',
            'run', '-a', 'stdout', '-a', 'stderr', '--rm',
            '--cpus', '2',
            '--env TEST_ENV=test_value',
            '--env OMP_NUM_THREADS=2',
            '-v/local:/container',
            '-w/work_dir',
            'test_image'
        ]
        for cmd in expected_cmd:
            self.assertIn(cmd, self.container.launcher)

        # test for possible duplicatgion
        self.container.update_container_launcher()
        # check that docker appears only once in launcher
        self.assertEqual(self.container.launcher.count('docker'), 1)


if __name__ == '__main__':
    unittest.main()
